package it.unisannio.ding.wslink.dataevents;

import it.unisannio.ding.wslink.api.ConsistencyType;
import it.unisannio.ding.wslink.api.EncodingType;
import it.unisannio.ding.wslink.api.PureLazyStrategy;
import it.unisannio.ding.wslink.api.annotation.Consistency;
import it.unisannio.ding.wslink.api.annotation.Encoding;
import it.unisannio.ding.wslink.api.annotation.Strategy;
import it.unisannio.ding.wslink.api.annotation.WSLink;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

// TODO: Auto-generated Javadoc
/**
 * The Class DataEvent.
 * 
 * @author qzagarese
 */
@XmlRootElement
@WSLink
public class DataEvent {
	/**
	 * The attachments of this events. Can be files or instances. All the
	 * attachments are in the same buffer, but indexes of each one are stored in
	 * descriptors. This releaves the unmarshaller of useless computation
	 * 
	 * */
	@Strategy(impl = PureLazyStrategy.class)
	@Encoding(EncodingType.XML)
	@Consistency(ConsistencyType.MAKE_COPY)
	private byte[] attachments;

	private Long timestamp;

	/**
	 * The descriptors of the attachments; descriptor at position x describes
	 * attachment in position x in attachments.
	 */
	private List<AttachmentDescriptor> descriptors;

	/** The topic expression. */
	private String topicExpression;

	/**
	 * Gets the attachments.
	 * 
	 * @return the attachments
	 */
	public byte[] getAttachments() {
		return attachments;
	}

	/**
	 * Sets the attachments.
	 * 
	 * @param attachments
	 *            the new attachments
	 */
	public void setAttachments(byte[] attachments) {
		this.attachments = attachments;
	}

	/**
	 * Gets the descriptors.
	 * 
	 * @return the descriptors
	 */
	public List<AttachmentDescriptor> getDescriptors() {
		return descriptors;
	}

	/**
	 * Sets the descriptors.
	 * 
	 * @param descriptors
	 *            the new descriptors
	 */
	public void setDescriptors(List<AttachmentDescriptor> descriptors) {
		this.descriptors = descriptors;
	}

	/**
	 * Gets the topic expression.
	 * 
	 * @return the topic expression
	 */
	public String getTopicExpression() {
		return topicExpression;
	}

	/**
	 * Sets the topic expression.
	 * 
	 * @param topicExpression
	 *            the new topic expression
	 */
	public void setTopicExpression(String topicExpression) {
		this.topicExpression = topicExpression;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
}
