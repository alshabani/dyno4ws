package it.unisannio.ding.dyno4ws.interceptors;

import it.unisannio.ding.dyno4ws.DynO4WSHeader;
import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.proxy.DynO4WSProxy;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.lang.reflect.Field;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

public class OutgoingRequestInterceptor extends
		AbstractPhaseInterceptor<Message> implements DynO4WSCXFInterceptor {

	private String nodeId;

	private String repositoryEndpoint;

	private OffloadingStrategy strategy;

	private OffloadingRepository repository;

	private Endpoint endpoint;

	private ParameterInterceptor parameterInterceptor;

	private OffloadingInterceptor offloadingInterceptor;
	
	
//	private PrintStream ps;

	public OutgoingRequestInterceptor() {
		super(Phase.USER_LOGICAL);
		// this.addAfter(SoapOutInterceptor.class.getName());
//		try {
//			ps = new PrintStream(new FileOutputStream(new File("OutgoingRequestInterceptor.txt")));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	@SuppressWarnings("rawtypes")
	public void handleMessage(Message message) throws Fault {
//		long ts = System.nanoTime();
		
		
		SoapMessage soap = (SoapMessage) message;
		List content = message.getContent(List.class);
		Object request = content.get(0);
		DynO4WSHeader header = new DynO4WSHeader();
		String callId = new Long(System.nanoTime()).toString();
		header.setCallId(callId);
		header.setNodeId(nodeId);

		for (Field f : request.getClass().getDeclaredFields()) {
			boolean accessible = f.isAccessible();
			f.setAccessible(true);
			Object target = null;
			try {
				target = f.get(request);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			OffloadingContext context = new OffloadingContext();
			context.setEndpoint(endpoint);
			context.setNodeId(nodeId);
			context.setParameterName(f.getName());
			context.setRepository(repository);
			context.setRepositoryEndpoint(repositoryEndpoint);
			context.setStrategy(strategy);
			context.setTarget(target);
			context.setOperationName(request.getClass().getSimpleName());
			context.setCallId(callId);
			DynO4WSParam paramHeader = null;
			if (target instanceof DynO4WSProxy) {
				paramHeader = offloadingInterceptor.onProxyINParam(context);
				DynO4WSProxy proxy = (DynO4WSProxy) target;
				try {
					f.set(request, proxy.getContext().getTarget());
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				paramHeader = offloadingInterceptor.onEntityINParam(context);
			}
			if (paramHeader != null) {
				if (paramHeader.getFields().size() > 0) {
					header.getParams().add(paramHeader);
				}
			}

			f.setAccessible(accessible);
		}

		try {
			soap.getHeaders().add(
					new Header(new QName("dyno4ws"), header,
							new JAXBDataBinding(DynO4WSHeader.class)));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		ts = System.nanoTime() - ts;
//		ps.println(ts);
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public void setRepositoryEndpoint(String repositoryEndpoint) {
		this.repositoryEndpoint = repositoryEndpoint;
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository = repository;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public void setParameterInterceptor(
			ParameterInterceptor parameterInterceptor) {
		this.parameterInterceptor = parameterInterceptor;
	}

	public void setOffloadingInterceptor(
			OffloadingInterceptor offloadingInterceptor) {
		this.offloadingInterceptor = offloadingInterceptor;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getRepositoryEndpoint() {
		return repositoryEndpoint;
	}

	public OffloadingStrategy getStrategy() {
		return strategy;
	}

	public OffloadingRepository getRepository() {
		return repository;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public ParameterInterceptor getParameterInterceptor() {
		return parameterInterceptor;
	}

	public OffloadingInterceptor getOffloadingInterceptor() {
		return offloadingInterceptor;
	}

}
