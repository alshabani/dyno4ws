package it.unisannio.ding.dyno4ws;

import it.unisannio.ding.dyno4ws.interceptors.DynO4WSCXFInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.IncomingRequestInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.IncomingResponseInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.OutgoingRequestInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.OutgoingResponseInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.repository.RepositoryProxy;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;
import it.unisannio.ding.dyno4ws.strategy.StrategyProxy;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

public class DynO4WS {

	
	private Server repositoryService;

	private String nodeId;

	private StrategyProxy strategy = new StrategyProxy();

	private RepositoryProxy repository = new RepositoryProxy();
	
	private OffloadingInterceptor offloadingInterceptor;
	
	private ParameterInterceptor parameterInterceptor;

	private String repositoryEndpoint;
	
	private List<Endpoint> registeredClients = new ArrayList<Endpoint>();
	
	private List<Endpoint> registeredServices = new ArrayList<Endpoint>();
	
	private List<DynO4WSCXFInterceptor> innerInterceptors = new ArrayList<DynO4WSCXFInterceptor>();

	public DynO4WS() {
		this(System.nanoTime() + "@DynO4WS");
	}

	public DynO4WS(String id) {
		this(id, "localhost");
	}

	public DynO4WS(String id, String host) {
		this(id, host, 12358);
	}

	public DynO4WS(String id, String host, int port) {
		this.nodeId = id;
		repository.setStrategy(strategy);
		try {
			URL repositoryURL = new URL("http://" + host + ":" + port + "/DynO4WS/repository");
			publishRepository(repositoryURL);
			this.repositoryEndpoint = repositoryService.getEndpoint().getEndpointInfo().getAddress() + "?wsdl";
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	private void publishRepository(URL endpoint) {
		ServerFactoryBean svrFactory = new JaxWsServerFactoryBean();
		svrFactory.setServiceClass(OffloadingRepository.class);
		svrFactory.setAddress(endpoint.toExternalForm());
		svrFactory.setServiceBean(repository);
		repositoryService = svrFactory.create();
//		repositoryService.getEndpoint().getInInterceptors()
//		.add(new LoggingInInterceptor());
//		repositoryService.getEndpoint().getOutInterceptors()
//				.add(new LoggingOutInterceptor());
//		repositoryService.getEndpoint().getInInterceptors().add(new RequestTimestamper());
//		repositoryService.getEndpoint().getOutInterceptors().add(new ResponseTimestamper("RepositoryTime.txt"));
	}

	private void setupInterceptor(Endpoint endpoint,
			DynO4WSCXFInterceptor interceptor) {
		interceptor.setNodeId(nodeId);
		interceptor.setStrategy(strategy);
		interceptor.setRepository(repository);
		interceptor.setOffloadingInterceptor(offloadingInterceptor);
		interceptor.setParameterInterceptor(parameterInterceptor);
		interceptor.setEndpoint(endpoint);
		interceptor.setRepositoryEndpoint(repositoryEndpoint);
	}

	public void registerService(Endpoint endpoint) {
		OutgoingResponseInterceptor responseInterceptor = new OutgoingResponseInterceptor();
		setupInterceptor(endpoint, responseInterceptor);
		
		IncomingRequestInterceptor requestInterceptor = new IncomingRequestInterceptor();
		setupInterceptor(endpoint, requestInterceptor);
		
		innerInterceptors.add(requestInterceptor);
		innerInterceptors.add(responseInterceptor);
		endpoint.getInInterceptors().add(requestInterceptor);
		endpoint.getOutInterceptors().add(responseInterceptor);
		
		registeredServices.add(endpoint);
	}

	public void registerClient(Endpoint endpoint) {
		IncomingResponseInterceptor responseInterceptor = new IncomingResponseInterceptor();
		setupInterceptor(endpoint, responseInterceptor);
		
		OutgoingRequestInterceptor requestInterceptor = new OutgoingRequestInterceptor();
		setupInterceptor(endpoint, requestInterceptor);

		innerInterceptors.add(requestInterceptor);
		innerInterceptors.add(responseInterceptor);
		
		endpoint.getInInterceptors().add(responseInterceptor);
		endpoint.getOutInterceptors().add(requestInterceptor);
		
		registeredClients.add(endpoint);
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy.setImplementingInstance(strategy);
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository.setImplementingInstance(repository);
	}

	public void setStrategyService(URL endpoint) {
		strategy.setImplementingService(endpoint);
		repository.setStrategyEndpoint(endpoint.toExternalForm());
	}

	public void setRepositoryService(URL endpoint) {
		repository.setImplementingService(endpoint);
		repositoryEndpoint = endpoint.toExternalForm();
		for(DynO4WSCXFInterceptor interceptor : innerInterceptors){
			interceptor.setRepositoryEndpoint(repositoryEndpoint);
		}
	}

	

	public void setOffloadingInterceptor(OffloadingInterceptor offloadingInterceptor) {
		this.offloadingInterceptor = offloadingInterceptor;
		for(DynO4WSCXFInterceptor interceptor : innerInterceptors){
			interceptor.setOffloadingInterceptor(offloadingInterceptor);
		}
	}

	public void setParameterInterceptor(ParameterInterceptor parameterInterceptor) {
		this.parameterInterceptor = parameterInterceptor;
		for(DynO4WSCXFInterceptor interceptor : innerInterceptors){
			interceptor.setParameterInterceptor(parameterInterceptor);
		}
	}

	public String getNodeId() {
		return nodeId;
	}

	public void shutdown() {
		repositoryService.stop();
	}

}
