/**
 * 
 */
package it.unisannio.ding.wslink.api;

/**
 * @author Quirino Zagarese
 *
 */
public enum EncodingType {

	XML,
	GZIP_XML,
	JSON,
	GZIP_JSON;
	
}
