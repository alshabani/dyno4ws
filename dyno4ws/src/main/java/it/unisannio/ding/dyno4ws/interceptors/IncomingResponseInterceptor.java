package it.unisannio.ding.dyno4ws.interceptors;

import it.unisannio.ding.dyno4ws.DynO4WSHeader;
import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.proxy.ParameterProxyFactory;
import it.unisannio.ding.dyno4ws.proxy.ProxyContext;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.lang.reflect.Field;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMSource;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Node;

public class IncomingResponseInterceptor extends
		AbstractPhaseInterceptor<Message> implements DynO4WSCXFInterceptor {

	private String nodeId;

	private String repositoryEndpoint;

	private OffloadingStrategy strategy;

	private OffloadingRepository repository;

	private Endpoint endpoint;

	private ParameterInterceptor parameterInterceptor;

	private OffloadingInterceptor offloadingInterceptor;

//	private PrintStream ps;
//
//	private PrintStream ps2;
	
	public IncomingResponseInterceptor() {
		super(Phase.USER_LOGICAL);
//		try {
//			ps = new PrintStream(new FileOutputStream(new File("IncomingResponseInterceptor.txt")));
//			ps2 = new PrintStream(new FileOutputStream(new File("ProxingTime.txt")));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	public void handleMessage(Message message) throws Fault {
	
//		long ts = System.nanoTime();
//		long proxingTime = 0;
		
		
		SoapMessage soap = (SoapMessage) message;
		Header header = soap.getHeader(new QName("dyno4ws"));
		DOMSource source = new DOMSource((Node) header.getObject());
		DynO4WSHeader dynoHeader = JAXB.unmarshal(source, DynO4WSHeader.class);
		MessageContentsList contentList = (MessageContentsList) soap
				.getContent(List.class);
		Object object = contentList.get(0);
		try {
			Class<? extends Object> class1 = object.getClass();
			Field[] declaredFields = class1.getDeclaredFields();
			for (Field f : declaredFields) {
				if (f.getName().equals("_return")) {
					boolean accessible = f.isAccessible();
					f.setAccessible(true);
					Object value = f.get(object);
					ProxyContext context = new ProxyContext();
					context.setInterceptor(parameterInterceptor);
					context.setOffloadingData(dynoHeader);
					context.setRepository(repository);
					context.setStrategy(strategy);
					context.setTarget(value);
					List<DynO4WSParam> params = dynoHeader.getParams();
					if (params.size() > 0) {
						context.setParameterData(params.get(0));
//						proxingTime = System.nanoTime();
						Object proxy = ParameterProxyFactory
								.createProxy(context);
//						proxingTime = System.nanoTime() - proxingTime;
						f.set(object, proxy);
//						context.setProxy((DynO4WSProxy) proxy);
						f.setAccessible(accessible);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

//		ts = System.nanoTime() - ts;
//		ps.println(ts);
//		ps2.println(proxingTime);
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public void setRepositoryEndpoint(String repositoryEndpoint) {
		this.repositoryEndpoint = repositoryEndpoint;
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository = repository;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public void setParameterInterceptor(
			ParameterInterceptor parameterInterceptor) {
		this.parameterInterceptor = parameterInterceptor;
	}

	public void setOffloadingInterceptor(
			OffloadingInterceptor offloadingInterceptor) {
		this.offloadingInterceptor = offloadingInterceptor;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getRepositoryEndpoint() {
		return repositoryEndpoint;
	}

	public OffloadingStrategy getStrategy() {
		return strategy;
	}

	public OffloadingRepository getRepository() {
		return repository;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public ParameterInterceptor getParameterInterceptor() {
		return parameterInterceptor;
	}

	public OffloadingInterceptor getOffloadingInterceptor() {
		return offloadingInterceptor;
	}

}
