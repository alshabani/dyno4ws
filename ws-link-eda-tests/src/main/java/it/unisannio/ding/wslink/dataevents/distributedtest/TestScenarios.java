package it.unisannio.ding.wslink.dataevents.distributedtest;

public enum TestScenarios {
	ONE_TO_ONE, MULTIPLE_TO_ONE, ONE_TO_MULTIPLE, MULTIPLE_TO_MULTIPLE, MULTI_SUB_PER_HOST, MULTI_PUB_PER_HOST
}
