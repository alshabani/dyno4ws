package it.unisannio.ding.dyno4ws.strategy;

import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.ProfileData;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;

import java.util.List;

import javax.jws.WebService;


@WebService
public interface OffloadingStrategy {

	public List<String> getParamOffloadableFields(String nodeId,
			String endpoint, String operation, String parameterName,
			String parameterType, List<String> fieldsList, List<Integer> fieldsSizes);
	
	public DynO4WSParam selectAndOffloadParamFields(String nodeId, String endpoint,
			String operation, String paramName, Object target, OffloadingContext context);
	
	
	public void updateProfiling(String nodeId, ProfileData data);
	
}
