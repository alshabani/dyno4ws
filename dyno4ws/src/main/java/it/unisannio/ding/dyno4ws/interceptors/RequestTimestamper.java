package it.unisannio.ding.dyno4ws.interceptors;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

public class RequestTimestamper extends AbstractPhaseInterceptor<Message> {

	public RequestTimestamper(){
		super(Phase.RECEIVE);
	}
	
	
	
	public void handleMessage(Message message) throws Fault {
		message.put("ts", System.nanoTime());
		
	}

}
