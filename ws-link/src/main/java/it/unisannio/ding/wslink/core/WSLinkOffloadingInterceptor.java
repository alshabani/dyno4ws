/**
 * 
 */
package it.unisannio.ding.wslink.core;

import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.DynO4WSParamField;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.proxy.DynO4WSProxy;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

/**
 * @author Quirino Zagarese
 * 
 */
public class WSLinkOffloadingInterceptor implements OffloadingInterceptor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor
	 * #onEntityINParam
	 * (it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext)
	 */
	public DynO4WSParam onEntityINParam(OffloadingContext context) {
		return onEntityOUTParam(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor
	 * #onProxyINParam
	 * (it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext)
	 */
	public DynO4WSParam onProxyINParam(OffloadingContext context) {
		return onProxyOUTParam(context);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor
	 * #onEntityOUTParam
	 * (it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext)
	 */
	public DynO4WSParam onEntityOUTParam(OffloadingContext context) {
		Object target = context.getTarget();
		return handleParam(context, target);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor
	 * #onProxyOUTParam
	 * (it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext)
	 */
	public DynO4WSParam onProxyOUTParam(OffloadingContext context) {
		DynO4WSProxy proxy = (DynO4WSProxy)context.getTarget();
		Object target = proxy.getContext().getTarget();
		DynO4WSParam param = handleParam(context, target);
		for(DynO4WSParamField field : proxy.getContext().getParameterData().getFields()){
			param.getFields().add(field);
		}
		return param;
	}

	private DynO4WSParam handleParam(OffloadingContext context, Object target) {
		OffloadingStrategy strategy = context.getStrategy();
	
		String requestNodeId = context.getNodeId();
		String endpoint = context.getEndpoint().getEndpointInfo().getAddress();
	
		DynO4WSParam param = strategy.selectAndOffloadParamFields(requestNodeId, endpoint,
				context.getOperationName(), context.getParameterName(), target, context);
	
		param.setName(context.getParameterName());
		return param;
	}

}
