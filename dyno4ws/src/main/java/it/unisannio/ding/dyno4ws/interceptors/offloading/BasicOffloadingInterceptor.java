package it.unisannio.ding.dyno4ws.interceptors.offloading;

import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.DynO4WSParamField;
import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.proxy.DynO4WSProxy;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXB;

import org.apache.cxf.endpoint.Endpoint;

public class BasicOffloadingInterceptor implements OffloadingInterceptor {

	// private PrintStream ps;
	// private PrintStream ps2;

	public BasicOffloadingInterceptor() {
		// try {
		// ps = new PrintStream(new FileOutputStream(new
		// File("OffloadingTime.txt")));
		// ps2 = new PrintStream(new FileOutputStream(new
		// File("SerializationTime.txt")));
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	public DynO4WSParam onEntityINParam(OffloadingContext context) {
		Object target = context.getTarget();
		List<String> fieldsList = getFieldsList(target);
		OffloadingStrategy strategy = context.getStrategy();
		Endpoint endpoint = context.getEndpoint();
		OffloadingRepository repository = context.getRepository();

		DynO4WSParam param = new DynO4WSParam();
		param.setName(context.getParameterName());
		List<String> offloadableFields = strategy.getParamOffloadableFields(
				context.getNodeId(), endpoint.getEndpointInfo().getAddress(),
				context.getOperationName(), context.getParameterName(), target
						.getClass().getName(), fieldsList,
				new ArrayList<Integer>());
		List<OffloadedField> fieldsToOffload = new ArrayList<OffloadedField>();
		prepareOffloading(context, target, param, offloadableFields,
				fieldsToOffload);
		if (fieldsToOffload.size() > 0) {
			List<String> ids = repository.offloadParamFields(
					context.getNodeId(), context.getCallId(),
					context.getParameterName(), fieldsToOffload);
			List<DynO4WSParamField> fieldsHeaders = param.getFields();
			for (int i = 0; i < fieldsHeaders.size(); i++) {
				fieldsHeaders.get(i).setId(ids.get(i));
			}
		}
		return param;
	}

	public DynO4WSParam onProxyINParam(OffloadingContext context) {
		DynO4WSProxy proxy = (DynO4WSProxy) context.getTarget();
		Object target = proxy.getContext().getTarget();
		List<String> fieldsList = getFieldsList(target);
		Endpoint endpoint = context.getEndpoint();
		OffloadingStrategy strategy = context.getStrategy();
		OffloadingRepository repository = context.getRepository();
		List<String> offloadableFields = strategy.getParamOffloadableFields(
				context.getNodeId(), endpoint.getEndpointInfo().getAddress(),
				context.getOperationName(), context.getParameterName(), target
						.getClass().getName(), fieldsList,
				new ArrayList<Integer>());
		DynO4WSParam parameterData = proxy.getContext().getParameterData();

		DynO4WSParam param = new DynO4WSParam();
		param.setName(context.getParameterName());

		for (DynO4WSParamField field : parameterData.getFields()) {
			param.getFields().add(field);
			offloadableFields.remove(field.getName());
		}

		List<OffloadedField> fieldsToOffload = new ArrayList<OffloadedField>();
		prepareOffloading(context, target, param, offloadableFields,
				fieldsToOffload);
		if (fieldsToOffload.size() > 0) {
			List<String> ids = repository.offloadParamFields(
					context.getNodeId(), context.getCallId(),
					context.getParameterName(), fieldsToOffload);
			List<DynO4WSParamField> fieldsHeaders = param.getFields();
			int i = 0;
			int j = 0;
			while (i < fieldsHeaders.size()) {
				DynO4WSParamField field = fieldsHeaders.get(i);
				if (field.getId() == null) {
					field.setId(ids.get(j));
					j++;
				}
				i++;
			}
		}

		return param;
	}

	public DynO4WSParam onEntityOUTParam(OffloadingContext context) {
		Object target = context.getTarget();
		List<String> fieldsList = getFieldsList(target);
		OffloadingStrategy strategy = context.getStrategy();
		OffloadingRepository repository = context.getRepository();
		String requestNodeId = context.getNodeId();
		String endpoint = context.getEndpoint().getEndpointInfo().getAddress();
		List<String> offloadableFields = strategy.getParamOffloadableFields(
				requestNodeId, endpoint, context.getOperationName(), context.getParameterName(),
				target.getClass().getName(), fieldsList,
				new ArrayList<Integer>());
		DynO4WSParam param = new DynO4WSParam();
		param.setName(context.getParameterName());

		List<OffloadedField> fieldsToOffload = new ArrayList<OffloadedField>();

		prepareOffloading(context, target, param, offloadableFields,
				fieldsToOffload);
		// long offloadingTime = System.nanoTime();
		if (fieldsToOffload.size() > 0) {

			List<String> ids = repository.offloadParamFields(
					context.getNodeId(), context.getCallId(), context.getParameterName(),
					fieldsToOffload);
			// offloadingTime = System.nanoTime() - offloadingTime;
			List<DynO4WSParamField> fieldsHeaders = param.getFields();
			for (int i = 0; i < fieldsHeaders.size(); i++) {
				fieldsHeaders.get(i).setId(ids.get(i));
			}
		}
		// ps.println(offloadingTime);
		return param;

	}

	public DynO4WSParam onProxyOUTParam(OffloadingContext context) {
		DynO4WSProxy proxy = (DynO4WSProxy) context.getTarget();
		Object target = proxy.getContext().getTarget();
		List<String> fieldsList = getFieldsList(target);
		OffloadingStrategy strategy = context.getStrategy();
		OffloadingRepository repository = context.getRepository();
		String requestNodeId = context.getNodeId();
		String endpoint = context.getEndpoint().getEndpointInfo().getAddress();
		List<String> offloadableFields = strategy.getParamOffloadableFields(
				requestNodeId, endpoint, context.getOperationName(), context.getParameterName(),
				target.getClass().getName(), fieldsList,
				new ArrayList<Integer>());
		// FIXME Add fields sizes to the list passed to the strategy
		DynO4WSParam parameterData = proxy.getContext().getParameterData();

		DynO4WSParam param = new DynO4WSParam();
		param.setName(context.getParameterName());

		for (DynO4WSParamField field : parameterData.getFields()) {
			param.getFields().add(field);
			offloadableFields.remove(field.getName());
		}

		List<OffloadedField> fieldsToOffload = new ArrayList<OffloadedField>();
		prepareOffloading(context, target, param, offloadableFields,
				fieldsToOffload);
		if (fieldsToOffload.size() > 0) {
			List<String> ids = repository.offloadParamFields(
					context.getNodeId(), context.getCallId(), context.getParameterName(),
					fieldsToOffload);
			List<DynO4WSParamField> fieldsHeaders = param.getFields();
			int i = 0;
			int j = 0;
			while (i < fieldsHeaders.size()) {
				DynO4WSParamField field = fieldsHeaders.get(i);
				if (field.getId() == null) {
					field.setId(ids.get(j));
					j++;
				}
				i++;
			}
		}

		return param;
	}

	private void prepareOffloading(OffloadingContext context, Object target,
			DynO4WSParam param, List<String> offloadableFields,
			List<OffloadedField> fieldsToOffload) {
		// long serialization = System.nanoTime();

		for (String fieldname : offloadableFields) {
			Field parameterField;
			try {
				Class<? extends Object> resultClass = target.getClass();
				parameterField = resultClass.getDeclaredField(fieldname);
				boolean innerAccessible = parameterField.isAccessible();
				parameterField.setAccessible(true);
				Object fieldValue = parameterField.get(target);

				if (fieldValue != null) {
					if (!isJDKType(fieldValue.getClass())) {
						OffloadedField offloadedField = new OffloadedField();
						offloadedField.setName(fieldname);
						offloadedField.setTimestamp(System.nanoTime());
						offloadedField.setType(parameterField.getType()
								.getName());
						offloadedField.setData(serializeEntity(fieldValue));

						parameterField.set(target, null);

						DynO4WSParamField fieldHeader = new DynO4WSParamField();
						fieldHeader.setHost(context.getRepositoryEndpoint());
						fieldHeader.setName(fieldname);
						fieldHeader.setCallId(context.getCallId());
						param.getFields().add(fieldHeader);
						fieldsToOffload.add(offloadedField);
					}
					parameterField.setAccessible(innerAccessible);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// serialization = System.nanoTime() - serialization;
		// ps2.println(serialization);
	}

	private String serializeEntity(Object value) {
		StringWriter writer = new StringWriter();
		JAXB.marshal(value, writer);
		return writer.toString();
	}

	private List<String> getFieldsList(Object target) {
		List<String> result = new ArrayList<String>();
		for (Field f : target.getClass().getDeclaredFields()) {
			result.add(f.getName());
		}
		return result;
	}

	private boolean isJDKType(Class<? extends Object> clazz) {
		boolean result = false;
		result = clazz.isPrimitive();
		if (!result) {
			result = clazz.getName().startsWith("java.");
		}
		return result;
	}

}
