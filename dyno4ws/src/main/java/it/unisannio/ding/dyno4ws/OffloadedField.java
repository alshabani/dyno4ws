package it.unisannio.ding.dyno4ws;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;


@XmlRootElement(name="field")
public class OffloadedField {

	private String name;	
	
	private Object reference;
	
	private boolean copy = true;	
	
	private String type;

	private String encoding = "xml";
	
	private String data;
	
	private long timestamp;
	
	@XmlAttribute
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@XmlTransient
	public Object getReference() {
		return reference;
	}


	public void setReference(Object value) {
		this.reference = value;
	}


	@XmlAttribute(name="ts")
	public long getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}


	@XmlAttribute
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	@XmlAttribute
	public boolean isCopy() {
		return copy;
	}


	public void setCopy(boolean copy) {
		this.copy = copy;
	}

	@XmlAttribute(name="enc")
	public String getEncoding() {
		return encoding;
	}


	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	@XmlValue
	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}
	
	
	
	
}
