package it.unisannio.ding.wslink.dataevents.eventservice.test;

public enum TestRole {
	EVENT_SERVICE, PRODUCER, SUBSCRIBER, PRODUCER_SUBSCRIBER, ALL, EVENT_SERVICE_AND_SUBSCRIBER, EVENT_SERVICE_AND_PRODUCER
}
