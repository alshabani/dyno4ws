/**
 * 
 */
package it.unisannio.ding.wslink.api;

import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;

import java.util.Map;

/**
 * @author Quirino Zagarese
 *
 */
public class PureLazyStrategy implements WSLinkStrategy {

	/* (non-Javadoc)
	 * @see it.unisannio.ding.wslink.api.WSLinkStrategy#shouldOffload(java.lang.Object, it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext, java.util.Map)
	 */
	public boolean shouldOffload(Object field, OffloadingContext context,
			Map<String, String> strategyParams) {
		// It expects no params and should return false, regardless of the object value
		return false;
	}

}
