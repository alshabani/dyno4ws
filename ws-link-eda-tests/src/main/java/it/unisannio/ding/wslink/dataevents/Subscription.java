package it.unisannio.ding.wslink.dataevents;

import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class Subscription. It is similar to wsn Subscribe, but explicitly
 * enables topic and content based subscriptions. If an endpoint wants to
 * subscribe for several topics, it will have to create topic-based
 * subscriptions.
 * 
 * @author qzagarese
 */
public class Subscription {

	/** The type. */
	private SubscriptionType type;

	/** The content. */
	private String content;

	/** The query language. */
	private String queryLanguage;

	/** The subscriber endpoint. */
	private URL subscriberEndpoint;

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public SubscriptionType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(SubscriptionType type) {
		this.type = type;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the query language.
	 * 
	 * @return the query language
	 */
	public String getQueryLanguage() {
		return queryLanguage;
	}

	/**
	 * Sets the query language.
	 * 
	 * @param queryLanguage
	 *            the new query language
	 */
	public void setQueryLanguage(String queryLanguage) {
		this.queryLanguage = queryLanguage;
	}

	/**
	 * Gets the subscriber endpoint.
	 * 
	 * @return the subscriber endpoint
	 */
	public URL getSubscriberEndpoint() {
		return subscriberEndpoint;
	}

	/**
	 * Sets the subscriber endpoint.
	 * 
	 * @param subscriberEndpoint
	 *            the new subscriber endpoint
	 */
	public void setSubscriberEndpoint(URL subscriberEndpoint) {
		this.subscriberEndpoint = subscriberEndpoint;
	}
}
