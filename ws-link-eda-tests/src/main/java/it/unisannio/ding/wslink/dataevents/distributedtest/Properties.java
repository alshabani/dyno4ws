/**
 * Distributed Test Scenario for the DataService
 * @author aalshaba
 */
package it.unisannio.ding.wslink.dataevents.distributedtest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

/**
 * <b>Test Properties</b>> <br>
 * Setting default test properties: This calss will read the properties file
 * called es.properties to return the value
 * 
 * 
 * @author aalshaba
 * 
 */
public class Properties {

    public static final Logger logger =
            Logger.getLogger(Properties.class.getName());

    // Getting instance of the class Properties
    private volatile static Properties instance;

    // Port numbers
    public static final int PUBLISHER_PORT = 22222;
    public static final int SUBSCRIBER_PORT = 12223;

    // TOPIC on which the subscription and the publication will be based
    private String topic;

    // DataEvent size in MB
    private int dataevetSize = 1;
    // number of publishers
    private int numOfPublishers = 1;
    // number of subscribers
    private int numOfSubscribers = 1;
    // number of events
    private int numOfEvents = 1;
    // number of topics per publisher/subscriber
    private int numOfTopics = 1;

    private int eventGeneratorThreadPoolSize = 0;

    private int subscriberThreadPoolSize = 0;

    private int delayBetweenEvents = 0;
    
    // Private constructor. Class could not be instantiated
    private Properties() {
        this.topic = "DATA";
        this.setProperties();
    }

    /**
     * @return Unique instance of Properties class
     */
    public static Properties getInstance() {
        if (instance == null) {
            synchronized (Properties.class) {
                if (instance == null) {
                    instance = new Properties();
                }
            }
        }

        return instance;
    }

    public String getTopic() {
        return this.topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getHost() {
        String host = "localhost";
        try {
            host = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            logger.info("UnknownHostException has been thrown in method getHost(). Program will use localhost as host name");
        }
        return host;
    }

    /**
     * 
     * @return string representation of the hostname of the Eventservice
     *         hostname
     * 
     */
    public String getESHost() {
        String esHost = "localhost";
        String line;
        String esPattern = "es.host";
        String hostsFile = getHostFileLocation();

        try {
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(this.getClass()
                            .getResourceAsStream(hostsFile)));
            while ((line = br.readLine()) != null) {
                String[] tmp = line.split("=");
                String id = tmp[0].trim();
                String value = tmp[1].trim();
                if (id.compareTo(esPattern) == 0) {
                    esHost = value;
                    break;
                }
            }

            if (br != null) {
                br.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Hosts file does not exist, using localhost as hostname");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch (NullPointerException npException) {
            esHost = "localhost";
        }
        return esHost;
    }

    public String getConsumerEndPoint() {
        String consumerEndpoint =
                String.format("http://%s:9098/notificationConsumer ", getHost());
        return consumerEndpoint;
    }

    public int getDelayBetweenEvents() {
        return this.delayBetweenEvents;
    }
    
    public String getProducerEndPoint() {
        String producerEndpoint =
                String.format("http://%s:9099/notificationProducer", getHost());
        return producerEndpoint;
    }

    private String getHostFileLocation() {
        String esHostFile = null;
        try {
            esHostFile = System.getProperty("hosts");
        } catch (NullPointerException npException) {
            esHostFile = "hosts";
        }
        return esHostFile;
    }

    private void setProperties() {
        String esPropertiesFile = getPropertiesFile();
        String line;

        try {
            BufferedReader br =
                    new BufferedReader(new InputStreamReader(this.getClass()
                            .getResourceAsStream(esPropertiesFile)));

            while ((line = br.readLine()) != null) {
                String[] tmp = line.split("=");
                if (tmp.length == 1) {
                    continue;
                } else {
                    String key = tmp[0].trim();
                    String value = tmp[1].trim();
                    if (key.equals("dataevent.size")) {
                        setDataevetSize(Integer.parseInt(value));
                        continue;
                    }

                    else if (key.equals("publishers.num")) {
                        setNumOfPublishers(Integer.parseInt(value));
                        continue;
                    }

                    else if (key.equals("subscribers.num")) {
                        setNumOfSubscribers(Integer.parseInt(value));
                        continue;
                    }

                    else if (key.equals("eventgenerator.threadpool.size")) {
                        eventGeneratorThreadPoolSize = Integer.parseInt(value);
                        continue;
                    }

                    else if (key.equals("subscriber.threadpool.size")) {
                        subscriberThreadPoolSize = Integer.parseInt(value);
                        continue;
                    }

                    else if (key.equals("delay.between.publications")) {
                        delayBetweenEvents = Integer.parseInt(value);
                        continue;
                    }
                    
                    else if (key.equals("event.num")) {
                        setNumOfEvents(Integer.parseInt(value));
                    } else if (key.equals("topics.num")) {
                        setNumOfTopics(Integer.parseInt(value));
                    }
                }
            }

            if (br != null) {
                br.close();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Properties file \"es.properties\" as not found. Test will use default values");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } catch ( NullPointerException npException) {
        	System.out.println("Will use default value");
        }
       
    
    }

    private String getPropertiesFile() {
        String propertiesFile;
        try {
            propertiesFile = System.getProperty("properties");
        } catch (NullPointerException npException) {
            propertiesFile = "hosts";
        }
        if (propertiesFile == null) {
            propertiesFile = "es.properties";
        }

        System.out.println("Property file set to :" + propertiesFile);
        return propertiesFile;
    }

    /**
     * @return the dataevetSize
     */
    public int getDataEventSize() {
        return dataevetSize;
    }

    /**
     * @param dataevetSize
     *            the dataevetSize to set
     */
    public void setDataevetSize(int dataevetSize) {
        this.dataevetSize = dataevetSize;
    }

    /**
     * @return the numOfPublishers
     */
    public int getNumOfPublishers() {
        return numOfPublishers;
    }

    /**
     * @param numOfPublishers
     *            the numOfPublishers to set
     */
    public void setNumOfPublishers(int numOfPublishers) {
        this.numOfPublishers = numOfPublishers;
    }

    /**
     * @return the numOfSubscribers
     */
    public int getNumOfSubscribers() {
        return numOfSubscribers;
    }

    /**
     * @param numOfSubscribers
     *            the numOfSubscribers to set
     */
    public void setNumOfSubscribers(int numOfSubscribers) {
        this.numOfSubscribers = numOfSubscribers;
    }

    /**
     * @return the numOfEvents
     */
    public int getNumOfEvents() {
        return numOfEvents;
    }

    /**
     * @param numOfEvents
     *            the numOfEvents to set
     */
    public void setNumOfEvents(int numOfEvents) {
        this.numOfEvents = numOfEvents;
    }

    /**
     * @return the numOfTopics
     */
    public int getNumOfTopics() {
        return numOfTopics;
    }

    /**
     * @param numOfTopics
     *            the numOfTopics to set
     */
    public void setNumOfTopics(int numOfTopics) {
        this.numOfTopics = numOfTopics;
    }

    /**
     * 
     * 
     * @return the eventGeneratorThreadPoolSize
     */
    public int getEventGeneratorThreadPoolSize() {
        return this.eventGeneratorThreadPoolSize;
    }

    /**
     * 
     * 
     * @return the subscriberThreadPoolSize
     */
    public int getSubscriberThreadPoolSize() {
        return this.subscriberThreadPoolSize;
    }

}
