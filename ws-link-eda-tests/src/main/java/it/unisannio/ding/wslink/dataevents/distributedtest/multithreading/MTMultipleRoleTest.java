///**
// * 
// */
//package it.unisannio.ding.wslink.dataevents.distributedtest.multithreading;
//
//import it.unisannio.ding.wslink.api.WSLink;
//import it.unisannio.ding.wslink.dataevents.Subscription;
//import it.unisannio.ding.wslink.dataevents.SubscriptionType;
//import it.unisannio.ding.wslink.dataevents.distributedtest.EventGenerator;
//import it.unisannio.ding.wslink.dataevents.distributedtest.MultipleRoleTest;
//import it.unisannio.ding.wslink.dataevents.distributedtest.Properties;
//import it.unisannio.ding.wslink.dataevents.distributedtest.SubscriberWS;
//import it.unisannio.ding.wslink.dataevents.distributedtest.TestScenarios;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
//import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
//import it.unisannio.ding.wslink.dataevents.eventservice.core.BaseEventService;
//import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;
//
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.List;
//
//import org.apache.cxf.endpoint.Client;
//import org.apache.cxf.endpoint.Server;
//
///**
// * @author aalshaba
// *
// */
//public class MTMultipleRoleTest {
//
//	//public static final String EVENT_SERVICE_HOST = "10.42.0.83";
//	public String EVENT_SERVICE_HOST;
//	public int EVENT_SERVICE_CONSUMER_PORT_NUMBER;
//	public int EVENT_SERVICE_PRODUCER_PORT_NUMBER;
//	
//	//Endpoints of publisher and subscriber
//	public String EVENT_SERVICE_NOTIFICATION_ENDPOINT;
//	public String EVENT_SERVICE_SUBSCRIPTION_ENDPOINT;
//	
//	
//	public String topic;
//	
//	//to hold results in a csv file
//	public static final String CSV_PERFORMANCE_FILE = "results.csv";
//
//	public String SUBSCRIBER_HOST/* = "localhost"*/;
//	public static final int SUBSCRIBERS_BASE_PORT_NUMBER = 14000;
//
//	public String PUBLISHER_HOST /* = "localhost"*/;
//	public static final int PUBLISHER_BASE_PORT_NUMBER = 12005;
//
//	/*public static final String EVENT_SERVICE_NOTIFICATION_ENDPOINT = "http://"
//			+ EVENT_SERVICE_HOST + ":"
//			+ EVENT_SERVICE_CONSUMER_PORT_NUMBER + "/notificationConsumer";
//
//	public static final String EVENT_SERVICE_SUBSCRIPTION_ENDPOINT = "http://"
//			+ EVENT_SERVICE_HOST + ":"
//			+ EVENT_SERVICE_PRODUCER_PORT_NUMBER + "/notificationProducer";
//
//*/	// private final static Logger logger = Logger.getLogger(MultipleRoleTest.class
//	// .getSimpleName());
//	private static final int INITIAL_WAIT_PER_PRODUCER = 125;
//	private static final long DELAY_BETWEEN_SAME_TOPIC_EVENTS = 40000;
//
//	// private List<WSLink> subscribersWSLink;
//
//	// private List<Client> producersClients;
//	//
//	// private List<WSLink> producersWSLink;
//	//
//	// private List<Client> subscribersClients;
//	//
//	// private List<Server> subscribersServices;
//	
//	//Using list to hold multiple subscribers/publishers within the same host
//	//Useless when multiple subscribers/publishers exist in different hosts
//	private List<String> subscriberTopicList;
//
//	private List<String> producerTopicList;
//	
//	//Properties of the test
//	private int numberOfSubscribers;
//	private int NumberOfPublisher;
//	
//	//Size of the data event in MB
//	private int dataeventSize;
//	//Number of events
//	private int numberOfEvents;
//	
//
//	public MTMultipleRoleTest() {
//		// subscribersWSLink = new ArrayList<WSLink>();
//		// producersWSLink = new ArrayList<WSLink>();
//		// subscribersClients = new ArrayList<Client>();
//		// producersClients = new ArrayList<Client>();
//		// subscribersServices = new ArrayList<Server>();
//		//subscriberTopicList = new ArrayList<String>();
//		//producerTopicList = new ArrayList<String>();
//		
//		Properties properties = Properties.getInstance();
//		
//		setNumberOfSubscribers(properties.getNumOfSubscribers());
//		setNumberOfPublisher(properties.getNumOfPublishers());
//		setDataeventSize(properties.getDataevetSize());
//		setNumberOfEvents(properties.getNumOfEvents());
//		
//		topic = properties.getTopic();
//		
//		
//		//Getting hosts and port numbers, forming the endpoints, at the EventService Side;
//		EVENT_SERVICE_HOST = properties.getESHost();
//		EVENT_SERVICE_CONSUMER_PORT_NUMBER = Properties.PUBLISHER_PORT;
//		EVENT_SERVICE_PRODUCER_PORT_NUMBER = Properties.SUBSCRIBER_PORT;
//				
//		EVENT_SERVICE_NOTIFICATION_ENDPOINT =
//				String.format("http://%s:%s/notificationConsumer", EVENT_SERVICE_HOST, EVENT_SERVICE_CONSUMER_PORT_NUMBER );
//		EVENT_SERVICE_SUBSCRIPTION_ENDPOINT =
//				String.format("http://%s:%s/notificationProducer", EVENT_SERVICE_HOST, EVENT_SERVICE_PRODUCER_PORT_NUMBER );
//		
//		//Getting hosts for the Publisher and subscriber each at its own side
//		SUBSCRIBER_HOST = properties.getHost();
//		PUBLISHER_HOST = properties.getHost();
//		
//	}
//
//	public static void main(String[] args) {
//		MultipleRoleTest tester = new MultipleRoleTest();
//		
//		TestScenarios scenario1 = TestScenarios.ONE_TO_ONE;
//		
//		//TestRole machineRole = TestRole.ALL;
//		String machineRole = getRole();
//		//TODO use the Property's get methods to get the number of subs/pubs/data event size ,etc... 
//		Integer numberOfSubscribers = tester.getNumberOfSubscribers();
//		Integer numberOfProducers = tester.getNumberOfPublisher();
//		Integer numberOfSubscriberTopics = 1;
//		Integer numberOfProducerTopics = 1;
//		Integer numberOfEvents = tester.getNumberOfEvents();
//		Integer numberOfSubscribersPerTopic = 1;
//		//DataEventSize field gives the size in MB
//		Integer attachmentSizeInKB = tester.getDataeventSize() * 1024;
//		//Number of attachments per event is fixed to 1
//		Integer numberOfAttachmentsPerEvent = 1;
//		
//		//delay between two consecutive events
//		long delay = 0;
//		
//		/*Boolean[][] attachmentAccessMap = new Boolean[numberOfSubscribers][numberOfAttachmentsPerEvent];
//		for (int i = 0; i < numberOfSubscribers; i++)
//			for (int j = 0; j < numberOfAttachmentsPerEvent; j++)
//				attachmentAccessMap[i][j] = true;*/
//		tester.test(machineRole, numberOfSubscribers, numberOfProducers,
//				numberOfEvents, attachmentSizeInKB,
//				delay);
//	}
//
//	private static String getRole() {
//		String role;
//		role = System.getProperty("role");
//		if (role == null)
//		{
//			role = "ALL";
//		}
//		return role;
//	}
//
//	public void test(String role, int numberOfSubscribers,
//			int numberOfProducers, int numberOfEvents,
//			int attachmentSizeInKB,
//			long delay) {
//		if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("ES"))
//		{
//			startEventService();			
//		}
//		
//		if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("SUBSCRIBER"))
//		{
//			startSubscriber(numberOfSubscribers, numberOfProducers, numberOfEvents);
//		}
//		
//		if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("PUBLISHER"))
//		{
//			startPublisher(numberOfEvents, attachmentSizeInKB, delay);
//		}
//	}
//	
//	//Starting test with specific scenario
//	public void test(String role, TestScenarios ts)
//	{
//		
//	}
//
//	/**
//	 * 
//	 * @param numberOfEvents number of events to publish
//	 * @param attachmentSizeInKB size of the attachment in each event
//	 * @param delay intervals between two consecutive events
//	 */
//	private void startPublisher(int numberOfEvents, int attachmentSizeInKB, long delay)	
//	{
//		int port = this.PUBLISHER_BASE_PORT_NUMBER;
//		WSLink publisherWSLink = new WSLink("publisher", PUBLISHER_HOST, port);
//		
//		Client publisherClient = ServiceUtility.createWsClient(
//				DataEventConsumer.class,
//				EVENT_SERVICE_NOTIFICATION_ENDPOINT);
//		publisherWSLink.registerClient(publisherClient.getEndpoint());
//		String producerEndpoint = String.format("http://%s:%s/producer", PUBLISHER_HOST, port);
////		EventGenerator eventGenerator = new EventGnerator(producerEndpoint, topic, attachmentSizeInKB, delay);
//		
//		//TODO
//		int numberOfThreads = 5;
//		//Anticipating processing time to be 120 seconds
//		long processingTime = 120 * 1000;
//		//EventGenerator eventGenerator = new EventGenerator(producerEndpoint, topic, numberOfEvents, attachmentSizeInKB, delay, publisherClient);
//		MTEventGenerator eventGenerator = new MTEventGenerator(producerEndpoint, topic, attachmentSizeInKB, numberOfEvents, 0, 0, publisherClient, numberOfThreads, processingTime);
//		//TODO check this out!!!
//		new Thread(eventGenerator).start();
//		//eventGenerator.start();
//		
//	}
//	
//
//	private void startSubscriber(int numberOfSubscribers, int numberOfProducers, int numberOfEvents)
//	{
//		int port = SUBSCRIBERS_BASE_PORT_NUMBER;
//		String subscriberEndpoint = String.format("http://%s:%s/subscriber", SUBSCRIBER_HOST, port );
//		Client subscriberClient = ServiceUtility.createWsClient(
//				DataEventProducer.class,
//				EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
//		//SubscriberWS subscriberWS = new SubscriberWS(subscriberEndpoint, numberOfProducers, numberOfEvents);
//		MTSubscriberWS mtSubscriberWS = new 
//		Server subscriberService = ServiceUtility.createService(subscriberWS, subscriberEndpoint);
//		
//		WSLink subscriberWSLink = new WSLink("subscriber", SUBSCRIBER_HOST, port);
//		subscriberWSLink.registerClient(subscriberClient.getEndpoint());
//		subscriberWSLink.registerService(subscriberService.getEndpoint());
//		Subscription subscription = createSubscription(topic, subscriberEndpoint);
//		try {
//			subscriberClient.invoke("subscribe", new Object[] {subscription});
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	
//	private void startEventService() {
//		EventService eventService = new BaseEventService();
//		eventService.start(EVENT_SERVICE_NOTIFICATION_ENDPOINT,
//				EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
//		System.out.println("EVENT SERVICE IS UP AND RUNNING");
//	}
//	
//
//	private Subscription createSubscription(String topic, String endpoint) {
//		Subscription subscription = new Subscription();
//		subscription.setType(SubscriptionType.TOPIC_BASED);
//		subscription.setContent(topic);
//		try {
//			subscription.setSubscriberEndpoint(new URL(endpoint));
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return subscription;
//	}
//
//	/**
//	 * @return the numberOfSubscribers
//	 */
//	public int getNumberOfSubscribers() {
//		return numberOfSubscribers;
//	}
//
//	/**
//	 * @param numberOfSubscribers the numberOfSubscribers to set
//	 */
//	public void setNumberOfSubscribers(int numberOfSubscribers) {
//		this.numberOfSubscribers = numberOfSubscribers;
//	}
//
//	/**
//	 * @return the numberOfPublisher
//	 */
//	public int getNumberOfPublisher() {
//		return NumberOfPublisher;
//	}
//
//	/**
//	 * @param numberOfPublisher the numberOfPublisher to set
//	 */
//	public void setNumberOfPublisher(int numberOfPublisher) {
//		NumberOfPublisher = numberOfPublisher;
//	}
//
//	/**
//	 * @return the dataeventSize
//	 */
//	public int getDataeventSize() {
//		return dataeventSize;
//	}
//
//	/**
//	 * @param dataeventSize the dataeventSize to set
//	 */
//	public void setDataeventSize(int dataeventSize) {
//		this.dataeventSize = dataeventSize;
//	}
//
//	/**
//	 * @return the numberOfEvents
//	 */
//	public int getNumberOfEvents() {
//		return numberOfEvents;
//	}
//
//	/**
//	 * @param numberOfEvents the numberOfEvents to set
//	 */
//	public void setNumberOfEvents(int numberOfEvents) {
//		this.numberOfEvents = numberOfEvents;
//	}
//	
//
//}
