package it.unisannio.ding.wslink.dataevents.eventservice.test;

import it.unisannio.ding.wslink.api.WSLink;
import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.SubscriptionType;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
import it.unisannio.ding.wslink.dataevents.eventservice.core.BaseEventService;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;
import it.unisannio.ding.wslink.dataevents.eventservice.test.TestRole;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;

public class WsLinkTest {
	//public static final String EVENT_SERVICE_IP_ADDRESS = "10.42.0.83";
	public static final String EVENT_SERVICE_IP_ADDRESS = "localhost";
	public static final int EVENT_SERVICE_CONSUMER_PORT_NUMBER = 9998;
	public static final int EVENT_SERVICE_PRODUCER_PORT_NUMBER = 9999;

	public static final String BASE_TOPIC_NAME = "TOPIC";

	public static final String CSV_PERFORMANCE_FILE = "results.csv";

	//public static final String SUBSCRIBERS_IP_ADDRESS = "10.42.0.57";
	 public static final String SUBSCRIBERS_IP_ADDRESS = "localhost";
	public static final int SUBSCRIBERS_BASE_PORT_NUMBER = 14000;

	//public static final String PRODUCERS_IP_ADDRESS = "10.42.0.1";
	public static final String PRODUCERS_IP_ADDRESS = "localhost";
	public static final int PRODUCERS_BASE_PORT_NUMBER = 12005;

	public static final String EVENT_SERVICE_NOTIFICATION_ENDPOINT = "http://"
			+ EVENT_SERVICE_IP_ADDRESS + ":"
			+ EVENT_SERVICE_CONSUMER_PORT_NUMBER + "/notificationConsumer";

	public static final String EVENT_SERVICE_SUBSCRIPTION_ENDPOINT = "http://"
			+ EVENT_SERVICE_IP_ADDRESS + ":"
			+ EVENT_SERVICE_PRODUCER_PORT_NUMBER + "/notificationProducer";

	// private final static Logger logger = Logger.getLogger(WsLinkTest.class
	// .getSimpleName());
	private static final int INITIAL_WAIT_PER_PRODUCER = 125;
	private static final long DELAY_BETWEEN_SAME_TOPIC_EVENTS = 400;

	// private List<WSLink> subscribersWSLink;

	// private List<Client> producersClients;
	//
	// private List<WSLink> producersWSLink;
	//
	// private List<Client> subscribersClients;
	//
	// private List<Server> subscribersServices;

	private List<String> subscriberTopicList;

	private List<String> producerTopicList;

	public WsLinkTest() {
		// subscribersWSLink = new ArrayList<WSLink>();
		// producersWSLink = new ArrayList<WSLink>();
		// subscribersClients = new ArrayList<Client>();
		// producersClients = new ArrayList<Client>();
		// subscribersServices = new ArrayList<Server>();
		subscriberTopicList = new ArrayList<String>();
		producerTopicList = new ArrayList<String>();
	}

	public static void main(String[] args) {
		WsLinkTest tester = new WsLinkTest();
		TestRole machineRole = TestRole.ALL;
		Integer numberOfSubscribers = 1;
		Integer numberOfProducers = 1;
		Integer numberOfSubscriberTopics = 1;
		Integer numberOfProducerTopics = 1;
		Integer numberOfEventsPerProducerPerTopic = 30;
		Integer numberOfSubscribersPerTopic = 1;
		Integer attachmentSizeInKB = 100 * 1024;
		Integer numberOfAttachmentsPerEvent = 1;
		Boolean[][] attachmentAccessMap = new Boolean[numberOfSubscribers][numberOfAttachmentsPerEvent];
		for (int i = 0; i < numberOfSubscribers; i++)
			for (int j = 0; j < numberOfAttachmentsPerEvent; j++)
				attachmentAccessMap[i][j] = true;
		tester.test(machineRole, numberOfSubscribers, numberOfProducers,
				numberOfSubscriberTopics, numberOfProducerTopics,
				numberOfEventsPerProducerPerTopic, numberOfSubscribersPerTopic,
				attachmentSizeInKB, attachmentAccessMap);
	}

	public void test(TestRole role, Integer numberOfSubscribers,
			Integer numberOfProducers, Integer numberOfSubscriberTopics,
			Integer numberOfProducerTopics, Integer numberOfEventsPerTopic,
			Integer numberOfSubscribersPerTopic, Integer attachmentSizeInKB,
			Boolean[][] attachmentAccessMap) {
		if (role.equals(TestRole.EVENT_SERVICE)
				|| role.equals(TestRole.EVENT_SERVICE_AND_SUBSCRIBER)
				|| role.equals(TestRole.EVENT_SERVICE_AND_PRODUCER)
				|| role.equals(TestRole.ALL)) {
			startEventService();
		}

		if (role.equals(TestRole.SUBSCRIBER)
				|| role.equals(TestRole.EVENT_SERVICE_AND_SUBSCRIBER)
				|| role.equals(TestRole.ALL)) {
			startSubscribers(numberOfSubscribers, numberOfSubscriberTopics,
					numberOfSubscribersPerTopic, attachmentAccessMap);
		}

		if (role.equals(TestRole.PRODUCER)
				|| role.equals(TestRole.EVENT_SERVICE_AND_PRODUCER)
				|| role.equals(TestRole.ALL)) {
			startProducers(numberOfProducers, numberOfProducerTopics,
					numberOfEventsPerTopic, attachmentSizeInKB);
		}
	}

	private void startProducers(Integer numberOfProducers,
			Integer numberOfProducerTopics, Integer numberOfEventsPerTopic,
			Integer attachmentSizeInKB) {
		createTopics(numberOfProducerTopics, this.producerTopicList);
		for (int i = 0; i < numberOfProducers; i++) {
			int port = PRODUCERS_BASE_PORT_NUMBER + i;
			WSLink producerWSLink = new WSLink("publisher" + i,
					PRODUCERS_IP_ADDRESS, port);

			Client producerClient = ServiceUtility.createWsClient(
					DataEventConsumer.class,
					EVENT_SERVICE_NOTIFICATION_ENDPOINT);
			producerWSLink.registerClient(producerClient.getEndpoint());
			// producersClients.add(producerClient);
			// producersWSLink.add(producerWSLink);

			String producerEndpoint = "http://" + PRODUCERS_IP_ADDRESS + ":"
					+ port + "/producer" + i;
			for (int j = 0; j < numberOfProducerTopics; j++) {
				String topic = producerTopicList.get(j);
				long delayBetweenEventsInMilliseconds = DELAY_BETWEEN_SAME_TOPIC_EVENTS;
				// sequential wait
				long initialDelayInMilliseconds = INITIAL_WAIT_PER_PRODUCER * j
						* numberOfEventsPerTopic + INITIAL_WAIT_PER_PRODUCER
						* i * numberOfEventsPerTopic * numberOfProducerTopics;
				EventGenerator eg = new EventGenerator(producerEndpoint, topic,
						attachmentSizeInKB, numberOfEventsPerTopic,
						initialDelayInMilliseconds,
						delayBetweenEventsInMilliseconds, producerClient);
				new Thread(eg).start();
			}
		}
	}

	private void startSubscribers(Integer numberOfSubscribers,
			Integer numberOfSubscriberTopics,
			Integer numberOfSubscribersPerTopic, Boolean[][] attachmentAccessMap) {
		createTopics(numberOfSubscriberTopics, subscriberTopicList);
		for (int i = 0; i < numberOfSubscribers; i++) {
			int port = SUBSCRIBERS_BASE_PORT_NUMBER + i;
			String subscriberEndpoint = "http://" + SUBSCRIBERS_IP_ADDRESS
					+ ":" + port + "/subscriber" + i;

			Client subscriptionClient = ServiceUtility.createWsClient(
					DataEventProducer.class,
					EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
			// subscribersClients.add(subscriptionClient);
			SubscriberService subscriberService = new SubscriberService(
					subscriberEndpoint, attachmentAccessMap[i],
					CSV_PERFORMANCE_FILE);
			Server subscriptionService = ServiceUtility.createService(
					subscriberService, subscriberEndpoint);
			// subscribersServices.add(subscriptionService);

			WSLink subscriberWSLink = new WSLink("subscriber" + i,
					SUBSCRIBERS_IP_ADDRESS, port);

			// subscribersWSLink.add(subscriberWSLink);

			subscriberWSLink.registerClient(subscriptionClient.getEndpoint());
			subscriberWSLink.registerService(subscriptionService.getEndpoint());

			for (int j = 0; j < numberOfSubscriberTopics; j++) {
				String topic = subscriberTopicList.get(j);
				Subscription s = createSubscription(topic, subscriberEndpoint);
				try {
					subscriptionClient.invoke("subscribe", new Object[] { s });
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void startEventService() {
		EventService eventService = new BaseEventService();
		eventService.start(EVENT_SERVICE_NOTIFICATION_ENDPOINT,
				EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
		System.out.println("EVENT SERVICE IS UP AND RUNNING");
	}

	private void createTopics(Integer numberOfTopics, List<String> topicList) {
		if (numberOfTopics == null)
			return;
		for (int i = 0; i < numberOfTopics; i++) {
			topicList.add(BASE_TOPIC_NAME + i);
		}
	}

	private Subscription createSubscription(String topic, String endpoint) {
		Subscription subscription = new Subscription();
		subscription.setType(SubscriptionType.TOPIC_BASED);
		subscription.setContent(topic);
		try {
			subscription.setSubscriberEndpoint(new URL(endpoint));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return subscription;
	}
}
