package it.unisannio.ding.wslink.dataevents.eventservice.core;

import it.unisannio.ding.wslink.api.WSLink;
import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.NotificationManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class BaseNotificationManager implements NotificationManager {

    public static final Logger logger =
            Logger.getLogger(BaseNotificationManager.class.getName());

    private Map<String, DataEventConsumer> endpointToClient =
            new HashMap<String, DataEventConsumer>();

    private Map<String, List<Subscription>> topicToSubscriptions =
            new HashMap<String, List<Subscription>>();

    private WSLink offloadingManager;

    public BaseNotificationManager(WSLink offloadingManager) {
        this.offloadingManager = offloadingManager;
    }

    private DataEventConsumer createClient(URL subscriberEndpoint) {
        DataEventConsumer result = null;
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(DataEventConsumer.class);
        factory.setAddress(subscriberEndpoint.toExternalForm());
        result = (DataEventConsumer) factory.create();
        Endpoint endpoint = ClientProxy.getClient(result).getEndpoint();
        // endpoint.getOutInterceptors().add(new LoggingOutInterceptor());
        if (offloadingManager != null) {
            offloadingManager.registerClient(endpoint);
        }
        return result;
    }

    @Override
    public synchronized void addSubscription(Subscription subscription) {
        URL subscriberEndpoint = subscription.getSubscriberEndpoint();
        DataEventConsumer client =
                endpointToClient.get(subscriberEndpoint.toExternalForm());
        if (client == null) {
            client = createClient(subscriberEndpoint);
            endpointToClient.put(subscriberEndpoint.toExternalForm(), client);
        }
        List<Subscription> list =
                topicToSubscriptions.get(subscription.getContent());
        if (list == null) {
            list = new ArrayList<Subscription>();
            topicToSubscriptions.put(subscription.getContent(), list);
        }
        list.add(subscription);
    }

    @Override
    public void removeSubscription(Subscription subscription) {
        // TODO Auto-generated method stub

    }

    @Override
    public void deliverEvent(DataEvent event, Subscription s) {
        long eventReceptionTime = System.currentTimeMillis();
        long timeToReceiveEvent = eventReceptionTime - event.getTimestamp();

        logger.info("Received event with publicationTime="
                + event.getTimestamp() + ", it tooks " + timeToReceiveEvent
                + " ms");

        String subscriberEndpoint = s.getSubscriberEndpoint().toExternalForm();

        DataEventConsumer consumer = endpointToClient.get(subscriberEndpoint);
        consumer.notify(event);

        logger.info("Forwarded event with publicationTime="
                + event.getTimestamp() + " to subscriber " + subscriberEndpoint
                + " in " + (System.currentTimeMillis() - eventReceptionTime)
                + " ms");
    }

    @Override
    public List<Subscription> getSubscriptionsFor(String topicExpression) {
        return topicToSubscriptions.get(topicExpression);
    }

}
