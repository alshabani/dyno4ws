package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Deflater;

import javax.xml.bind.JAXB;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

public class MongoGridFSDBRepository implements OffloadingRepository {

    private Deflater compresser = new Deflater();

    protected String dbname = "wslink";

    private Map<String, OffloadedField> references = new HashMap<String, OffloadedField>();

    protected DBCollection coll;

    public MongoGridFSDBRepository() {
        MongoClient m = null;
        try {
            m = new MongoClient("localhost");
            // the save method will not complete until the 
            // write operation has completed
            m.setWriteConcern(WriteConcern.ACKNOWLEDGED);

            DB db = m.getDB(dbname);
            coll = db.getCollection("offloadedfield");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (MongoException e) {
            throw new RuntimeException(e);
        }
    }

    public List<OffloadedField> retrieveFieldsById(String id, ProfileData data) {
        long timestamp1 = System.currentTimeMillis();
        List<OffloadedField> result = new ArrayList<OffloadedField>();
        String cid = id.substring(id.indexOf(",cid:") + 5);
        cid = cid.substring(0, cid.indexOf(","));
        GridFS gridFS = new GridFS(coll.getDB(), "offloaded_field");
        List<GridFSDBFile> fields = gridFS.find(cid);

        for (GridFSDBFile gridFile : fields) {
            OffloadedField f = new OffloadedField();
            DBObject dbObject = gridFile.getMetaData();
            f.setName((String) dbObject.get("name"));
            f.setTimestamp((Long) dbObject.get("timestamp"));
            f.setType((String) dbObject.get("type"));

            byte[] content = null;
            try {
                InputStream stream = gridFile.getInputStream();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024 * 1024];
                int read = stream.read(buffer);
                while (read != -1) {
                    out.write(buffer, 0, read);
                    read = stream.read(buffer);
                }
                content = out.toByteArray();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            f.setData(new String(content));
            result.add(f);
        }

        long timestamp2 = System.currentTimeMillis();
        double timeElapsed = (timestamp2 - timestamp1) / 1000d;
        System.out.println("Time to retrieve and send back offloaded field is "
                + timeElapsed);
        return result;
    }

    private void compress(OffloadedField item) {
        byte[] input = null;
        input = item.getData().getBytes();
        // Compress the bytes
        compresser.setInput(input);
        compresser.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

        byte[] buf = new byte[1024];
        while (!compresser.finished()) {
            int count = compresser.deflate(buf);
            bos.write(buf, 0, count);
        }
        try {
            bos.close();
        } catch (IOException e) {
        }

        // Get the compressed data
        byte[] compressedData = bos.toByteArray();
        // String data = Base64.encodeBase64String(compressedData);
        item.setData(new String(compressedData));
    }

    protected void serializeItem(OffloadedField item) {
        if (item.getEncoding().equalsIgnoreCase("xml")) {
            toXML(item);
        } else if (item.getEncoding().equalsIgnoreCase("gzip_xml")) {
            toXML(item);
            compress(item);
        }
    }

    private void toXML(OffloadedField item) {
        StringWriter writer = new StringWriter();
        JAXB.marshal(item.getReference(), writer);
        item.setData(writer.toString());
    }

    public List<String> offloadParamFields(String nodeId, String invocationId,
            String paramName, List<OffloadedField> fields) {
        List<String> result = new ArrayList<String>();
        for (OffloadedField f : fields) {
            String key = createKey(nodeId, invocationId, paramName, f.getName());
            if (f.isCopy()) {
                if (f.getData() == null) {
                    serializeItem(f);
                }
                GridFS gfsField = new GridFS(coll.getDB(), "offloaded_field");
                GridFSInputFile gfsFile = gfsField.createFile(f.getData()
                        .getBytes());
                DBObject object = new BasicDBObject();
                object.put("idkey", key);
                object.put("name", f.getName());
                object.put("timestamp", f.getTimestamp());
                object.put("type", f.getType());
                gfsFile.setMetaData(object);
                gfsFile.setFilename(invocationId);
                gfsFile.save();
            } else {
                references.put(key, f);
            }

            result.add(key);
        }
        return result;
    }

    public void setStrategyEndpoint(String endpoint) {
        // TODO Auto-generated method stub

    }

    private String createKey(String nodeId, String invocationId,
            String paramName, String name) {
        return "{nodeId:" + nodeId + ",cid:" + invocationId + ",param:"
                + paramName + ",name:" + name + "}";
    }

    public String getDbname() {
        return dbname;
    }

    public void setDbname(String dbname) {
        this.dbname = dbname;
    }

}