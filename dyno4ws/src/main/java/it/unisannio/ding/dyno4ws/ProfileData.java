package it.unisannio.ding.dyno4ws;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="pdata")
public class ProfileData {

	private long timestamp;
	
	private List<AccessData> access = new ArrayList<AccessData>(); 
	
	private List<MiscElement> misc = new ArrayList<MiscElement>();

	
	@XmlAttribute(name="t")
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@XmlElement(name="access")
	public List<AccessData> getAccess() {
		return access;
	}

	public void setAccess(List<AccessData> access) {
		this.access = access;
	}
	
	@XmlElement(name="misc")
	public List<MiscElement> getMisc() {
		return misc;
	}

	public void setMisc(List<MiscElement> misc) {
		this.misc = misc;
	}
	
}
