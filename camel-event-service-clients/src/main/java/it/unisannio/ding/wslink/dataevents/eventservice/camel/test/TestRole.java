package it.unisannio.ding.wslink.dataevents.eventservice.camel.test;

public enum TestRole {
	PRODUCER, SUBSCRIBER, PRODUCER_SUBSCRIBER, ALL
}
