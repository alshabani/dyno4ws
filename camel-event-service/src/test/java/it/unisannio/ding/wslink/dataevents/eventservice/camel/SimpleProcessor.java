package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SimpleProcessor implements Processor, ApplicationContextAware{

	private String endpoint;
	private ApplicationContext context;
	private ProducerTemplate camelTemplate;

	public SimpleProcessor(String endpoint){
		this.endpoint = endpoint;
	}

	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		this.context = arg0;
		camelTemplate = context.getBean("camelTemplate",
                ProducerTemplate.class);
	}
	
	@Override
	public void process(Exchange exchange) throws Exception {
		camelTemplate.sendBody(endpoint, exchange.getIn());
	}
	
}
