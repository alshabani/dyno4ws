package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoException;

public class MongoDBRepository implements OffloadingRepository {

	protected String dbname = "icws";
	
	protected DBCollection coll;

	public MongoDBRepository() {
		Mongo m = null;
		try {
			m = new Mongo("localhost");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DB db = m.getDB(dbname);
		coll = db.getCollection("offloadedfield");

	}

	
	public List<OffloadedField> retrieveFieldsById(String id, ProfileData data) {
		List<OffloadedField> result = new ArrayList<OffloadedField>();
		BasicDBObject query = new BasicDBObject();
		query.put("idkey", id);
		DBCursor cur = coll.find(query);

		while (cur.hasNext()) {
			DBObject next = cur.next();
			OffloadedField f = new OffloadedField();
			f.setName((String) next.get("name"));
			f.setTimestamp((Long) next.get("timestamp"));
			f.setType((String) next.get("type"));
			f.setData((String) next.get("data"));
			result.add(f);
		}
		return result;
	}

	
	public List<String> offloadParamFields(String nodeId, String invocationId,
			String paramName, List<OffloadedField> fields) {
		List<String> result = new ArrayList<String>();
		List<DBObject> docs = new ArrayList<DBObject>();
		for (OffloadedField f : fields) {
			String key = createKey(nodeId, invocationId, paramName, f.getName());
			DBObject object = new BasicDBObject();
			object.put("idkey", key);
			object.put("name", f.getName());
			object.put("timestamp", f.getTimestamp());
			object.put("data", f.getData());
			object.put("type", f.getType());
			docs.add(object);
			result.add(key);
		}
		save(docs);
		return result;
	}

	protected void save(List<DBObject> docs) {
		coll.insert(docs);
	}


	public void setStrategyEndpoint(String endpoint) {
		// TODO Auto-generated method stub

	}

	private String createKey(String nodeId, String invocationId,
			String paramName, String name) {
		return "{nodeId:" + nodeId + ",cid:" + invocationId + ",param:"
				+ paramName + ",name:" + name + "}";
	}


	public String getDbname() {
		return dbname;
	}


	public void setDbname(String dbname) {
		this.dbname = dbname;
	}

}
