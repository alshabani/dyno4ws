package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.model.RouteDefinition;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

@Service("subscriptionReceiver")
public class SubscriptionReceiver implements ApplicationContextAware {

	private ModelCamelContext context;

	private ConfigurableApplicationContext applicationContext;

	Map<String, List<String>> topicToEndpoints = new HashMap<String, List<String>>();
	Map<String, RouteDefinition> topicToRouteDefinition = new HashMap<String, RouteDefinition>();

	public void onSubscription(Subscription subscription) {
		System.out.println("Received subscription for topic: "
				+ subscription.getContent() + " from endpoint: "
				+ subscription.getSubscriberEndpoint());

		try {
			DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) applicationContext
					.getBeanFactory();

			// create proxy bean factory definition from subscription data
			GenericBeanDefinition factoryDefinition = new GenericBeanDefinition();
			factoryDefinition.setBeanClass(JaxWsProxyFactoryBean.class);
			MutablePropertyValues properties = new MutablePropertyValues();
			properties
					.add("serviceClass",
							"it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer");
			properties.add("address", subscription.getSubscriberEndpoint()
					.toExternalForm());
			factoryDefinition.setPropertyValues(properties);
			String factoryBeanName = "subscriberProxyFactory-"
					+ subscription.getContent() + "-"
					+ subscription.getSubscriberEndpoint();
			beanFactory.registerBeanDefinition(factoryBeanName,
					factoryDefinition);

			// create proxy bean definition from subscription data
			GenericBeanDefinition proxyDefinition = new GenericBeanDefinition();

			proxyDefinition.setBeanClass(DataEventConsumer.class);
			proxyDefinition.setFactoryBeanName(factoryBeanName);
			proxyDefinition.setLazyInit(true);
			proxyDefinition.setFactoryMethodName("create");
			String subscriberProxyName = "subscriberProxy-"
					+ subscription.getContent() + "-"
					+ subscription.getSubscriberEndpoint();
			beanFactory.registerBeanDefinition(subscriberProxyName,
					proxyDefinition);

			// create the proxy bean endpoint string route
			String route = "bean:" + subscriberProxyName + "?method=notify";

			// get the string routes for the subscribed topic
			List<String> routes = topicToEndpoints.get(subscription
					.getContent());

			if (routes == null) {
				routes = new ArrayList<String>();
				topicToEndpoints.put(subscription.getContent(), routes);
			}

			if (!routes.contains(route)) {
				// add the new route if not present
				routes.add(route);

				// shutdown and remove the old multicast route definition if
				// present
				RouteDefinition oldRouteDefinition = topicToRouteDefinition
						.get(subscription.getContent());
				if (oldRouteDefinition != null) {
					String oldRouteId = oldRouteDefinition.getId();
					context.shutdownRoute(oldRouteId);
					context.removeRoute(oldRouteId);
				}

				// create the new one for the new string route set in parallel
				// mode
				RouteDefinition routeDefinition = new RouteDefinition()
						.from("vm:" + subscription.getContent());
				String[] routesArray = routes.toArray(new String[0]);
				routeDefinition.multicast().parallelProcessing()
						.to(routesArray);

				// start everything
				routeDefinition.autoStartup(true);
				System.out.println("Routes for topic "
						+ subscription.getContent() + " are: " + routes);
				System.out
						.println("Route definition for topic "
								+ subscription.getContent() + " is: "
								+ routeDefinition);

				context.addRouteDefinition(routeDefinition);

				// update maps
				topicToEndpoints.put(subscription.getContent(), routes);
				topicToRouteDefinition.put(subscription.getContent(),
						routeDefinition);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setModelContext(ModelCamelContext context) {
		this.context = context;
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		this.applicationContext = (ConfigurableApplicationContext) arg0;
	}

}
