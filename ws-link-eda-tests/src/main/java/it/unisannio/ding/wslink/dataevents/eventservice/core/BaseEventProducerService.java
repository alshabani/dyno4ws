package it.unisannio.ding.wslink.dataevents.eventservice.core;

import javax.jws.WebService;

import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.eventservice.NotificationManager;

@WebService(serviceName = "DataEventProducer", portName = "DataEventProducerPort", name = "DataEventProducerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer")
public class BaseEventProducerService implements DataEventProducer {
    private NotificationManager notificationManager;

    public BaseEventProducerService(NotificationManager manager) {
        this.notificationManager = manager;
    }

    @Override
    public void subscribe(Subscription s) {
        notificationManager.addSubscription(s);
    }

    @Override
    public void unsubscribe(Subscription s) {
        notificationManager.removeSubscription(s);
    }
}
