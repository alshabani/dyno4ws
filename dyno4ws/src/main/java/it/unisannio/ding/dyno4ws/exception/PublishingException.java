package it.unisannio.ding.dyno4ws.exception;

public class PublishingException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8475817040439518718L;

	public PublishingException(String message){
		super(message);
	}
	
}
