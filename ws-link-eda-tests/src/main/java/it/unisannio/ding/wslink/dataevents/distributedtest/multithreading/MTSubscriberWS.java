/**
 * 
 */
package it.unisannio.ding.wslink.dataevents.distributedtest.multithreading;
import java.util.ArrayList;
import java.util.List;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;

import javax.jws.WebService;

@WebService(serviceName = "DataEventConsumer", portName = "DataEventConsumerPort", name = "DataEventConsumerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
public class MTSubscriberWS implements DataEventConsumer {

	private long processingTime;

	private int numberOfThreads;

	boolean threadsStarted = false;

	private int threadToScheduleIndex = 0;

	List<SubscriberThread> threads = new ArrayList<SubscriberThread>();

//	private SimpleOutputWriter writer;

	public MTSubscriberWS(long processingTime, int numberOfThreads) {
		this.processingTime = processingTime;
		this.numberOfThreads = numberOfThreads;
//		this.writer = writer;
	}

	@Override
	public void notify(DataEvent event) {
		synchronized (this) {

			if (!threadsStarted) {
				startThreads(event);
				threadsStarted = true;
			} else {
				scheduleEvent(event);
			}
		}

	}

	private void scheduleEvent(DataEvent event) {
		threads.get(threadToScheduleIndex).submitEvent(event);
		threadToScheduleIndex = (threadToScheduleIndex + 1) % numberOfThreads;
	}

	private void startThreads(DataEvent event) {
		for (int i = 0; i < numberOfThreads; i++) {
			SubscriberThread st = new SubscriberThread("Thread[" + i + "]",
					event, this.processingTime);
			threads.add(st);
			st.start();
		}
	}

}
