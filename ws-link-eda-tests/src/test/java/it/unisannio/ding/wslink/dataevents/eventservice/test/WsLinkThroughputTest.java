package it.unisannio.ding.wslink.dataevents.eventservice.test;

import it.unisannio.ding.wslink.api.WSLink;
import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.SubscriptionType;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
import it.unisannio.ding.wslink.dataevents.eventservice.core.BaseEventService;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;

public class WsLinkThroughputTest {
    public static final String EVENT_SERVICE_IP_ADDRESS = "10.42.0.83";
    // public static final String EVENT_SERVICE_IP_ADDRESS = "localhost";
    public static final int EVENT_SERVICE_CONSUMER_PORT_NUMBER = 9098;
    public static final int EVENT_SERVICE_PRODUCER_PORT_NUMBER = 9099;

    public static final String BASE_TOPIC_NAME = "TOPIC";

    public static final String CSV_PERFORMANCE_FILE = "results.csv";

    public static final String SUBSCRIBERS_IP_ADDRESS = "10.42.0.57";
    // public static final String SUBSCRIBERS_IP_ADDRESS = "localhost";

    public static final int SUBSCRIBERS_BASE_PORT_NUMBER = 10000;

    public static final String PRODUCERS_IP_ADDRESS = "10.42.0.1";
    // public static final String PRODUCERS_IP_ADDRESS = "localhost";

    public static final int PRODUCERS_BASE_PORT_NUMBER = 12000;

    public static final String EVENT_SERVICE_NOTIFICATION_ENDPOINT = "http://"
            + EVENT_SERVICE_IP_ADDRESS + ":"
            + EVENT_SERVICE_CONSUMER_PORT_NUMBER + "/notificationConsumer";

    public static final String EVENT_SERVICE_SUBSCRIPTION_ENDPOINT = "http://"
            + EVENT_SERVICE_IP_ADDRESS + ":"
            + EVENT_SERVICE_PRODUCER_PORT_NUMBER + "/notificationProducer";

    // private final static Logger logger = Logger.getLogger(WsLinkTest.class
    // .getSimpleName());
    private static final int INITIAL_WAIT_PER_PRODUCER = 0;
    private static final long DELAY_BETWEEN_SAME_TOPIC_EVENTS = 5000;

    // private List<WSLink> subscribersWSLink;

    // private List<Client> producersClients;
    //
    // private List<WSLink> producersWSLink;
    //
    // private List<Client> subscribersClients;
    //
    // private List<Server> subscribersServices;

    private List<String> subscriberTopicList;

    private List<String> producerTopicList;

    public WsLinkThroughputTest() {
        // subscribersWSLink = new ArrayList<WSLink>();
        // producersWSLink = new ArrayList<WSLink>();
        // subscribersClients = new ArrayList<Client>();
        // producersClients = new ArrayList<Client>();
        // subscribersServices = new ArrayList<Server>();
        subscriberTopicList = new ArrayList<String>();
        producerTopicList = new ArrayList<String>();
    }

    public static void main(String[] args) {
        WsLinkThroughputTest tester = new WsLinkThroughputTest();
        TestRole machineRole = TestRole.PRODUCER;
        String producerBaseName = "producer at " + PRODUCERS_IP_ADDRESS;
        String subscriberBaseName = "subscriber at " + SUBSCRIBERS_IP_ADDRESS;
        Integer numberOfSubscribers = 1;
        Integer numberOfProducers = 1;
        Integer numberOfSubscriberTopics = 1;
        Integer numberOfProducerTopics = 1;
        Integer numberOfEventsPerProducerPerTopic = 100000;
        Integer numberOfSubscribersPerTopic = 1;
        Integer attachmentSizeInKB = 50 * 1024;
        Integer numberOfAttachmentsPerEvent = 1;
        int numberOfThreads = 1;
        long processingTime = 120 * 1000;

        Boolean[][] attachmentAccessMap =
                new Boolean[numberOfSubscribers][numberOfAttachmentsPerEvent];
        for (int i = 0; i < numberOfSubscribers; i++)
            for (int j = 0; j < numberOfAttachmentsPerEvent; j++)
                attachmentAccessMap[i][j] = true;
        tester.test(
                machineRole, numberOfSubscribers, numberOfProducers,
                subscriberBaseName, producerBaseName, numberOfSubscriberTopics,
                numberOfProducerTopics, numberOfEventsPerProducerPerTopic,
                numberOfSubscribersPerTopic, attachmentSizeInKB,
                attachmentAccessMap, numberOfThreads, processingTime);
    }

    public void test(TestRole role, Integer numberOfSubscribers,
                     Integer numberOfProducers, String subscriberBaseName,
                     String producerBaseName, Integer numberOfSubscriberTopics,
                     Integer numberOfProducerTopics,
                     Integer numberOfEventsPerTopic,
                     Integer numberOfSubscribersPerTopic,
                     Integer attachmentSizeInKB,
                     Boolean[][] attachmentAccessMap, int numberOfThreads,
                     long processingTime) {
        if (role.equals(TestRole.EVENT_SERVICE)
                || role.equals(TestRole.EVENT_SERVICE_AND_SUBSCRIBER)
                || role.equals(TestRole.EVENT_SERVICE_AND_PRODUCER)
                || role.equals(TestRole.ALL)) {
            startEventService();
        }

        if (role.equals(TestRole.SUBSCRIBER)
                || role.equals(TestRole.EVENT_SERVICE_AND_SUBSCRIBER)
                || role.equals(TestRole.ALL)) {
            startSubscribers(
                    subscriberBaseName, numberOfSubscribers,
                    numberOfSubscriberTopics, numberOfSubscribersPerTopic,
                    processingTime, numberOfThreads, attachmentAccessMap);
        }

        if (role.equals(TestRole.PRODUCER)
                || role.equals(TestRole.EVENT_SERVICE_AND_PRODUCER)
                || role.equals(TestRole.ALL)) {
            startProducers(
                    producerBaseName, numberOfProducers,
                    numberOfProducerTopics, numberOfEventsPerTopic,
                    numberOfThreads, processingTime, attachmentSizeInKB);
        }
    }

    private void startProducers(String producerBaseName,
                                Integer numberOfProducers,
                                Integer numberOfProducerTopics,
                                Integer numberOfEventsPerTopic,
                                Integer numberOfThreads, long processingTime,
                                Integer attachmentSizeInKB) {
        createTopics(numberOfProducerTopics, this.producerTopicList);
        for (int i = 0; i < numberOfProducers; i++) {
            int port = PRODUCERS_BASE_PORT_NUMBER + i;
            WSLink producerWSLink =
                    new WSLink(producerBaseName + i, PRODUCERS_IP_ADDRESS, port);

            Client producerClient =
                    ServiceUtility.createWsClient(
                            DataEventConsumer.class,
                            EVENT_SERVICE_NOTIFICATION_ENDPOINT);
            producerWSLink.registerClient(producerClient.getEndpoint());
            // producersClients.add(producerClient);
            // producersWSLink.add(producerWSLink);

            String producerEndpoint =
                    "http://" + PRODUCERS_IP_ADDRESS + ":" + port + "/producer"
                            + i;
            for (int j = 0; j < numberOfProducerTopics; j++) {
                String topic = producerTopicList.get(j);
                long delayBetweenEventsInMilliseconds =
                        DELAY_BETWEEN_SAME_TOPIC_EVENTS;
                // sequential wait
                long initialDelayInMilliseconds =
                        INITIAL_WAIT_PER_PRODUCER * j * numberOfEventsPerTopic
                                + INITIAL_WAIT_PER_PRODUCER * i
                                * numberOfEventsPerTopic
                                * numberOfProducerTopics;
                ThroughputEventGenerator eg =
                        new ThroughputEventGenerator(
                                producerEndpoint, topic, attachmentSizeInKB,
                                numberOfEventsPerTopic,
                                initialDelayInMilliseconds,
                                delayBetweenEventsInMilliseconds,
                                producerClient, numberOfThreads, processingTime);
                new Thread(eg).start();
            }
        }
    }

    private void startSubscribers(String subscriberBaseName,
                                  Integer numberOfSubscribers,
                                  Integer numberOfSubscriberTopics,
                                  Integer numberOfSubscribersPerTopic,
                                  long processingTime, int numberOfThreads,
                                  Boolean[][] attachmentAccessMap) {
        createTopics(numberOfSubscriberTopics, subscriberTopicList);
        for (int i = 0; i < numberOfSubscribers; i++) {
            int port = SUBSCRIBERS_BASE_PORT_NUMBER + i;
            String subscriberEndpoint =
                    "http://" + SUBSCRIBERS_IP_ADDRESS + ":" + port
                            + "/subscriber" + i;

            Client subscriptionClient =
                    ServiceUtility.createWsClient(
                            DataEventProducer.class,
                            EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
            // subscribersClients.add(subscriptionClient);
            SimpleOutputWriter writer =
                    new SimpleOutputWriter(CSV_PERFORMANCE_FILE);
            ThroughputSubscriberService subscriberService =
                    new ThroughputSubscriberService(
                            processingTime, numberOfThreads, writer);
            Server subscriptionService =
                    ServiceUtility.createService(
                            subscriberService, subscriberEndpoint);
            // subscribersServices.add(subscriptionService);

            WSLink subscriberWSLink =
                    new WSLink(
                            subscriberBaseName + i, SUBSCRIBERS_IP_ADDRESS,
                            port);

            // subscribersWSLink.add(subscriberWSLink);

            subscriberWSLink.registerClient(subscriptionClient.getEndpoint());
            subscriberWSLink.registerService(subscriptionService.getEndpoint());

            for (int j = 0; j < numberOfSubscriberTopics; j++) {
                String topic = subscriberTopicList.get(j);
                Subscription s = createSubscription(topic, subscriberEndpoint);
                try {
                    subscriptionClient.invoke("subscribe", new Object[] {s});
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    private void startEventService() {
        EventService eventService = new BaseEventService();
        eventService.start(
                EVENT_SERVICE_NOTIFICATION_ENDPOINT,
                EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
        System.out.println("EVENT SERVICE IS UP AND RUNNING");
    }

    private void createTopics(Integer numberOfTopics, List<String> topicList) {
        if (numberOfTopics == null)
            return;
        for (int i = 0; i < numberOfTopics; i++) {
            topicList.add(BASE_TOPIC_NAME + i);
        }
    }

    private Subscription createSubscription(String topic, String endpoint) {
        Subscription subscription = new Subscription();
        subscription.setType(SubscriptionType.TOPIC_BASED);
        subscription.setContent(topic);
        try {
            subscription.setSubscriberEndpoint(new URL(endpoint));
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return subscription;
    }
}
