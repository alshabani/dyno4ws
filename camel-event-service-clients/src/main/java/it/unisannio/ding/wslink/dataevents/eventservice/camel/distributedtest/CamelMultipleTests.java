/**
 * 
 */
package it.unisannio.ding.wslink.dataevents.eventservice.camel.distributedtest;

/**
 * @author aalshaba
 *
 */

import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.SubscriptionType;
import it.unisannio.ding.wslink.dataevents.distributedtest.EventGenerator;
import it.unisannio.ding.wslink.dataevents.distributedtest.Properties;
import it.unisannio.ding.wslink.dataevents.distributedtest.SubscriberWS;
import it.unisannio.ding.wslink.dataevents.distributedtest.TestScenarios;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;

public class CamelMultipleTests {	//public static final String EVENT_SERVICE_HOST = "10.42.0.83";
	public String EVENT_SERVICE_HOST;
	public int EVENT_SERVICE_CONSUMER_PORT_NUMBER;
	public int EVENT_SERVICE_PRODUCER_PORT_NUMBER;
	
	//Endpoints of publisher and subscriber
	public String EVENT_SERVICE_NOTIFICATION_ENDPOINT;
	public String EVENT_SERVICE_SUBSCRIPTION_ENDPOINT;
	
	
	public String topic;
	
	//to hold results in a csv file
	public static final String CSV_PERFORMANCE_FILE = "results.csv";

	public String SUBSCRIBER_HOST/* = "localhost"*/;
	public static final int SUBSCRIBERS_BASE_PORT_NUMBER = 12000;

	public String PUBLISHER_HOST /* = "localhost"*/;
	public static final int PUBLISHER_BASE_PORT_NUMBER = 14000;

	private List<String> subscriberTopicList;

	private List<String> producerTopicList;
	
	//Properties of the test
	private int numberOfSubscribers;
	private int NumberOfPublisher;
	
	//Size of the data event in MB
	private int dataeventSize;
	//Number of events
	private int numberOfEvents;
	

	public CamelMultipleTests() {
		
		Properties properties = Properties.getInstance();
		
		setNumberOfSubscribers(properties.getNumOfSubscribers());
		setNumberOfPublisher(properties.getNumOfPublishers());
		setDataeventSize(properties.getDataEventSize());
		setNumberOfEvents(properties.getNumOfEvents());
		
		topic = properties.getTopic();
		
		
		//Getting hosts and port numbers, forming the endpoints, at the EventService Side;
		EVENT_SERVICE_HOST = properties.getESHost();
		EVENT_SERVICE_CONSUMER_PORT_NUMBER = 9999;
		EVENT_SERVICE_PRODUCER_PORT_NUMBER = 9998;
				
		EVENT_SERVICE_NOTIFICATION_ENDPOINT =
				String.format("http://%s:%s/eventService", EVENT_SERVICE_HOST, EVENT_SERVICE_CONSUMER_PORT_NUMBER );
		EVENT_SERVICE_SUBSCRIPTION_ENDPOINT =
				String.format("http://%s:%s/subscriptionService", EVENT_SERVICE_HOST, EVENT_SERVICE_PRODUCER_PORT_NUMBER );
		
		//Getting hosts for the Publisher and subscriber each at its own side
		SUBSCRIBER_HOST = properties.getHost();
		PUBLISHER_HOST = properties.getHost();
		
		

	}

	public static void main(String[] args) {
		CamelMultipleTests tester = new CamelMultipleTests();
		
		TestScenarios scenario1 = TestScenarios.ONE_TO_ONE;
		
		//TestRole machineRole = TestRole.ALL;
		String machineRole = getRole();
		//TODO use the Property's get methods to get the number of subs/pubs/data event size ,etc... 
		Integer numberOfSubscribers = tester.getNumberOfSubscribers();
		Integer numberOfProducers = tester.getNumberOfPublisher();
		Integer numberOfSubscriberTopics = 1;
		Integer numberOfProducerTopics = 1;
		Integer numberOfEvents = tester.getNumberOfEvents();
		Integer numberOfSubscribersPerTopic = 1;
		//DataEventSize field gives the size in MB
		Integer attachmentSizeInKB = tester.getDataeventSize() * 1024;
		//Number of attachments per event is fixed to 1
		Integer numberOfAttachmentsPerEvent = 1;
		
		//delay between two consecutive events
		long delay = 0;
		
		/*Boolean[][] attachmentAccessMap = new Boolean[numberOfSubscribers][numberOfAttachmentsPerEvent];
		for (int i = 0; i < numberOfSubscribers; i++)
			for (int j = 0; j < numberOfAttachmentsPerEvent; j++)
				attachmentAccessMap[i][j] = true;*/
		tester.test(machineRole, numberOfSubscribers, numberOfProducers,
				numberOfEvents, attachmentSizeInKB,
				delay);
	}

	private static String getRole() {
		String role;
		role = System.getProperty("role");
		if (role == null)
		{
			role = "PUBLISHER";
		}
		return role;
	}

	public void test(String role, int numberOfSubscribers,
			int numberOfProducers, int numberOfEvents,
			int attachmentSizeInKB,
			long delay) {
		if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("SUBSCRIBER"))
		{
			startSubscriber(numberOfSubscribers, numberOfProducers, numberOfEvents);
		}
		
		if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("PUBLISHER"))
		{
			startPublisher(numberOfEvents, attachmentSizeInKB, delay);
		}
	}
	
	//Starting test with specific scenario
	public void test(String role, TestScenarios ts)
	{
		
	}

	/**
	 * 
	 * @param numberOfEvents number of events to publish
	 * @param attachmentSizeInKB size of the attachment in each event
	 * @param delay intervals between two consecutive events
	 */
	private void startPublisher(int numberOfEvents, int attachmentSizeInKB, long delay)	
	{
		int port = PUBLISHER_BASE_PORT_NUMBER;
		
		Client publisherClient = ServiceUtility.createWsClient(
				DataEventConsumer.class,
				EVENT_SERVICE_NOTIFICATION_ENDPOINT);
		String producerEndpoint = String.format("http://%s:%s/producer", PUBLISHER_HOST, port);

		EventGenerator eventGenerator = new EventGenerator(producerEndpoint, topic, numberOfEvents, attachmentSizeInKB, delay, publisherClient, 0);
		//TODO check this out!!!
		new Thread(eventGenerator).start();
		
	}
	

	private void startSubscriber(int numberOfSubscribers, int numberOfProducers, int numberOfEvents)
	{
		int port = SUBSCRIBERS_BASE_PORT_NUMBER;
		String subscriberEndpoint = String.format("http://%s:%s/subscriber", SUBSCRIBER_HOST, port );
		Client subscriberClient = ServiceUtility.createWsClient(
				DataEventProducer.class,
				EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
		SubscriberWS subscriberWS = new SubscriberWS(subscriberEndpoint, numberOfProducers, numberOfEvents, 0);
		Server subscriberService = ServiceUtility.createService(subscriberWS, subscriberEndpoint);
		
//		WSLink subscriberWSLink = new WSLink("subscriber", SUBSCRIBER_HOST, port);
//		subscriberWSLink.registerClient(subscriberClient.getEndpoint());
//		subscriberWSLink.registerService(subscriberService.getEndpoint());
		Subscription subscription = createSubscription(topic, subscriberEndpoint);
		try {
			subscriberClient.invoke("subscribe", new Object[] {subscription});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	

	private Subscription createSubscription(String topic, String endpoint) {
		Subscription subscription = new Subscription();
		subscription.setType(SubscriptionType.TOPIC_BASED);
		subscription.setContent(topic);
		try {
			subscription.setSubscriberEndpoint(new URL(endpoint));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return subscription;
	}

	/**
	 * @return the numberOfSubscribers
	 */
	public int getNumberOfSubscribers() {
		return numberOfSubscribers;
	}

	/**
	 * @param numberOfSubscribers the numberOfSubscribers to set
	 */
	public void setNumberOfSubscribers(int numberOfSubscribers) {
		this.numberOfSubscribers = numberOfSubscribers;
	}

	/**
	 * @return the numberOfPublisher
	 */
	public int getNumberOfPublisher() {
		return NumberOfPublisher;
	}

	/**
	 * @param numberOfPublisher the numberOfPublisher to set
	 */
	public void setNumberOfPublisher(int numberOfPublisher) {
		NumberOfPublisher = numberOfPublisher;
	}

	/**
	 * @return the dataeventSize
	 */
	public int getDataeventSize() {
		return dataeventSize;
	}

	/**
	 * @param dataeventSize the dataeventSize to set
	 */
	public void setDataeventSize(int dataeventSize) {
		this.dataeventSize = dataeventSize;
	}

	/**
	 * @return the numberOfEvents
	 */
	public int getNumberOfEvents() {
		return numberOfEvents;
	}

	/**
	 * @param numberOfEvents the numberOfEvents to set
	 */
	public void setNumberOfEvents(int numberOfEvents) {
		this.numberOfEvents = numberOfEvents;
	}
	
	
}
