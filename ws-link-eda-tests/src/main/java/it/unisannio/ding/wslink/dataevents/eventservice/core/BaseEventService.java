package it.unisannio.ding.wslink.dataevents.eventservice.core;

import it.unisannio.ding.wslink.api.WSLink;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
import it.unisannio.ding.wslink.dataevents.eventservice.NotificationManager;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;

import org.apache.cxf.endpoint.Server;

public class BaseEventService implements EventService {
	private NotificationManager notificationManager;
	private WSLink eventServiceMiddleware;
	private DataEventConsumer consumer;
	private DataEventProducer producer;

	public BaseEventService() {
		this.eventServiceMiddleware = new WSLink(
				"eventServiceOffloadingMiddleware", "localhost", 12345);
		this.notificationManager = new BaseNotificationManager(
				eventServiceMiddleware);
	}

	@Override
	public void start(String consumerEndpoint, String producerEndpoint) {
		consumer = new BaseEventConsumerService(notificationManager);
		producer = new BaseEventProducerService(notificationManager);

		Server consumerService = ServiceUtility.createService(consumer,
				consumerEndpoint);
		Server producerService = ServiceUtility.createService(producer,
				producerEndpoint);
		eventServiceMiddleware.registerService(consumerService.getEndpoint());
		eventServiceMiddleware.registerService(producerService.getEndpoint());
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setNotificationManager(NotificationManager manager) {
		this.notificationManager = manager;
	}
}
