package it.unisannio.ding.wslink.dataevents.eventservice.test;

import it.unisannio.ding.wslink.dataevents.DataEvent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import org.apache.cxf.endpoint.Client;

public class GeneratorThread extends Thread {

	private BlockingQueue<DataEvent> events = new LinkedBlockingDeque<DataEvent>(
			1);
	private String name;
	private Client notificationClient;

	public GeneratorThread(String name, Client notificationClient) {
		this.name = name;
		System.out.println(this.name + " started");
		this.notificationClient = notificationClient;
	}

	public void run() {
		DataEvent event = null;
		do {
			try {
				event = events.poll(1, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (event != null) {
				try {
					notificationClient.invoke("notify", new Object[] { event });
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} while (event != null);
	}

	public void submitEvent(DataEvent event) {
		try {
			events.put(event);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
