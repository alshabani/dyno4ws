package it.unisannio.ding.wslink.dataevents;

// TODO: Auto-generated Javadoc
/**
 * The Enum AttachmentType.
 * @author qzagarese
 */
public enum AttachmentType {

	/** The FILE. */
	FILE,
	
	/** The INSTANCE. */
	INSTANCE;
	
}
