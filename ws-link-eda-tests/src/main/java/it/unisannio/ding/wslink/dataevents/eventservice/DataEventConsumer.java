package it.unisannio.ding.wslink.dataevents.eventservice;

import javax.jws.Oneway;
import javax.jws.WebService;

import it.unisannio.ding.wslink.dataevents.DataEvent;

@WebService
public interface DataEventConsumer {
	@Oneway
	public void notify(DataEvent event);
	
}
