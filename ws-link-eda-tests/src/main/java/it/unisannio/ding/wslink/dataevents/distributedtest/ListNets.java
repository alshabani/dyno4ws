package it.unisannio.ding.wslink.dataevents.distributedtest;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;

public class ListNets {

    public static String getHostAddress(String interfaceName) {
        Enumeration<InetAddress> enume =
                getNetworkInterface(interfaceName).getInetAddresses();

        if (enume == null) {
            return null;
        }

        while (enume.hasMoreElements()) {
            InetAddress inet = enume.nextElement();

            if (inet instanceof Inet4Address) {
                return inet.getHostAddress();
            }
        }

        return null;
    }

    public static NetworkInterface getNetworkInterface(String interfaceName) {
        Enumeration<NetworkInterface> nets;
        try {
            nets = NetworkInterface.getNetworkInterfaces();

            for (NetworkInterface netint : Collections.list(nets)) {
                if (netint.getName().equals(interfaceName)) {
                    return netint;
                }
            }
        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String args[]) throws SocketException {
        System.out.println(getHostAddress("eth0"));
    }

}
