package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import it.unisannio.ding.wslink.dataevents.DataEvent;

import org.apache.camel.ProducerTemplate;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service("notificationManager")
public class NotificationManager implements ApplicationContextAware {

	private ApplicationContext context;

	private ProducerTemplate camelTemplate;

	public void onEventProduced(DataEvent event) {
		camelTemplate.sendBody("vm:" + event.getTopicExpression(), event);
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		this.context = arg0;
		camelTemplate = context
				.getBean("camelTemplate", ProducerTemplate.class);
	}
}
