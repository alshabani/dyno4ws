package it.unisannio.ding.dyno4ws.proxy;

import it.unisannio.ding.dyno4ws.DynO4WSHeader;
import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.lang.reflect.Method;

public class ProxyContext {

	private DynO4WSProxy proxy;
	
	private Object target;

	private Method interceptedMethod;
	
	private Object[] args;

	private DynO4WSHeader offloadingData;
	
	private DynO4WSParam parameterData;

	private OffloadingRepository repository;

	private OffloadingStrategy strategy;
	
	private ParameterInterceptor interceptor;

	public ProxyContext() {

	}

	public ProxyContext(Object target, Method interceptedMethod,
			DynO4WSHeader offloadingData, OffloadingRepository repository,
			OffloadingStrategy strategy) {
		this.target = target;
		this.interceptedMethod = interceptedMethod;
		this.offloadingData = offloadingData;
		this.repository = repository;
		this.strategy = strategy;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	public Method getInterceptedMethod() {
		return interceptedMethod;
	}

	public void setInterceptedMethod(Method interceptedMethod) {
		this.interceptedMethod = interceptedMethod;
	}

	public DynO4WSHeader getOffloadingData() {
		return offloadingData;
	}

	public void setOffloadingData(DynO4WSHeader offloadingData) {
		this.offloadingData = offloadingData;
	}

	public OffloadingRepository getRepository() {
		return repository;
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository = repository;
	}

	public OffloadingStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public DynO4WSProxy getProxy() {
		return proxy;
	}

	public void setProxy(DynO4WSProxy proxy) {
		this.proxy = proxy;
	}

	public ParameterInterceptor getInterceptor() {
		return interceptor;
	}

	public void setInterceptor(ParameterInterceptor interceptor) {
		this.interceptor = interceptor;
	}

	public DynO4WSParam getParameterData() {
		return parameterData;
	}

	public void setParameterData(DynO4WSParam parameterData) {
		this.parameterData = parameterData;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}
	
}
