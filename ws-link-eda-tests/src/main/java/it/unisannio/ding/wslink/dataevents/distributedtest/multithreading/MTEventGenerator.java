/**
 * 
 */
package it.unisannio.ding.wslink.dataevents.distributedtest.multithreading;


import it.unisannio.ding.wslink.dataevents.AttachmentDescriptor;
import it.unisannio.ding.wslink.dataevents.AttachmentType;
import it.unisannio.ding.wslink.dataevents.DataEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.cxf.endpoint.Client;

public class MTEventGenerator implements Runnable {
	private String producerEndpoint;
	private String topic;
	private int numberOfEventsPerTopic;
	private long delayBetweenEventsInMilliseconds;
	private long initialDelayInMilliseconds;
	private Client eventServiceNotificationClient;
	private int attachmentSizeInKB;
	private int numberOfThreads;
	boolean threadsStarted = false;
	private int threadToScheduleIndex = 0;
	private long processingTime;
	List<GeneratorThread> threads = new ArrayList<GeneratorThread>();

	public MTEventGenerator(String producerEndpoint, String topic,
			int attachmentSizeInKB, int numberOfEventsPerTopic,
			long initialDelayInMilliseconds,
			long delayBetweenEventsInMilliseconds,
			Client eventServiceNotificationClient, int numberOfThreads,
			long processingTime) {
		this.producerEndpoint = producerEndpoint;
		this.topic = topic;
		this.attachmentSizeInKB = attachmentSizeInKB;
		this.numberOfEventsPerTopic = numberOfEventsPerTopic;
		this.delayBetweenEventsInMilliseconds = delayBetweenEventsInMilliseconds;
		this.initialDelayInMilliseconds = initialDelayInMilliseconds;
		this.eventServiceNotificationClient = eventServiceNotificationClient;
		this.numberOfThreads = numberOfThreads;
		this.processingTime = processingTime;
	}

	private void scheduleEvent(DataEvent event) {
		threads.get(threadToScheduleIndex).submitEvent(event);
		threadToScheduleIndex = (threadToScheduleIndex + 1) % numberOfThreads;
	}

	private void startThreads(DataEvent event) {
		for (int i = 0; i < numberOfThreads; i++) {
			GeneratorThread t = new GeneratorThread("generator-thread" + i,
					eventServiceNotificationClient);
			threads.add(t);
			t.start();
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		try {
			System.out.println("New producer started on endpoint "
					+ producerEndpoint);
			Thread.currentThread().sleep(initialDelayInMilliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long initTimestamp = System.currentTimeMillis();
		long currentTimestamp = System.currentTimeMillis();
		for (int i = 0; i < numberOfEventsPerTopic
				&& (currentTimestamp - initTimestamp) < processingTime; i++) {
			DataEvent event = createEvent(topic, attachmentSizeInKB);

			synchronized (this) {

				if (!threadsStarted) {
					startThreads(event);
					threadsStarted = true;
				} else {
					scheduleEvent(event);
				}
			}

			try {
				Thread.currentThread().sleep(delayBetweenEventsInMilliseconds);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			currentTimestamp = System.currentTimeMillis();
		}
	}

	private DataEvent createEvent(String topic, int attachmentSize) {
		DataEvent event = new DataEvent();
        event.setTopicExpression(topic);

        AttachmentDescriptor descriptor = new AttachmentDescriptor();
        descriptor.setType(AttachmentType.INSTANCE);
        descriptor.setBeginOffset(0);

        List<AttachmentDescriptor> descriptors =
                new ArrayList<AttachmentDescriptor>();
        descriptors.add(descriptor);
        event.setDescriptors(descriptors);

        byte[] data = generateAndFillData(attachmentSize);
        descriptor.setEndOffset(data.length);

        event.setAttachments(data);

        event.setTimestamp(System.currentTimeMillis());

        return event;

	}

	private byte[] generateAndFillData(int attachmentSize) {
		byte[] b = new byte[attachmentSize * 1024];
		new Random().nextBytes(b);
		return b;
	}
}