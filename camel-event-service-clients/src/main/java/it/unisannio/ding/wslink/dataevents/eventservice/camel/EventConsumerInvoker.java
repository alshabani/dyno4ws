package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import org.apache.cxf.endpoint.Client;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;

public class EventConsumerInvoker {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Client client = ServiceUtility.createWsClient(DataEventConsumer.class,
				"http://localhost:9999/eventService?wsdl");
		DataEvent event = new DataEvent();
		event.setTopicExpression("hello");
		try {
			client.invoke("notify",
					new Object[] { event });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
