package it.unisannio.ding.wslink.api;

import it.unisannio.ding.dyno4ws.DynO4WS;
import it.unisannio.ding.dyno4ws.repository.MongoGridFSDBRepository;
import it.unisannio.ding.wslink.core.WSLinkBaseParamInterceptor;
import it.unisannio.ding.wslink.core.WSLinkOffloadingInterceptor;
import it.unisannio.ding.wslink.core.WSLinkOffloadingStrategy;

import org.apache.cxf.endpoint.Endpoint;

public class WSLink {

	private DynO4WS dyno;

	public WSLink() {
		this(System.nanoTime() + "@DynO4WS");
	}

	public WSLink(String id) {
		this(id, "localhost");
	}

	public WSLink(String id, String host) {
		this(id, host, 12358);
	}

	public WSLink(String id, String host, int port) {
		dyno = new DynO4WS(id, host, port);
		dyno.setParameterInterceptor(new WSLinkBaseParamInterceptor());
		dyno.setOffloadingInterceptor(new WSLinkOffloadingInterceptor());
		dyno.setStrategy(new WSLinkOffloadingStrategy());
		// dyno.setRepository(new WSLinkRepository());
		dyno.setRepository(new MongoGridFSDBRepository());
	}

	public void registerService(Endpoint endpoint) {
		dyno.registerService(endpoint);
	}

	public void registerClient(Endpoint endpoint) {
		dyno.registerClient(endpoint);
	}

	public DynO4WS getDyno() {
		return dyno;
	}

	public void setDyno(DynO4WS dyno) {
		this.dyno = dyno;
	}

	public void shutdown() {
		this.dyno.shutdown();
	}

}
