package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;

import java.util.List;

import javax.jws.WebService;


@WebService
public interface OffloadingRepository {

	
	public List<OffloadedField> retrieveFieldsById(String id, ProfileData data);

	
	public List<String> offloadParamFields(String nodeId, String invocationId, String paramName,
			List<OffloadedField> fields);

	public void setStrategyEndpoint(String endpoint);
}
