package it.unisannio.ding.dyno4ws.interceptors.parameters;

import it.unisannio.ding.dyno4ws.proxy.ProxyContext;


public interface ParameterInterceptor {

	public Object onGet(ProxyContext context);
	
	public void onSet(ProxyContext context);
	
	public void onConstruct(ProxyContext context);
	
	public void onDestroy(ProxyContext context);
	
}
