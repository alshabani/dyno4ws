package it.unisannio.ding.dyno4ws.interceptors.parameters;

import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.DynO4WSParamField;
import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.proxy.ProxyContext;
import it.unisannio.ding.dyno4ws.repository.FieldRetriever;

import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.bind.JAXB;

public class BasicParameterInterceptor implements ParameterInterceptor {

	private static final Logger logger = Logger
			.getLogger(BasicParameterInterceptor.class.getSimpleName());

	public Object onGet(ProxyContext context) {
		Object result = null;
		Object target = context.getTarget();
		DynO4WSParam parameterData = context.getParameterData();

		String fieldName = getFieldName(context.getInterceptedMethod()
				.getName());
		DynO4WSParamField fieldData = findFieldData(fieldName,
				parameterData.getFields());

		if (fieldData != null) {

			// the field had been offloaded and should be retrieved
			parameterData.getFields().remove(fieldData);
			long time = System.currentTimeMillis();

			List<OffloadedField> fields = FieldRetriever.retrieveParamFieldBy(
					fieldData.getHost(), fieldData.getId(), null);

			time = System.currentTimeMillis() - time;
//			System.out.println(time);
			


			for (OffloadedField field : fields) {
				Object value = unserialize(field);
				Field declaredField = null;
				try {
					declaredField = target.getClass().getDeclaredField(
							field.getName());

					boolean accessible = declaredField.isAccessible();
					declaredField.setAccessible(true);
					declaredField.set(target, value);
					declaredField.setAccessible(accessible);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (field.getName().equals(fieldName)) {
					result = value;
				}
//				System.out.println(field.getData().length);
			}
			
		} else {
			// the field has been transferred and its value is in the proxied instance
			result = getFieldValue(fieldName, target);
		}
		return result;
	}


	private Object getFieldValue(String fieldName, Object target) {
		Object result = null;
		Field f = null;
		try {
			f = target.getClass().getDeclaredField(fieldName);
			boolean accessible = f.isAccessible();
			f.setAccessible(true);
			result = f.get(target);
			f.setAccessible(accessible);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return result;
	}

	protected Object unserialize(OffloadedField field) {
		Object result = null;
		if (field.getEncoding().equalsIgnoreCase("xml")) {
			fromXML(field);
		}
		// TODO Add support to json and other formats
		result = field.getReference();
		return result;
	}

	protected void fromXML(OffloadedField field) {
		Class<?> fieldType = null;

		try {
			fieldType = Class.forName(field.getType());

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (fieldType.isArray()) {
			field.setData(filterBrackets(field.getData()));
		}
		field.setReference(JAXB.unmarshal(new StringReader(field.getData()), fieldType));
	}

	private String filterBrackets(String value) {

		int firstOccur = value.indexOf("[]");
		value = value.substring(0, firstOccur)
				+ value.substring(firstOccur + 2, value.length());
		int lastOccur = value.lastIndexOf("[]");
		value = value.substring(0, lastOccur)
				+ value.substring(lastOccur + 2, value.length());
		return value;
	}

	private DynO4WSParamField findFieldData(String fieldName,
			List<DynO4WSParamField> fields) {
		for (int i = 0; i < fields.size(); i++) {
			DynO4WSParamField field = fields.get(i);
			if (field.getName().equals(fieldName)) {
				fields.remove(field);
				return field;
			}
		}
		return null;
	}

	private String getFieldName(String name) {
		name = name.substring(3, name.length());
		name = name.substring(0, 1).toLowerCase()
				+ name.substring(1, name.length());
		return name;
	}

	public void onConstruct(ProxyContext context) {
//		logger.log(Level.INFO, "Initialized proxy " + context.getProxy());
	}

	public void onDestroy(ProxyContext context) {
//		logger.log(Level.INFO, "Garbage collected proxy " + context.getProxy());
		try {
			super.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onSet(ProxyContext context) {

		Method interceptedMethod = context.getInterceptedMethod();
		try {
			interceptedMethod.invoke(context.getTarget(), context.getArgs());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
