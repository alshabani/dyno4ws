package it.unisannio.ding.wslink.dataevents.eventservice.test;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageLoader extends Component {

	/**
         * 
         */
	private static final long serialVersionUID = 1L;
	BufferedImage img;

	public void paint(Graphics g) {
		g.drawImage(img, 0, 0, null);
	}

	public ImageLoader(byte[] data) {
		try {
			img = ImageIO.read(new ByteArrayInputStream(data));
		} catch (IOException e) {
		}

	}

	public Dimension getPreferredSize() {
		if (img == null) {
			return new Dimension(100, 100);
		} else {
			return new Dimension(img.getWidth(null), img.getHeight(null));
		}
	}

}