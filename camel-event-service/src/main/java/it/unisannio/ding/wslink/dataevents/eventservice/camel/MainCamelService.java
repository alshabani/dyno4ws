/**
 * 
 */
package it.unisannio.ding.wslink.dataevents.eventservice.camel;


import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
/** 
 * A main program to start Camel and run as a server using Spring config file.
 */
public class MainCamelService {
	private Main main;
	public static final String configFileLocation="/spring-context.xml";	
	public static void main(String[] args) throws Exception {
		
		
		MainCamelService runner = new MainCamelService();
		runner.boot();

//		runner.run(configFileLocation);
//		runner.run(configFileLocation);
	}
	private void boot() throws Exception {
		main = new Main();
		main.enableHangupSupport();
		main.setApplicationContextUri(configFileLocation);

		main.run();			
		}
	/*	public void runwithCamelRoute(String className)
	{
		final CamelContext camelContext = new DefaultCamelContext();
		try {
			Class<?> cls = Class.forName(className);
			if (RouteBuilder.class.isAssignableFrom(cls))
			{
					Object obj = cls.newInstance();
					RouteBuilder rb = (RouteBuilder) obj;
					
					camelContext.addRoutes(rb);
					camelContext.start();
				
			}
		} catch (ClassNotFoundException e) {
			logger.info("Class not found: " + className);
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	private static Logger logger = LoggerFactory.getLogger(MainCamelService.class);
	public void run(String configFileLocation) throws Exception {
		
		final ConfigurableApplicationContext springContext = new FileSystemXmlApplicationContext(configFileLocation);
//		final ConfigurableApplicationContext springCPXMLContext = new ClassPathXmlApplicationContext(configFileLocation);
		
		// Register proper shutdown.
		Runtime.getRuntime().addShutdownHook(new Thread() { 
			@Override
			public void run() {
				try {
					//springContext.close();
					logger.info("Spring stopped.");
				} catch (Exception e) {
					logger.error("Failed to stop Spring.", e);
				}
			}
		});

		// Start spring
		logger.info("Camel EVENTSERVICE IS UP AND RUNNING.");
		
		// Wait for user to hit CRTL+C to stop the service
	}
}