package it.unisannio.ding.dyno4ws;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="param")
public class DynO4WSParam {

	private String name;
	
	private List<DynO4WSParamField> fields = new ArrayList<DynO4WSParamField>();

	@XmlAttribute(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="field")
	public List<DynO4WSParamField> getFields() {
		return fields;
	}

	public void setFields(List<DynO4WSParamField> fields) {
		this.fields = fields;
	}
	
}
