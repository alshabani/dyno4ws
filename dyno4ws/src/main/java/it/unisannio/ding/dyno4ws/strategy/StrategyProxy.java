package it.unisannio.ding.dyno4ws.strategy;

import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.ProfileData;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;

import java.net.URL;
import java.util.List;

import javax.jws.WebService;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

@WebService(endpointInterface = "it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy", serviceName = "OffloadingStrategy")
public class StrategyProxy implements OffloadingStrategy {

	private OffloadingStrategy strategy;

	public void setImplementingInstance(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public void setImplementingService(URL endpoint) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(OffloadingStrategy.class);
		factory.setAddress(endpoint.toExternalForm());
		strategy = (OffloadingStrategy) factory.create();
	}

	public void updateProfiling(String nodeId, ProfileData data) {
		if (strategy == null) {
			throw new RuntimeException("No strategy impl or service provided");
		} else {
			strategy.updateProfiling(nodeId, data);
		}
	}

	public List<String> getParamOffloadableFields(String nodeId,
			String endpoint, String operation, String parameterName,
			String parameterType, List<String> fieldsList,
			List<Integer> fieldsSizes) {
		List<String> result = null;
		if (strategy == null) {
			throw new RuntimeException("No strategy impl or service provided");
		} else {
			result = strategy.getParamOffloadableFields(nodeId, endpoint,
					operation, parameterName, parameterType, fieldsList,
					fieldsSizes);
		}
		return result;
	}

	public DynO4WSParam selectAndOffloadParamFields(String nodeId,
			String endpoint, String operation, String paramName, Object target, OffloadingContext context) {
		DynO4WSParam result = null;
		if (strategy == null) {
			throw new RuntimeException("No strategy impl or service provided");
		} else {
			result = strategy.selectAndOffloadParamFields(nodeId, endpoint,
					operation, paramName, target, context);
		}
		return result;

	}
}