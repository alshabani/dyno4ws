package it.unisannio.ding.dyno4ws.interceptors.offloading;

import org.apache.cxf.endpoint.Endpoint;

import it.unisannio.ding.dyno4ws.DynO4WSHeader;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

public class OffloadingContext {

	private String nodeId;
	
	private String callId;
	
	private Object target;
	
	private String parameterName;
	
	private String operationName;
	
	private DynO4WSHeader exchangeHeader; 
	
	private OffloadingRepository repository;

	private OffloadingStrategy strategy;

	private Endpoint endpoint;
	
	private String repositoryEndpoint;

	

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}

	public DynO4WSHeader getExchangeHeader() {
		return exchangeHeader;
	}

	public void setExchangeHeader(DynO4WSHeader exchangeHeader) {
		this.exchangeHeader = exchangeHeader;
	}

	public OffloadingRepository getRepository() {
		return repository;
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository = repository;
	}

	public OffloadingStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public String getRepositoryEndpoint() {
		return repositoryEndpoint;
	}

	public void setRepositoryEndpoint(String repositoryEndpoint) {
		this.repositoryEndpoint = repositoryEndpoint;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}
	
	
	
}
