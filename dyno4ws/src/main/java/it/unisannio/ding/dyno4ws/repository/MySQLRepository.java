package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MySQLRepository implements OffloadingRepository {

	private Connection con;

	public MySQLRepository() {
		con = null;

		String url = "jdbc:mysql://localhost:3306/";
		String db = "icws";
		String driver = "com.mysql.jdbc.Driver";
		String user = "root";
		String pass = "r04dtr1p";
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url + db, user, pass);
			con.setAutoCommit(false);// Disables auto-commit.
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public List<OffloadedField> retrieveFieldsById(String id, ProfileData data) {
		List<OffloadedField> result = new ArrayList<OffloadedField>();
		ResultSet query = null;
		try {
			PreparedStatement s = con
					.prepareStatement("SELECT * FROM offloadedfield o WHERE o.idkey = '"
							+ id + "'");
			query = s.executeQuery();
			while (query.next()) {
				OffloadedField f = new OffloadedField();
				f.setName(query.getString("name"));
				f.setTimestamp(query.getLong("timestamp"));
				f.setType(query.getString("type"));
				Blob blob = query.getBlob("value");
				byte[] blobContent = new byte[(int) blob.length()];
				blob.getBinaryStream().read(blobContent);
				f.setData(new String(blobContent));
				result.add(f);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	
	public List<String> offloadParamFields(String nodeId, String invocationId,
			String paramName, List<OffloadedField> fields) {
		List<String> result = new ArrayList<String>();
		try {
			Statement st = con.createStatement();
			for (OffloadedField f : fields) {
				String key = createKey(nodeId, invocationId, paramName,
						f.getName());
				String insert = "INSERT INTO offloadedfield VALUES( \"" + key
						+ "\",\"" + f.getName() + "\",'" + f.getData()
						+ "',\"" + f.getTimestamp() + "\",\"" + f.getType()
						+ "\")";
				// System.out.println(insert);
				st.addBatch(insert);
				result.add(key);
			}
			st.executeBatch();
			// System.out.println("saved");
			st.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO Auto-generated method stub
		return result;
	}

	
	public void setStrategyEndpoint(String endpoint) {
		// TODO Auto-generated method stub

	}

	private String createKey(String nodeId, String invocationId,
			String paramName, String name) {
		return "{nodeId:" + nodeId + ",cid:" + invocationId + ",param:"
				+ paramName + ",name:" + name + "}";
	}

}
