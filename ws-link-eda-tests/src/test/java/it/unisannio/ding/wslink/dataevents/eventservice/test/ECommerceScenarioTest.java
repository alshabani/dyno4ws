//package it.unisannio.ding.wslink.dataevents.eventservice.test;
//
//import it.unisannio.ding.wslink.api.WSLink;
//import it.unisannio.ding.wslink.dataevents.AttachmentDescriptor;
//import it.unisannio.ding.wslink.dataevents.AttachmentType;
//import it.unisannio.ding.wslink.dataevents.DataEvent;
//import it.unisannio.ding.wslink.dataevents.Subscription;
//import it.unisannio.ding.wslink.dataevents.SubscriptionType;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
//import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
//import it.unisannio.ding.wslink.dataevents.eventservice.core.BaseEventService;
//import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;
//
//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Logger;
//
//import org.apache.cxf.endpoint.Client;
//import org.apache.cxf.endpoint.Server;
//
//public class ECommerceScenarioTest {
//
//	public static final String USER_SUBSYSTEM_ENDPOINT = "http://localhost:9072/userSubsystem";
//
//	private static final String RECOMMENDER_ENDPOINT = "http://localhost:9073/recommender";
//
//	private final static Logger logger = Logger
//			.getLogger(DataEventSimpleTest.class.getSimpleName());
//
//	private static final String CONSUMER_ENDPOINT = "http://localhost:9098/notificationConsumer";
//
//	private static final String PRODUCER_ENDPOINT = "http://localhost:9099/notificationProducer";
//
//	private WSLink producerWSLink;
//
//	private WSLink userSubsystemWSLink;
//
//	private WSLink recommenderWSLink;
//
//	private Client eventServiceNotificationClient;
//
//	private Client userSubsystemSubscriptionClient;
//
//	private Client recommenderSubscriptionClient;
//
//	private Server userSubsystemService;
//
//	private Server recommenderService;
//
//	public ECommerceScenarioTest() {
//
//		producerWSLink = new WSLink("publisher", "localhost", 12222);
//		userSubsystemWSLink = new WSLink("userSubsystem", "localhost", 12223);
//		recommenderWSLink = new WSLink("recommender", "localhost", 12224);
//
//		eventServiceNotificationClient = ServiceUtility.createWsClient(
//				DataEventConsumer.class, CONSUMER_ENDPOINT);
//
//		producerWSLink.registerClient(eventServiceNotificationClient
//				.getEndpoint());
//
//		userSubsystemSubscriptionClient = ServiceUtility.createWsClient(
//				DataEventProducer.class, PRODUCER_ENDPOINT);
//
//		recommenderSubscriptionClient = ServiceUtility.createWsClient(
//				DataEventProducer.class, PRODUCER_ENDPOINT);
//
//		userSubsystemService = ServiceUtility.createService(
//				new UserSubsystemSubscriberService(), USER_SUBSYSTEM_ENDPOINT);
//
//		recommenderService = ServiceUtility.createService(
//				new RecommenderSubscriberService(), RECOMMENDER_ENDPOINT);
//
//		userSubsystemWSLink.registerClient(userSubsystemSubscriptionClient
//				.getEndpoint());
//		userSubsystemWSLink.registerService(userSubsystemService.getEndpoint());
//
//		recommenderWSLink.registerClient(recommenderSubscriptionClient
//				.getEndpoint());
//		recommenderWSLink.registerService(recommenderService.getEndpoint());
//	}
//
//	public static void main(String[] args) {
//		EventService eventService = new BaseEventService();
//		eventService.start(CONSUMER_ENDPOINT, PRODUCER_ENDPOINT);
//		logger.info("EVENT SERVICE IS UP AND RUNNING");
//		ECommerceScenarioTest tester = new ECommerceScenarioTest();
//		tester.test();
//
//		// cleanup resources
//		eventService.shutdown();
//	}
//
//	public void test() {
//		Subscription s = createSubscription("GRAPHICS", USER_SUBSYSTEM_ENDPOINT);
//		Subscription s2 = createSubscription("GRAPHICS", RECOMMENDER_ENDPOINT);
//		try {
//			userSubsystemSubscriptionClient.invoke("subscribe",
//					new Object[] { s });
//
//			recommenderSubscriptionClient.invoke("subscribe",
//					new Object[] { s2 });
//			DataEvent event = createEvent(s.getContent());
//			eventServiceNotificationClient.invoke("notify",
//					new Object[] { event });
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private Subscription createSubscription(String topic, String endpoint) {
//		Subscription subscription = new Subscription();
//		subscription.setType(SubscriptionType.TOPIC_BASED);
//		subscription.setContent(topic);
//		try {
//			subscription.setSubscriberEndpoint(new URL(endpoint));
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return subscription;
//	}
//
//	private DataEvent createEvent(String topic) {
//		DataEvent result = new DataEvent();
//		result.setTopicExpression(topic);
//
//		AttachmentDescriptor descriptor = new AttachmentDescriptor();
//		descriptor.setFilename("image.jpg");
//		descriptor.setType(AttachmentType.FILE);
//		descriptor.setMimetype("image/jpg");
//		descriptor.setBeginOffset(0);
//		List<AttachmentDescriptor> descriptors = new ArrayList<AttachmentDescriptor>();
//		descriptors.add(descriptor);
//		result.setDescriptors(descriptors);
//
//		// read the attachment
//		byte[] data = null;
//		try {
//			InputStream stream = inputStreamFrom("/image.jpg");
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			byte[] buffer = new byte[1024];
//			int read = stream.read(buffer);
//			while (read != -1) {
//				out.write(buffer, 0, read);
//				read = stream.read(buffer);
//			}
//			data = out.toByteArray();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		descriptor.setEndOffset(data.length);
//		result.setAttachments(data);
//		descriptor.setEndOffset(data.length);
//		result.setAttachments(data);
//		return result;
//	}
//
//	private static InputStream inputStreamFrom(String file) {
//		InputStream is = null;
//
//		if (file != null) {
//			is = DataEventSimpleTest.class.getResourceAsStream(file);
//		}
//
//		// InputStream is = null;
//		// try {
//		// is = new URL("http://sampleurl/pic.jpg").openStream();
//		// } catch (MalformedURLException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// } catch (IOException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// }
//		return is;
//	}
//}
