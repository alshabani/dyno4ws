package it.unisannio.ding.dyno4ws.interceptors;

import it.unisannio.ding.dyno4ws.DynO4WSHeader;
import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.proxy.DynO4WSProxy;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.lang.reflect.Field;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMSource;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageContentsList;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Node;

public class OutgoingResponseInterceptor extends
		AbstractPhaseInterceptor<Message> implements DynO4WSCXFInterceptor {

	private String nodeId;

	private String repositoryEndpoint;

	private OffloadingStrategy strategy;

	private OffloadingRepository repository;

	private Endpoint endpoint;

	private ParameterInterceptor parameterInterceptor;

	private OffloadingInterceptor offloadingInterceptor;

//	private PrintStream ps;

	public OutgoingResponseInterceptor() {
		super(Phase.USER_LOGICAL);
		
//		try {
//			ps = new PrintStream(new FileOutputStream(new File("OutgoingResponseInterceptor.txt")));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}


	public void handleMessage(Message message) throws Fault {
	
		
//		long ts = System.nanoTime();

		
		MessageContentsList o = (MessageContentsList) message
				.getContent(List.class);

		SoapMessage soap = (SoapMessage) message;

		SoapMessage inMessage = (SoapMessage) soap.getExchange().getInMessage();

		Header header = inMessage.getHeader(new QName("dyno4ws"));

		if (header != null) {
			DOMSource source = new DOMSource((Node) header.getObject());
			DynO4WSHeader requestHeader = JAXB.unmarshal(source,
					DynO4WSHeader.class);

			Object resultWrapper = o.get(0);
			Object target = null;
			try {
				Class<? extends Object> class1 = resultWrapper.getClass();
				Field[] declaredFields = class1.getDeclaredFields();
				for (Field f : declaredFields) {
					if (f.getName().equals("_return")) {
						boolean accessible = f.isAccessible();
						f.setAccessible(true);
						target = f.get(resultWrapper);
						f.setAccessible(accessible);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			OffloadingContext context = new OffloadingContext();
			context.setCallId(requestHeader.getCallId());
			context.setEndpoint(endpoint);
			context.setNodeId(nodeId);
			context.setParameterName("_result");
			context.setRepository(repository);
			context.setRepositoryEndpoint(repositoryEndpoint);
			context.setStrategy(strategy);
			context.setTarget(target);
			context.setOperationName(resultWrapper.getClass().getSimpleName());
			context.setExchangeHeader(requestHeader);
			DynO4WSHeader responseHeader = new DynO4WSHeader();
			responseHeader.setCallId(requestHeader.getCallId());
			responseHeader.setNodeId(nodeId);

			DynO4WSParam paramHeader = null;
			if (target != null) {
				if (target instanceof DynO4WSProxy) {
					paramHeader = offloadingInterceptor
							.onProxyOUTParam(context);
				} else {
					paramHeader = offloadingInterceptor
							.onEntityOUTParam(context);
				}
				if (paramHeader != null) {
					if (paramHeader.getFields().size() > 0) {
						responseHeader.getParams().add(paramHeader);
					}
				}
			}

			try {
				soap.getHeaders().add(
						new Header(new QName("dyno4ws"), responseHeader,
								new JAXBDataBinding(DynO4WSHeader.class)));
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


//		ts = System.nanoTime() - ts;
//		ps.println(ts);
		
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public void setRepositoryEndpoint(String repositoryEndpoint) {
		this.repositoryEndpoint = repositoryEndpoint;
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository = repository;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public void setParameterInterceptor(
			ParameterInterceptor parameterInterceptor) {
		this.parameterInterceptor = parameterInterceptor;
	}

	public void setOffloadingInterceptor(
			OffloadingInterceptor offloadingInterceptor) {
		this.offloadingInterceptor = offloadingInterceptor;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getRepositoryEndpoint() {
		return repositoryEndpoint;
	}

	public OffloadingStrategy getStrategy() {
		return strategy;
	}

	public OffloadingRepository getRepository() {
		return repository;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public ParameterInterceptor getParameterInterceptor() {
		return parameterInterceptor;
	}

	public OffloadingInterceptor getOffloadingInterceptor() {
		return offloadingInterceptor;
	}

}
