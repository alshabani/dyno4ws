package it.unisannio.ding.dyno4ws.interceptors;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

public class ResponseTimestamper extends AbstractPhaseInterceptor<Message> {

	private PrintStream ps;


	public ResponseTimestamper(String filePath){
		super(Phase.SEND_ENDING);
		try {
			ps = new PrintStream(new FileOutputStream(new File(filePath)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public void handleMessage(Message message) throws Fault {
		long ts = (Long) message.getExchange().getInMessage().get("ts");
		ts = System.nanoTime() - ts;
		ps.println(ts);
	}

}
