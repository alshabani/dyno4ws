package it.unisannio.ding.wslink.dataevents.eventservice.camel.test;

import it.unisannio.ding.wslink.dataevents.AttachmentDescriptor;
import it.unisannio.ding.wslink.dataevents.AttachmentType;
import it.unisannio.ding.wslink.dataevents.DataEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.cxf.endpoint.Client;

public class EventGenerator implements Runnable {

	private String producerEndpoint;
	private String topic;
	private int numberOfEventsPerTopic;
	private long delayBetweenEventsInMilliseconds;
	private long initialDelayInMilliseconds;
	private Client eventServiceNotificationClient;
	private int attachmentSizeInKB;

	public EventGenerator(String producerEndpoint, String topic,
			int attachmentSizeInKB, int numberOfEventsPerTopic,
			long initialDelayInMilliseconds,
			long delayBetweenEventsInMilliseconds,
			Client eventServiceNotificationClient) {
		this.producerEndpoint = producerEndpoint;
		this.topic = topic;
		this.attachmentSizeInKB = attachmentSizeInKB;
		this.numberOfEventsPerTopic = numberOfEventsPerTopic;
		this.delayBetweenEventsInMilliseconds = delayBetweenEventsInMilliseconds;
		this.initialDelayInMilliseconds = initialDelayInMilliseconds;
		this.eventServiceNotificationClient = eventServiceNotificationClient;
	}

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		try {
			System.out.println("New producer started on endpoint "
					+ producerEndpoint);
			Thread.currentThread().sleep(initialDelayInMilliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < numberOfEventsPerTopic; i++) {
			DataEvent event = createEvent(topic, attachmentSizeInKB);

			try {
				eventServiceNotificationClient.invoke("notify",
						new Object[] { event });
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Thread.currentThread().sleep(delayBetweenEventsInMilliseconds);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private DataEvent createEvent(String topic, int attachmentSize) {
		DataEvent result = new DataEvent();
		result.setTopicExpression(topic);

		AttachmentDescriptor descriptor = new AttachmentDescriptor();
		descriptor.setFilename("image.jpg");
		descriptor.setType(AttachmentType.FILE);
		descriptor.setMimetype("image/jpg");
		descriptor.setBeginOffset(0);
		List<AttachmentDescriptor> descriptors = new ArrayList<AttachmentDescriptor>();
		descriptors.add(descriptor);
		result.setDescriptors(descriptors);

		// read the attachment
		// byte[] data = generateRandomBytesContent(attachmentSize);
		byte[] data = new byte[1024 * attachmentSizeInKB];

		descriptor.setEndOffset(data.length);
		result.setAttachments(data);
		descriptor.setEndOffset(data.length);
		result.setAttachments(data);
		result.setTimestamp(System.currentTimeMillis());
		return result;
	}

	private byte[] generateRandomBytesContent(int attachmentSize) {
		byte[] b = new byte[attachmentSize * 1024];
		new Random().nextBytes(b);
		return b;
	}
}