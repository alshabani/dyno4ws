package it.unisannio.ding.wslink.dataevents;

import java.net.URL;

// TODO: Auto-generated Javadoc
/**
 * The Class AttachmentDescriptor. Describes an attachment inside a DataEvent
 * offsets are to be used in order to correctly retrieve the attachment in the 
 * data buffer of he event
 * @author qzagarese
 */
public class AttachmentDescriptor {

	/** The begin offset. */
	private int beginOffset;
	
	/** The end offset. */
	private int endOffset;
	
	/** The type. */
	private AttachmentType type; 
	
	/** The filename. */
	private String filename;
 	
	/** The mimetype. If it is a file */
	private String mimetype;
	
	/** The instance type. If it an instance */
	private String instanceType;
	
	/** The type definition wsdl. */
	private URL typeDefinitionWSDL;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public AttachmentType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(AttachmentType type) {
		this.type = type;
	}

	/**
	 * Gets the mimetype.
	 *
	 * @return the mimetype
	 */
	public String getMimetype() {
		return mimetype;
	}

	/**
	 * Sets the mimetype.
	 *
	 * @param mimetype the new mimetype
	 */
	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	/**
	 * Gets the instance type.
	 *
	 * @return the instance type
	 */
	public String getInstanceType() {
		return instanceType;
	}

	/**
	 * Sets the instance type.
	 *
	 * @param instanceType the new instance type
	 */
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	/**
	 * Gets the type definition wsdl.
	 *
	 * @return the type definition wsdl
	 */
	public URL getTypeDefinitionWSDL() {
		return typeDefinitionWSDL;
	}

	/**
	 * Sets the type definition wsdl.
	 *
	 * @param typeDefinitionWSDL the new type definition wsdl
	 */
	public void setTypeDefinitionWSDL(URL typeDefinitionWSDL) {
		this.typeDefinitionWSDL = typeDefinitionWSDL;
	}

	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Gets the begin offset.
	 *
	 * @return the begin offset
	 */
	public int getBeginOffset() {
		return beginOffset;
	}

	/**
	 * Sets the begin offset.
	 *
	 * @param beginOffset the new begin offset
	 */
	public void setBeginOffset(int beginOffset) {
		this.beginOffset = beginOffset;
	}

	/**
	 * Gets the end offset.
	 *
	 * @return the end offset
	 */
	public int getEndOffset() {
		return endOffset;
	}

	/**
	 * Sets the end offset.
	 *
	 * @param endOffset the new end offset
	 */
	public void setEndOffset(int endOffset) {
		this.endOffset = endOffset;
	}
	
		
}
