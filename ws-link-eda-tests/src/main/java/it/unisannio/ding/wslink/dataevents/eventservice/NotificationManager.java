package it.unisannio.ding.wslink.dataevents.eventservice;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.Subscription;

import java.util.List;

public interface NotificationManager {

	public void addSubscription(Subscription subscription);

	public void removeSubscription(Subscription subscription);

	public List<Subscription> getSubscriptionsFor(String topicExpression);

	public void deliverEvent(DataEvent event, Subscription s);
}
