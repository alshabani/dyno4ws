package it.unisannio.ding.wslink.dataevents.eventservice;

import javax.jws.WebService;

import it.unisannio.ding.wslink.dataevents.Subscription;

@WebService
public interface DataEventProducer {

	public void subscribe(Subscription s);

	public void unsubscribe(Subscription s);

}
