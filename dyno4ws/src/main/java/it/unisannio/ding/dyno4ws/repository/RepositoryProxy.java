package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.net.URL;
import java.util.List;

import javax.jws.WebService;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

@WebService(endpointInterface = "it.unisannio.ding.dyno4ws.repository.OffloadingRepository", serviceName = "OffloadingRepository")
public class RepositoryProxy implements OffloadingRepository {

	private OffloadingRepository repository;
	private OffloadingStrategy strategy;

	// private PrintStream ps;

	public RepositoryProxy() {
		// try {
		// ps = new PrintStream(new FileOutputStream(new File("Tread.txt")));
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

	public void setImplementingInstance(OffloadingRepository repository) {
		this.repository = repository;
	}

	public void setImplementingService(URL endpoint) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(OffloadingRepository.class);
		factory.setAddress(endpoint.toExternalForm());
		repository = (OffloadingRepository) factory.create();
	}

	public List<OffloadedField> retrieveFieldsById(String id, ProfileData data) {
		List<OffloadedField> result = null;
		if (repository == null) {
			throw new RuntimeException("No repository impl or service provided");
		} else {
			result = repository.retrieveFieldsById(id, data);
		}
		return result;
	}

	public List<String> offloadParamFields(String clientId,
			String invocationId, String paramName, List<OffloadedField> fields) {
		List<String> result = null;
		if (repository == null) {
			throw new RuntimeException("No repository impl or service provided");
		} else {
			result = repository.offloadParamFields(clientId, invocationId,
					paramName, fields);
		}
		return result;
	}

	public void setStrategyEndpoint(String endpoint) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(OffloadingStrategy.class);
		factory.setAddress(endpoint);
		strategy = (OffloadingStrategy) factory.create();
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

}
