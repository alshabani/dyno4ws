package it.unisannio.ding.dyno4ws.proxy;


public interface DynO4WSProxy {

	public ProxyContext getContext();
	
}
