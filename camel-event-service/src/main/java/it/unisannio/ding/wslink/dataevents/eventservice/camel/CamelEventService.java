package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import java.util.logging.Logger;

import it.unisannio.ding.wslink.dataevents.distributedtest.Properties;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.ModelCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CamelEventService extends RouteBuilder {
	
	public Properties properties = Properties.getInstance();
	
	
	
//	public static String eventReceiverURI = "cxf:http://localhost:9999/eventService?serviceClass="
//			+ DataEventConsumer.class.getName();
//
//	public static String subscriptionReceiverURI = "cxf:http://localhost:9998/subscriptionService?serviceClass="
//			+ DataEventProducer.class.getName();
//
//	public static final Logger logger = Logger.getLogger(CamelEventService.class.getName());
//	
	@Autowired
	private SubscriptionReceiver subscriptionReceiver;
	private ModelCamelContext ctx = this.getContext();

	
	@Override
	public void configure() throws Exception {
		System.out.println("This Context is : " + ctx);
		subscriptionReceiver.setModelContext(this.getContext());
//		System.out.println("This context is:" + this.getContext());
		String esHostname = properties.getESHost();
		System.out.println("ES.Hostname : " + esHostname);
		String eventReceiverURI = String.format("cxf:http://%s:9999/eventService?serviceClass=%s",
				esHostname, DataEventConsumer.class.getName());
		String subscriptionReceiverURI = String.format("cxf:http://%s:9998/subscriptionService?serviceClass=%s",
				esHostname, DataEventProducer.class.getName());

		from(eventReceiverURI)
				.beanRef("notificationManager", "onEventProduced");
		from(subscriptionReceiverURI).beanRef("subscriptionReceiver",
				"onSubscription");
	}
}
