package it.unisannio.ding.wslink.dataevents;

// TODO: Auto-generated Javadoc
/**
 * The Enum SubscriptionType. Topic based contains a string that will be treated as the topic.
 * Content based can contain a query (i.e. sparql query)
 * 
 * @author qzagarese
 */
public enum SubscriptionType {

	/** The TOPI c_ based. */
	TOPIC_BASED,	

	/** The CONTEN t_ based. */
	CONTENT_BASED;
	
}
