/**
 * 
 */
package it.unisannio.ding.wslink.core.annotation;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * @author Quirino Zagarese
 * 
 */
public interface AnnotationProcessor {

	public Map<String, Object> scanAnnotation(Annotation annotation,
			Field declaringField);

	public OffloadedField processField(Map<Class<? extends Annotation>, Map<String, Object>> metadata, Field f,
			Object fieldValue, Object parent, OffloadedField result, OffloadingContext context);

}
