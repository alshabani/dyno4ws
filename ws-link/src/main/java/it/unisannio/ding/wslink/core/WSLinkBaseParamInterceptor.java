package it.unisannio.ding.wslink.core;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.interceptors.parameters.BasicParameterInterceptor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;

public class WSLinkBaseParamInterceptor extends BasicParameterInterceptor {

	private Inflater decompresser = new Inflater();
	
	private Gson gson = new Gson();

	protected Object unserialize(OffloadedField field) {
		Object result = null;
		if (field.getEncoding().equalsIgnoreCase("xml")) {
			fromXML(field);
		} else if (field.getEncoding().equalsIgnoreCase("gzip_xml")) {
			uncompress(field);
			fromXML(field);
		} 
//		else if (field.getEncoding().equalsIgnoreCase("json")) {
//			fromJSON(field);
//		} else if (field.getEncoding().equalsIgnoreCase("gzip_json")) {
//			uncompress(field);
//			fromJSON(field);
//		} 
//		else if (field.getEncoding().equalsIgnoreCase("hessian")) {
//			fromHessian(field);
//		}
		result = field.getReference();
		return result;
	}

//	private void fromHessian(OffloadedField field) {
//		Class<?> fieldType = null;
//
//		try {
//			fieldType = Class.forName(field.getType());
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//		
//		byte[] decodedData = Base64.decodeBase64(field.getData());
//		ByteArrayInputStream buffer = new ByteArrayInputStream(decodedData);
//		Hessian2Input hessian = new Hessian2Input(buffer);
//		SerializerFactory factory = new SerializerFactory();
//		factory.setAllowNonSerializable(true);
//		try {
//			Deserializer objectDeserializer = factory.getObjectDeserializer(field.getType(), fieldType);
//			hessian.startMessage();
//			Object object = objectDeserializer.readMap(hessian);
//			field.setReference(object);
//			hessian.completeMessage();
//			buffer.close();
//			hessian.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

//	private void fromJSON(OffloadedField field) {
//		Class<?> fieldType = null;
//
//		try {
//			fieldType = Class.forName(field.getType());
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
//		field.setReference(gson .fromJson(field.getData(), fieldType));
//	}

	private void uncompress(OffloadedField field) {
		// Decompress the bytes
		byte[] compressedData = Base64.decodeBase64(field.getData());
		
		decompresser.setInput(compressedData);
		ByteArrayOutputStream bos = new ByteArrayOutputStream(
				compressedData.length);

		// Decompress the data
		byte[] buf = new byte[1024];
		while (!decompresser.finished()) {
			try {
				int count = decompresser.inflate(buf);
				bos.write(buf, 0, count);
			} catch (DataFormatException e) {
				e.printStackTrace();
			}
		}
		try {
			bos.close();
		} catch (IOException e) {
		}

		// Get the decompressed data
		byte[] decompressedData = bos.toByteArray();
		field.setData(new String(decompressedData));
	}

}
