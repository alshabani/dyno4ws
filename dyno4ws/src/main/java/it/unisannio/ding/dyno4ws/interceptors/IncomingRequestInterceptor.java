package it.unisannio.ding.dyno4ws.interceptors;

import it.unisannio.ding.dyno4ws.DynO4WSHeader;
import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.proxy.ParameterProxyFactory;
import it.unisannio.ding.dyno4ws.proxy.ProxyContext;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;
import javax.xml.transform.dom.DOMSource;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Node;

public class IncomingRequestInterceptor extends
		AbstractPhaseInterceptor<Message> implements DynO4WSCXFInterceptor {

	private String nodeId;

	private String repositoryEndpoint;

	private OffloadingStrategy strategy;

	private OffloadingRepository repository;

	private Endpoint endpoint;

	private ParameterInterceptor parameterInterceptor;

	private OffloadingInterceptor offloadingInterceptor;
	
//	private PrintStream ps;

	public IncomingRequestInterceptor() {
		super(Phase.USER_LOGICAL);
//		try {
//			ps = new PrintStream(new FileOutputStream(new File("IncomingRequestInterceptor.txt")));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	
	public void handleMessage(Message message) throws Fault {
//		long ts = System.nanoTime();

		SoapMessage soap = (SoapMessage) message;
		Header header = soap.getHeader(new QName("dyno4ws"));
		DOMSource source = new DOMSource((Node) header.getObject());
		DynO4WSHeader sentHeader = JAXB.unmarshal(source, DynO4WSHeader.class);
		@SuppressWarnings("rawtypes")
		List content = message.getContent(List.class);
		Object request = content.get(0);

		List<DynO4WSParam> paramHeadersCopy = new ArrayList<DynO4WSParam>();

		for (DynO4WSParam param : sentHeader.getParams()) {
			paramHeadersCopy.add(param);
		}

		for (Field f : request.getClass().getDeclaredFields()) {
			boolean accessible = f.isAccessible();
			f.setAccessible(true);
			Object arg;
			DynO4WSParam paramHeader = findParamHeader(f.getName(),
					paramHeadersCopy);
			if (paramHeader != null) {
				try {
					arg = f.get(request);
					if (!isJDKType(arg.getClass())
							&& (!Modifier
									.isFinal(arg.getClass().getModifiers()))) {
						ProxyContext context = new ProxyContext();
						context.setInterceptor(parameterInterceptor);
						context.setOffloadingData(sentHeader);
						context.setRepository(repository);
						context.setStrategy(strategy);
						context.setTarget(arg);
						context.setParameterData(paramHeader);
						Object proxy = ParameterProxyFactory
								.createProxy(context);
						// context.setProxy((DynO4WSProxy) proxy);
						f.set(request, proxy);
					}
					f.setAccessible(accessible);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

//		ts = System.nanoTime() - ts;
//		ps.println(ts);
	}

	private DynO4WSParam findParamHeader(String name,
			List<DynO4WSParam> paramHeadersCopy) {
		for (int i = 0; i < paramHeadersCopy.size(); i++) {
			DynO4WSParam param = paramHeadersCopy.get(i);
			if (param.getName().equals(name)) {
				paramHeadersCopy.remove(param);
				return param;
			}
		}
		return null;
	}

	private boolean isJDKType(Class<? extends Object> clazz) {
		boolean result = false;
		result = clazz.isPrimitive();
		if (!result) {
			result = clazz.getName().startsWith("java.");
		}
		return result;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public void setRepositoryEndpoint(String repositoryEndpoint) {
		this.repositoryEndpoint = repositoryEndpoint;
	}

	public void setStrategy(OffloadingStrategy strategy) {
		this.strategy = strategy;
	}

	public void setRepository(OffloadingRepository repository) {
		this.repository = repository;
	}

	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public void setParameterInterceptor(
			ParameterInterceptor parameterInterceptor) {
		this.parameterInterceptor = parameterInterceptor;
	}

	public void setOffloadingInterceptor(
			OffloadingInterceptor offloadingInterceptor) {
		this.offloadingInterceptor = offloadingInterceptor;
	}

	public String getNodeId() {
		return nodeId;
	}

	public String getRepositoryEndpoint() {
		return repositoryEndpoint;
	}

	public OffloadingStrategy getStrategy() {
		return strategy;
	}

	public OffloadingRepository getRepository() {
		return repository;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public ParameterInterceptor getParameterInterceptor() {
		return parameterInterceptor;
	}

	public OffloadingInterceptor getOffloadingInterceptor() {
		return offloadingInterceptor;
	}

}
