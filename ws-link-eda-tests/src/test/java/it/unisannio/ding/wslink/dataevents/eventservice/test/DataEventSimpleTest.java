//package it.unisannio.ding.wslink.dataevents.eventservice.test;
//
//import it.unisannio.ding.wslink.api.WSLink;
//import it.unisannio.ding.wslink.dataevents.AttachmentDescriptor;
//import it.unisannio.ding.wslink.dataevents.AttachmentType;
//import it.unisannio.ding.wslink.dataevents.DataEvent;
//import it.unisannio.ding.wslink.dataevents.Subscription;
//import it.unisannio.ding.wslink.dataevents.SubscriptionType;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
//import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
//import it.unisannio.ding.wslink.dataevents.eventservice.core.BaseEventService;
//import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;
//
//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Logger;
//
//import org.apache.cxf.endpoint.Client;
//import org.apache.cxf.endpoint.Server;
//
//public class DataEventSimpleTest {
//
//	public static final String SUBSCRIBER_ENDPOINT = "http://localhost:9072/subscriber";
//
//	private static final String CONSUMER_ENDPOINT = "http://localhost:9098/notificationConsumer";
//
//	private static final String PRODUCER_ENDPOINT = "http://localhost:9099/notificationProducer";
//
//	private final static Logger logger = Logger
//			.getLogger(DataEventSimpleTest.class.getSimpleName());
//
//	private static final int DEFAULT_ATTACHMENT_SIZE = 10240; // 10 KB
//
//	private int attachmentSize;
//
//	private WSLink wsLinkProducer;
//
//	private WSLink wsLinkConsumer;
//
//	private Client eventServiceNotificationClient;
//
//	private Client eventServiceSubscriptionClient;
//
//	private Server consumerService;
//
//	public DataEventSimpleTest() {
//		this.attachmentSize = DEFAULT_ATTACHMENT_SIZE;
//		wsLinkProducer = new WSLink("publisher", "localhost", 12222);
//		wsLinkConsumer = new WSLink("subscriber", "localhost", 12223);
//
//		eventServiceNotificationClient = ServiceUtility.createWsClient(
//				DataEventConsumer.class, CONSUMER_ENDPOINT);
//
//		eventServiceSubscriptionClient = ServiceUtility.createWsClient(
//				DataEventProducer.class, PRODUCER_ENDPOINT);
//
//		consumerService = ServiceUtility.createService(
//				new UserSubsystemSubscriberService(), SUBSCRIBER_ENDPOINT);
//		wsLinkProducer.registerClient(eventServiceNotificationClient
//				.getEndpoint());
//
//		wsLinkConsumer.registerClient(eventServiceSubscriptionClient
//				.getEndpoint());
//		wsLinkConsumer.registerService(consumerService.getEndpoint());
//	}
//
//	public static void main(String[] args) {
//		EventService eventService = new BaseEventService();
//		eventService.start(CONSUMER_ENDPOINT, PRODUCER_ENDPOINT);
//		logger.info("EVENT SERVICE IS UP AND RUNNING");
//		// Test
//		DataEventSimpleTest tester = new DataEventSimpleTest();
//		tester.test();
//
//		// cleanup resources
//		eventService.shutdown();
//	}
//
//	public void test() {
//		Subscription s = createSubscription("GRAPHICS");
//		try {
//			eventServiceSubscriptionClient.invoke("subscribe",
//					new Object[] { s });
//			DataEvent event = createEvent(s.getContent());
//			eventServiceNotificationClient.invoke("notify",
//					new Object[] { event });
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private Subscription createSubscription(String topic) {
//		Subscription subscription = new Subscription();
//		subscription.setType(SubscriptionType.TOPIC_BASED);
//		subscription.setContent(topic);
//		try {
//			subscription.setSubscriberEndpoint(new URL(SUBSCRIBER_ENDPOINT));
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return subscription;
//	}
//
//	private DataEvent createEvent(String topic) {
//		DataEvent result = new DataEvent();
//		result.setTopicExpression(topic);
//
//		AttachmentDescriptor descriptor = new AttachmentDescriptor();
//		descriptor.setFilename("image.jpg");
//		descriptor.setType(AttachmentType.FILE);
//		descriptor.setMimetype("image/jpg");
//		descriptor.setBeginOffset(0);
//		List<AttachmentDescriptor> descriptors = new ArrayList<AttachmentDescriptor>();
//		descriptors.add(descriptor);
//		result.setDescriptors(descriptors);
//
//		// read the attachment
//		byte[] data = null;
//		try {
//			InputStream stream = inputStreamFrom("/image.jpg");
//			ByteArrayOutputStream out = new ByteArrayOutputStream();
//			byte[] buffer = new byte[1024];
//			int read = stream.read(buffer);
//			while (read != -1) {
//				out.write(buffer, 0, read);
//				read = stream.read(buffer);
//			}
//			data = out.toByteArray();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		descriptor.setEndOffset(data.length);
//		result.setAttachments(data);
//		descriptor.setEndOffset(data.length);
//		result.setAttachments(data);
//		return result;
//	}
//
//	private static InputStream inputStreamFrom(String file) {
//		InputStream is = null;
//
//		if (file != null) {
//			is = DataEventSimpleTest.class.getResourceAsStream(file);
//		}
//
//		// InputStream is = null;
//		// try {
//		// is = new URL("http://sampleurl/pic.jpg").openStream();
//		// } catch (MalformedURLException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// } catch (IOException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// }
//		return is;
//	}
//
//}
