package it.unisannio.ding.dyno4ws.repository;

import java.util.List;

import com.mongodb.DBObject;

public class AsynchMongoDBRepository extends MongoDBRepository {

	private MongoDBThread persistenceThread;

	public AsynchMongoDBRepository() {
		super();
		persistenceThread = new MongoDBThread(coll);
		persistenceThread.start();
	}

	protected void save(List<DBObject> docs) {
		persistenceThread.save(docs);
	}
}
