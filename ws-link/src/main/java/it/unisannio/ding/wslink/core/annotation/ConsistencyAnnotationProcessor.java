/**
 * 
 */
package it.unisannio.ding.wslink.core.annotation;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.wslink.api.ConsistencyType;
import it.unisannio.ding.wslink.api.annotation.Consistency;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Quirino Zagarese
 * 
 */
public class ConsistencyAnnotationProcessor implements AnnotationProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.wslink.core.AnnotationProcessor#processAnnotation(java
	 * .lang.annotation.Annotation, java.lang.reflect.Field)
	 */
	public Map<String, Object> scanAnnotation(Annotation annotation,
			Field declaringField) {
		Map<String, Object> result = new HashMap<String, Object>();
		Consistency c = (Consistency) annotation;
		result.put("value", c.value());
		return result;
	}

	public OffloadedField processField(
			Map<Class<? extends Annotation>, Map<String, Object>> metadata,
			Field f, Object fieldValue, Object parent, OffloadedField result,
			OffloadingContext context) {
		Map<String, Object> localData = metadata.get(Consistency.class);
		if (localData != null) {
			result.setCopy(localData.get("value").equals(
					ConsistencyType.MAKE_COPY));
		}
		return result;
	}

}
