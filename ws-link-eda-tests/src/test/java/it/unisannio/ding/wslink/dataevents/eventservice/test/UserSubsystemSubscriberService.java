//package it.unisannio.ding.wslink.dataevents.eventservice.test;
//
//import it.unisannio.ding.wslink.dataevents.DataEvent;
//import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
//
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//
//import javax.jws.WebService;
//import javax.swing.JFrame;
//
//@WebService(serviceName = "DataEventConsumer", portName = "DataEventConsumerPort", name = "DataEventConsumerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
//public class UserSubsystemSubscriberService implements DataEventConsumer {
//
//	@Override
//	public void notify(DataEvent event) {
//		System.out.println("USER SUBSYSTEM: event topic is "
//				+ event.getTopicExpression());
//		byte[] content = event.getAttachments();
//		System.out.println("USER SUBSYSTEM: content length is "
//				+ content.length);
//		System.out.println("USER SUBSYSTEM: loading image...");
//		showAttachment(content);
//	}
//
//	private void showAttachment(byte[] attachment) {
//
//		JFrame f = new JFrame("Load Image Sample");
//
//		f.addWindowListener(new WindowAdapter() {
//			public void windowClosing(WindowEvent e) {
//				System.exit(0);
//			}
//		});
//
//		f.add(new ImageLoader(attachment));
//		f.pack();
//		f.setVisible(true);
//
//	}
//}
