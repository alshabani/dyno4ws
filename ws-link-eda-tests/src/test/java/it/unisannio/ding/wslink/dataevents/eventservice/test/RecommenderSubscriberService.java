package it.unisannio.ding.wslink.dataevents.eventservice.test;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;

import javax.jws.WebService;

@WebService(serviceName = "DataEventConsumer", portName = "DataEventConsumerPort", name = "DataEventConsumerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
public class RecommenderSubscriberService implements DataEventConsumer {

	@Override
	public void notify(DataEvent event) {
		System.out.println("RECOMMENDER SUBSYSTEM: event topic is " + event.getTopicExpression());
	}

}
