package it.unisannio.ding.dyno4ws.interceptors.offloading;

import it.unisannio.ding.dyno4ws.DynO4WSParam;

public interface OffloadingInterceptor {

	public DynO4WSParam onEntityINParam(OffloadingContext context);

	public DynO4WSParam onProxyINParam(OffloadingContext context);

	public DynO4WSParam onEntityOUTParam(OffloadingContext context);

	public DynO4WSParam onProxyOUTParam(OffloadingContext context);

}
