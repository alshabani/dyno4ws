package it.unisannio.ding.wslink.dataevents.eventservice.test;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.jws.WebService;

@WebService(serviceName = "DataEventConsumer", portName = "DataEventConsumerPort", name = "DataEventConsumerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
public class SubscriberService implements DataEventConsumer {
	private String endpoint;
	private Boolean[] attachmentAccessMap;
	private String performanceDataFile;

	public SubscriberService(String endpoint, Boolean[] attachmentAccessMap,
			String performanceDataFile) {
		this.endpoint = endpoint;
		this.attachmentAccessMap = attachmentAccessMap;
		this.performanceDataFile = performanceDataFile;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public void notify(DataEvent event) {
		// System.out.println("Subscriber " + endpoint
		// + ": received event with topic: " + event.getTopicExpression());
		if (attachmentAccessMap != null) {
			for (int i = 0; i < attachmentAccessMap.length; i++) {
				if (attachmentAccessMap[i]) {
					// FIXME: parameter required to access the i-th attachment
					byte[] data = event.getAttachments();
					long currentTimestamp = System.currentTimeMillis();
					long producedTimestamp = event.getTimestamp();
					long delta = currentTimestamp - producedTimestamp;
					try {
						PrintStream ps = new PrintStream(new FileOutputStream(
								new File(performanceDataFile), true));
						ps.println(producedTimestamp + "," + currentTimestamp
								+ "," + delta);
						ps.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("Received " + data.length);
					// System.out.println("Attachment content is: "
					// + new String(data));
				}
			}
		}
	}

}
