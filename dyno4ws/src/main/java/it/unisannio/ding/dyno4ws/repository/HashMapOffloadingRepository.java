package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HashMapOffloadingRepository implements OffloadingRepository{

	private Map<String, OffloadedField> fieldsMap = new HashMap<String, OffloadedField>();
	
	
	
	public List<OffloadedField> retrieveFieldsById(String id, ProfileData data) {
		List<OffloadedField> result = new ArrayList<OffloadedField>();
		OffloadedField field = fieldsMap.get(id);
		if(field != null){
			result.add(field);
		}
		return result;
	}

	
	public List<String> offloadParamFields(String nodeId, String invocationId,
			String paramName, List<OffloadedField> fields) {
		List<String> result = new ArrayList<String>();
		for(OffloadedField field : fields){
			String key = createKey(nodeId, invocationId, paramName, field.getName());
			fieldsMap.put(key, field);
			result.add(key);
		}
//		fieldsMap = new HashMap<String, OffloadedField>();
		return result;
	}

	private String createKey(String nodeId, String invocationId,
			String paramName, String name) {
		return "{nodeId:" + nodeId + ",cid:" +  invocationId + ",param:" + paramName +  ",name:" + name + "}";
	}


	public void setStrategyEndpoint(String endpoint) {
		// TODO Auto-generated method stub
		
	}
	

}
