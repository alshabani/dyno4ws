/**
 * 
 */
package it.unisannio.ding.wslink.dataevents.distributedtest.multithreading;

import it.unisannio.ding.wslink.dataevents.DataEvent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class SubscriberThread extends Thread {

	private BlockingQueue<DataEvent> events = new LinkedBlockingDeque<DataEvent>();
	private long processingTime;
	private long firstTimestamp;
	private long lastTimestamp;
	private int processedEvents = 0;
	private String name;

	public SubscriberThread(String name, DataEvent firstEvent,
			long processingTime) {
		System.out.println(name + " started");
		this.name = name;
		this.processingTime = processingTime;
		this.firstTimestamp = firstEvent.getTimestamp();
//		this.writer = writer;
	}

	public void run() {
		DataEvent event = null;
		do {
			try {
				event = events.poll(1, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (event != null) {
				byte[] attachments = event.getAttachments();
				// System.out.println(name + " processed " + processedEvents +
				// " events");
				lastTimestamp = System.currentTimeMillis();
				if ((lastTimestamp - firstTimestamp) <= processingTime) {
					processedEvents++;
				}
			}
		} while ((lastTimestamp - firstTimestamp) <= processingTime);
		System.out.println(name + " processed " + processedEvents
				+ " events in " + (lastTimestamp - firstTimestamp)
				+ "ms");
//		writer.writeOutput(name, lastTimestamp - firstTimestamp, processedEvents);
	}

	public void submitEvent(DataEvent event) {
		events.add(event);
	}

}