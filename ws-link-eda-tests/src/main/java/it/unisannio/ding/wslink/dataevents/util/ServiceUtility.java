package it.unisannio.ding.wslink.dataevents.util;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.jaxws.JaxWsClientFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

public class ServiceUtility {
	public static Server createService(Object implementor, String address) {
		ServerFactoryBean svrFactory = new JaxWsServerFactoryBean();
		svrFactory.setServiceClass(implementor.getClass());
		svrFactory.setServiceBean(implementor);
		svrFactory.setAddress(address);
		return svrFactory.create();
	}

	public static <T> Client createWsClient(Class<T> serviceClass,
			String serviceAddress) {
		JaxWsClientFactoryBean factory = new JaxWsClientFactoryBean();
		factory.setServiceClass(serviceClass);
		factory.setAddress(serviceAddress);
		return factory.create();
	}
}
