package it.unisannio.ding.dyno4ws;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="dyno4ws")
public class DynO4WSHeader {

	private String nodeId;
	
	private String callId;
	
	private List<DynO4WSParam> params = new ArrayList<DynO4WSParam>();

	@XmlAttribute(name="nid")
	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	@XmlAttribute(name="cid")
	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	@XmlElement(name="param")
	public List<DynO4WSParam> getParams() {
		return params;
	}

	public void setParams(List<DynO4WSParam> params) {
		this.params = params;
	}
	
}
