package it.unisannio.ding.wslink.dataevents.eventservice;

public interface EventService {

	public void start(String consumerEndpoint, String producerEndpoint);

	public void shutdown();

	public void setNotificationManager(NotificationManager manager);
}
