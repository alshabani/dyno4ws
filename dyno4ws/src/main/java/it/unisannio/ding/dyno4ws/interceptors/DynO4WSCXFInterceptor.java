package it.unisannio.ding.dyno4ws.interceptors;

import org.apache.cxf.endpoint.Endpoint;

import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingInterceptor;
import it.unisannio.ding.dyno4ws.interceptors.parameters.ParameterInterceptor;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;

public interface DynO4WSCXFInterceptor {
	
	public void setNodeId(String id);
		
	public void setRepositoryEndpoint(String repository);
	
	public void setEndpoint(Endpoint endpoint);
	
	public void setOffloadingInterceptor(OffloadingInterceptor interceptor);
	
	public void setParameterInterceptor(ParameterInterceptor interceptor);
	
	public void setRepository(OffloadingRepository repository);
	
	public void setStrategy(OffloadingStrategy strategy);
	
}
