package it.unisannio.ding.wslink.dataevents.distributedtest;

import it.unisannio.ding.wslink.api.WSLink;
import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.SubscriptionType;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.eventservice.EventService;
import it.unisannio.ding.wslink.dataevents.eventservice.core.BaseEventService;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transport.http.spring.HttpConduitBeanDefinitionParser;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

/**
 * @author aalshaba
 */
public class MultipleRoleTest {

    private static String NETWORK_INTERFACE_NAME = "eth0";

    public String EVENT_SERVICE_HOST;
    public int EVENT_SERVICE_CONSUMER_PORT_NUMBER;
    public int EVENT_SERVICE_PRODUCER_PORT_NUMBER;

    // Endpoints of publisher and subscriber
    public String EVENT_SERVICE_NOTIFICATION_ENDPOINT;
    public String EVENT_SERVICE_SUBSCRIPTION_ENDPOINT;

    public String topic;

    // to hold results in a csv file
    public static final String CSV_PERFORMANCE_FILE = "results.csv";

    public static final int SUBSCRIBERS_BASE_PORT_NUMBER = 14000;

    public static final int PUBLISHER_BASE_PORT_NUMBER = 12005;

    private Properties properties;

    public MultipleRoleTest() {
        // subscribersWSLink = new ArrayList<WSLink>();
        // producersWSLink = new ArrayList<WSLink>();
        // subscribersClients = new ArrayList<Client>();
        // producersClients = new ArrayList<Client>();
        // subscribersServices = new ArrayList<Server>();
        // subscriberTopicList = new ArrayList<String>();
        // producerTopicList = new ArrayList<String>();

        properties = Properties.getInstance();

        // Getting hosts and port numbers, forming the endpoints, at the
        // EventService Side;
        EVENT_SERVICE_HOST = properties.getESHost();
        EVENT_SERVICE_CONSUMER_PORT_NUMBER = Properties.PUBLISHER_PORT;
        EVENT_SERVICE_PRODUCER_PORT_NUMBER = Properties.SUBSCRIBER_PORT;

        EVENT_SERVICE_NOTIFICATION_ENDPOINT =
                String.format(
                        "http://%s:%s/notificationConsumer",
                        EVENT_SERVICE_HOST, EVENT_SERVICE_CONSUMER_PORT_NUMBER);
        EVENT_SERVICE_SUBSCRIPTION_ENDPOINT =
                String.format(
                        "http://%s:%s/notificationProducer",
                        EVENT_SERVICE_HOST, EVENT_SERVICE_PRODUCER_PORT_NUMBER);
    }

    public static void main(String[] args) {
        MultipleRoleTest tester = new MultipleRoleTest();

        Properties properties = tester.getProperties();

        // TestRole machineRole = TestRole.ALL;
        String machineRole = getRole();
        // DataEventSize field gives the size in MB
        Integer attachmentSizeInKB = properties.getDataEventSize() * 1024;

        // delay between two consecutive events that are published
        long delay = properties.getDelayBetweenEvents();

        tester.test(
                machineRole, properties.getNumOfSubscribers(),
                properties.getNumOfPublishers(), properties.getNumOfEvents(),
                attachmentSizeInKB, delay,
                properties.getEventGeneratorThreadPoolSize(),
                properties.getSubscriberThreadPoolSize());
    }

    private static String getRole() {
        String role;
        role = System.getProperty("role");
        if (role == null) {
            role = "ALL";
        }
        return role;
    }

    public void test(String role, int numberOfSubscribers,
                     int numberOfProducers, int numberOfEvents,
                     int attachmentSizeInKB, long delay,
                     int eventGeneratorThreadPoolSize,
                     int subscriberThreadPoolSize) {
        if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("ES")) {
            startEventService();
        }

        if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("SUBSCRIBER")) {
            startSubscriber(
                    numberOfSubscribers, numberOfProducers, numberOfEvents,
                    subscriberThreadPoolSize);
        }

        if (role.equalsIgnoreCase("ALL") || role.equalsIgnoreCase("PUBLISHER")) {
            startPublisher(
                    numberOfEvents, attachmentSizeInKB, delay,
                    eventGeneratorThreadPoolSize);
        }
    }

    // Starting test with specific scenario
    public void test(String role, TestScenarios ts) {

    }

    /**
     * 
     * @param numberOfEvents
     *            number of events to publish
     * @param attachmentSizeInKB
     *            size of the attachment in each event
     * @param delay
     *            intervals between two consecutive events
     */
    private void startPublisher(int numberOfEvents, int attachmentSizeInKB,
                                long delay, int eventGeneratorThreadPoolSize) {
        WSLink publisherWSLink =
                new WSLink(
                        "publisher",
                        ListNets.getHostAddress(NETWORK_INTERFACE_NAME),
                        PUBLISHER_BASE_PORT_NUMBER);

        Client publisherClient =
                ServiceUtility.createWsClient(
                        DataEventConsumer.class,
                        EVENT_SERVICE_NOTIFICATION_ENDPOINT);
        //Setting timeout
        HTTPConduit publisherHTTPConduit = (HTTPConduit)publisherClient.getConduit();
        HTTPClientPolicy publisherPolicy = publisherHTTPConduit.getClient();
        publisherPolicy.setConnectionTimeout(0);
        // to manage the "Unmarshalling Error". Useless for the publisher side
        publisherPolicy.setReceiveTimeout(0);
        //TODO verify the affect of turning off the chunking for larger attachments
        publisherPolicy.setAllowChunking(false);
//
        
        publisherWSLink.registerClient(publisherClient.getEndpoint());

        String producerEndpoint =
                String.format(
                        "http://%s:%s/producer",
                        ListNets.getHostAddress(NETWORK_INTERFACE_NAME),
                        PUBLISHER_BASE_PORT_NUMBER);

        EventGenerator eventGenerator =
                new EventGenerator(
                        producerEndpoint, topic, numberOfEvents,
                        attachmentSizeInKB, delay, publisherClient,
                        eventGeneratorThreadPoolSize);

        eventGenerator.run();
    }

    private void startSubscriber(int numberOfSubscribers,
                                 int numberOfProducers, int numberOfEvents,
                                 int subscriberThreadPoolSize) {
        String subscriberEndpoint =
                String.format(
                        "http://%s:%s/subscriber",
                        ListNets.getHostAddress(NETWORK_INTERFACE_NAME),
                        SUBSCRIBERS_BASE_PORT_NUMBER);

        Client subscriberClient =
                ServiceUtility.createWsClient(
                        DataEventProducer.class,
                        EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);

      //Setting timeout for the 
        HTTPConduit subscriberHTTPConduit = (HTTPConduit)subscriberClient.getConduit();
        HTTPClientPolicy subscriberPolicy = subscriberHTTPConduit.getClient();
        subscriberPolicy.setConnectionTimeout(0);
        subscriberPolicy.setReceiveTimeout(0);
        //TODO verify the affect of turning of the chunking for large attachment size
        subscriberPolicy.setAllowChunking(false);
        
        SubscriberWS subscriberWS =
                new SubscriberWS(
                        subscriberEndpoint, numberOfProducers, numberOfEvents,
                        subscriberThreadPoolSize);
        
        
        
        Server subscriberService =
                ServiceUtility.createService(subscriberWS, subscriberEndpoint);

        WSLink subscriberWSLink =
                new WSLink(
                        "subscriber",
                        ListNets.getHostAddress(NETWORK_INTERFACE_NAME),
                        SUBSCRIBERS_BASE_PORT_NUMBER);
        subscriberWSLink.registerClient(subscriberClient.getEndpoint());
        subscriberWSLink.registerService(subscriberService.getEndpoint());
        Subscription subscription =
                createSubscription(topic, subscriberEndpoint);

        try {
            subscriberClient.invoke("subscribe", new Object[] {subscription});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startEventService() {
        EventService eventService = new BaseEventService();
        eventService.start(
                EVENT_SERVICE_NOTIFICATION_ENDPOINT,
                EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
        System.out.println("EVENT SERVICE IS UP AND RUNNING");
    }

    private Subscription createSubscription(String topic, String endpoint) {
        Subscription subscription = new Subscription();
        subscription.setType(SubscriptionType.TOPIC_BASED);
        subscription.setContent(topic);
        try {
            subscription.setSubscriberEndpoint(new URL(endpoint));
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return subscription;
    }

    /**
     * 
     * 
     * @return the properties
     */
    public Properties getProperties() {
        return this.properties;
    }

    /*private void startSubscribers(Integer numberOfSubscribers,
    		Integer numberOfSubscriberTopics,
    		Integer numberOfSubscribersPerTopic, Boolean[][] attachmentAccessMap) {
    	createTopics(numberOfSubscriberTopics, subscriberTopicList);
    	for (int i = 0; i < numberOfSubscribers; i++) {
    		int port = SUBSCRIBERS_BASE_PORT_NUMBER + i;
    		String subscriberEndpoint = "http://" + SUBSCRIBER_HOST
    				+ ":" + port + "/subscriber" + i;

    		Client subscriptionClient = ServiceUtility.createWsClient(
    				DataEventProducer.class,
    				EVENT_SERVICE_SUBSCRIPTION_ENDPOINT);
    		// subscribersClients.add(subscriptionClient);
    		SubscriberService subscriberService = new SubscriberService(
    				subscriberEndpoint, attachmentAccessMap[i],
    				CSV_PERFORMANCE_FILE);
    		Server subscriptionService = ServiceUtility.createService(
    				subscriberService, subscriberEndpoint);
    		// subscribersServices.add(subscriptionService);

    		WSLink subscriberWSLink = new WSLink("subscriber" + i,
    				SUBSCRIBER_HOST, port);

    		// subscribersWSLink.add(subscriberWSLink);

    		subscriberWSLink.registerClient(subscriptionClient.getEndpoint());
    		subscriberWSLink.registerService(subscriptionService.getEndpoint());

    		for (int j = 0; j < numberOfSubscriberTopics; j++) {
    			String topic = subscriberTopicList.get(j);
    			Subscription s = createSubscription(topic, subscriberEndpoint);
    			try {
    				subscriptionClient.invoke("subscribe", new Object[] { s });
    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	}
    }


    /*	private void createTopics(Integer numberOfTopics, List<String> topicList) {
    	if (numberOfTopics == null)
    		return;
    	for (int i = 0; i < numberOfTopics; i++) {
    		topicList.add(BASE_TOPIC_NAME + i);
    	}
    }*/

}
