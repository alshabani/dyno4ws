/**
 * 
 */
package it.unisannio.ding.wslink.core;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.Deflater;

import javax.xml.bind.JAXB;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

/**
 * @author Quirino Zagarese
 * 
 */
public class WSLinkRepository implements OffloadingRepository {

	private DBCollection coll;

	private String dbname = "wslink";

	private long minWait = 100;
	
	
	private long maxWait = 6400;
	

	private static Logger logger = Logger.getLogger(WSLinkRepository.class
			.getName());

	private Map<String, OffloadedField> references = new HashMap<String, OffloadedField>();

	private Deflater compresser = new Deflater();


	public WSLinkRepository() {
		Mongo m = null;
		try {
			m = new Mongo("localhost");
		} catch (Exception e) {
			e.printStackTrace();
			logger.log(
					Level.INFO,
					"No Mongo instance found; fallback on memory. Only @Consistency(onsistencyType.KEEP_REFERENCE) supported.");
		}

		DB db = m.getDB(dbname);
		coll = db.getCollection("offloadedfield");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.repository.OffloadingRepository#retrieveFieldsById
	 * (java.lang.String, it.unisannio.ding.dyno4ws.ProfileData)
	 */
	public List<OffloadedField> retrieveFieldsById(String id, ProfileData data) {
		List<OffloadedField> result = new ArrayList<OffloadedField>();

		OffloadedField item = references.get(id);
		if (item == null) {
			// field has been kept as a copy, retrieve it from db

			DBCursor cur = null;
			long sleepTime = minWait;
			while (sleepTime <= maxWait){
				BasicDBObject query = new BasicDBObject();
				query.put("idkey", id);
				cur = coll.find(query);
				int size = cur.size();
				if(size > 0){
					sleepTime = maxWait + 1;
				} else {
					Thread.yield();
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					sleepTime *= 2;
				}
			}

			while (cur.hasNext()) {
				DBObject next = cur.next();
				OffloadedField f = new OffloadedField();
				f.setName((String) next.get("name"));
				f.setTimestamp((Long) next.get("timestamp"));
				f.setType((String) next.get("type"));
				f.setData((String) next.get("data"));
				f.setEncoding((String) next.get("enc"));
				result.add(f);
			}
		} else {
			// field has been kept as a reference, thus it was in the table
			if (item.getData() == null) {
				serializeItem(item);
			}
			// references.remove(id);
			result.add(item);
		}
		// StringWriter xml = new StringWriter();
		// JAXB.marshal(result, xml);
		// System.out.println(xml.toString());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.repository.OffloadingRepository#offloadParamFields
	 * (java.lang.String, java.lang.String, java.lang.String, java.util.List)
	 */
	public List<String> offloadParamFields(String nodeId, String invocationId,
			String paramName, List<OffloadedField> fields) {
		List<String> result = new ArrayList<String>();
		List<DBObject> docs = new ArrayList<DBObject>();
		for (OffloadedField f : fields) {
			String key = createKey(nodeId, invocationId, paramName, f.getName());
			if (f.isCopy()) {
				if (f.getData() == null) {
					serializeItem(f);
				}
				DBObject object = new BasicDBObject();
				object.put("idkey", key);
				object.put("name", f.getName());
				object.put("timestamp", f.getTimestamp());
				object.put("enc", f.getEncoding());
				object.put("data", f.getData());
				object.put("type", f.getType());
				docs.add(object);
			} else {
				references.put(key, f);
			}
			result.add(key);
		}
		save(docs);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.repository.OffloadingRepository#setStrategyEndpoint
	 * (java.lang.String)
	 */
	public void setStrategyEndpoint(String endpoint) {
		// TODO Auto-generated method stub

	}

	protected void serializeItem(OffloadedField item) {
		if (item.getEncoding().equalsIgnoreCase("xml")) {
			toXML(item);
		} else if (item.getEncoding().equalsIgnoreCase("gzip_xml")) {
			toXML(item);
			compress(item);
		}
		// else if (item.getEncoding().equalsIgnoreCase("json")) {
		// toJSON(item);
		// } else if (item.getEncoding().equalsIgnoreCase("gzip_json")) {
		// toJSON(item);
		// compress(item);
		// }
		// else if (item.getEncoding().equalsIgnoreCase("hessian")) {
		// toHessian(item);
		// }
	}

	private void compress(OffloadedField item) {
		byte[] input = null;
		input = item.getData().getBytes();
		// Compress the bytes
		compresser.setInput(input);
		compresser.finish();
		ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

		byte[] buf = new byte[1024];
		while (!compresser.finished()) {
			int count = compresser.deflate(buf);
			bos.write(buf, 0, count);
		}
		try {
			bos.close();
		} catch (IOException e) {
		}

		// Get the compressed data
		byte[] compressedData = bos.toByteArray();
		// String data = Base64.encodeBase64String(compressedData);
		item.setData(new String(compressedData));
	}

	// private void toHessian(OffloadedField item) {
	// ByteArrayOutputStream buffer = new ByteArrayOutputStream();
	// Hessian2Output hessian = new Hessian2Output(buffer);
	// SerializerFactory factory = new SerializerFactory();
	// factory.setAllowNonSerializable(true);
	// try {
	//
	// hessian.startMessage();
	// Serializer objectSerializer = factory.getObjectSerializer(item
	// .getReference().getClass());
	// objectSerializer.writeObject(item.getReference(), hessian);
	// hessian.completeMessage();
	// hessian.flush();
	// String data = Base64.encodeBase64String(buffer.toByteArray());
	// item.setData(data);
	// buffer.close();
	// hessian.close();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	private void toXML(OffloadedField item) {
		StringWriter writer = new StringWriter();
		JAXB.marshal(item.getReference(), writer);
		item.setData(writer.toString());
	}

	// private void toJSON(OffloadedField item) {
	// String json = gson.toJson(item.getReference(), item.getReference()
	// .getClass());
	// item.setData(json);
	// }

	protected void save(List<DBObject> docs) {
		coll.insert(docs);
	}

	private String createKey(String nodeId, String invocationId,
			String paramName, String name) {
		return "{nodeId:" + nodeId + ",cid:" + invocationId + ",param:"
				+ paramName + ",name:" + name + "}";
	}

}
