package it.unisannio.ding.wslink.dataevents.distributedtest;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;

import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import javax.jws.Oneway;
import javax.jws.WebService;

/**
 * @author aalshaba
 */
@WebService(serviceName = "DataEventConsumer", portName = "DataEventConsumerPort", name = "DataEventConsumerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
public class SubscriberWS implements DataEventConsumer {

    public static final Logger logger =
            Logger.getLogger(SubscriberWS.class.getName());

    private AtomicInteger numInvocation = new AtomicInteger(0);
    private AtomicLong accumulatedTime = new AtomicLong(0);

    private ConcurrentSkipListSet<Long> eventPublicationTimes;

    private String endpoint;
    private int numberOfProducers;
    private int numberOfEvents;
    private int maxNumInvocation;

    private int threadPoolSize;
    private ExecutorService threadPool;

    // Boolean variables to probe the state of subscriber
    private boolean finnished;

    public SubscriberWS(String endpoint, int numberOfProducers,
            int numberOfEvents, int subscriberThreadPoolSize) {
        this.endpoint = endpoint;
        this.numberOfProducers = numberOfProducers;
        this.numberOfEvents = numberOfEvents;
        this.maxNumInvocation = numberOfEvents * numberOfProducers;
        this.eventPublicationTimes = new ConcurrentSkipListSet<Long>();

        this.threadPoolSize = subscriberThreadPoolSize;

        logger.info("Subscriber starting with thread pool size: "
                + this.threadPoolSize);

        if (this.threadPoolSize > 0) {
            this.threadPool = Executors.newFixedThreadPool(this.threadPoolSize);
        }
    }

    @Oneway
    @Override
    public void notify(final DataEvent event) {
        if (this.threadPoolSize > 0) {
            this.threadPool.execute(new Runnable() {

                @Override
                public void run() {
                    receiveNotification(event);
                }
            });
        } else {
            receiveNotification(event);
        }
    }

    private void receiveNotification(DataEvent event) {
        long timeToPartialMetaEvent =
                System.currentTimeMillis() - event.getTimestamp();

        byte[] data = event.getAttachments();

        long timeToReceiveFullEvent =
                System.currentTimeMillis() - event.getTimestamp();

        int numInvocation = this.numInvocation.incrementAndGet();

        logger.info("Event size received=" + data.length + ", invocation="
                + numInvocation + ", eventPublicationTime="
                + event.getTimestamp() + ", timeToReceivePartialEvent(t4)="
                + timeToPartialMetaEvent + ", timeToReceiveFullEvent(t8)="
                + timeToReceiveFullEvent);

        this.eventPublicationTimes.add(event.getTimestamp());
        this.accumulatedTime.addAndGet(timeToReceiveFullEvent);

        if (numInvocation == this.maxNumInvocation) {
            // you have to get the smallest publication time among all the
            // events received since the first event received by the subscriber
            // is not necessarily the first event published!
            long firstPublicationTime = eventPublicationTimes.first();
            long elapsedTime =
                    System.currentTimeMillis() - firstPublicationTime;

            this.finnished = true;

            logger.info("Total time elapsed between the first publication and the last event received: "
                    + elapsedTime);
        }
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * @return the status of subscriber.<b> true</b> If finished
     */
    public boolean isFinnished() {
        return this.finnished;
    }

}