package it.unisannio.ding.wslink.core;

public class ConfigurationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConfigurationException(String message){
		super(message);
	}
	
}
