/**
 * 
 */
package it.unisannio.ding.wslink.api.annotation;

import it.unisannio.ding.wslink.api.EncodingType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Quirino Zagarese
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Encoding {
	public EncodingType value() default EncodingType.XML;
}
