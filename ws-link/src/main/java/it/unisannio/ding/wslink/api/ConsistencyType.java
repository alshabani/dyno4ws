/**
 * 
 */
package it.unisannio.ding.wslink.api;

/**
 * @author Quirino Zagarese
 *
 */
public enum ConsistencyType {

	MAKE_COPY,
	KEEP_REFERENCE;
	
}
