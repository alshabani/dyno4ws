package it.unisannio.ding.dyno4ws.repository;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class FieldRetriever {

	private static Map<String, OffloadingRepository> clients = new HashMap<String, OffloadingRepository>();

	public static List<OffloadedField> retrieveParamFieldBy(String endpoint,
			String id, ProfileData data) {
		OffloadingRepository client = checkClient(endpoint);
		return client.retrieveFieldsById(id, data);
	}

	private static OffloadingRepository checkClient(String endpoint) {
		OffloadingRepository client = clients.get(endpoint);
		if (client == null) {
			JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
			factory.setServiceClass(OffloadingRepository.class);
			factory.setAddress(endpoint);
			client = (OffloadingRepository) factory.create();
			clients.put(endpoint, client);
		}
		return client;
	}

	// private static Object invokeService(String method, Object[] args, Client
	// client) {
	// Object[] result = null;
	// try {
	// result = client.invoke(method, args);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return result[0];
	// }
}
