package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import javax.jws.WebService;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;

@WebService(serviceName = "DataEventConsumer1", portName = "DataEventConsumer1Port", name = "DataEventConsumer1PortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
public class SubscriberService1 implements DataEventConsumer {

	public void notify(DataEvent event) {
		System.out.println("Subscriber1: I received an event! Event topic is: "
				+ event.getTopicExpression());
		String content = new String(event.getAttachments());
		System.out.println("Event content is: " + content);
	}

}
