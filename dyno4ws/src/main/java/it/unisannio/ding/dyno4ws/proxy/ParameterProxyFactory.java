package it.unisannio.ding.dyno4ws.proxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.NotFoundException;

public class ParameterProxyFactory {

	private static Map<String, CtClass> proxyClasses = new HashMap<String, CtClass>();

	public static Object createProxy(ProxyContext context) {
		Object target = context.getTarget();
		CtClass proxyClass = null;
		synchronized (ParameterProxyFactory.class) {
			proxyClass = proxyClasses.get(target.getClass().getName());
			if (proxyClass == null) {

				proxyClass = createProxyClass(target);

			}
		}
		return instantiateProxy(proxyClass, context);
	}

	private static Object instantiateProxy(CtClass proxyClass,
			ProxyContext context) {
		Object proxy = null;
		try {
			Class<?> jdkClass = null;
			try {
				jdkClass = Class.forName(proxyClass.getName());
			} catch (ClassNotFoundException e) {
				jdkClass = proxyClass.toClass();
			}
			@SuppressWarnings("unused")
			Constructor<?>[] declaredConstructors = jdkClass
					.getDeclaredConstructors();
			Constructor<?> constructor = jdkClass
					.getConstructor(new Class[] { ProxyContext.class });
			proxy = constructor.newInstance(new Object[] { context });
		} catch (CannotCompileException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return proxy;
	}

	private static synchronized CtClass createProxyClass(Object target) {
		ClassPool classPool = ClassPool.getDefault();
		Class<? extends Object> realClass = target.getClass();
		CtClass clazz = classPool.makeClass(realClass.getSimpleName()
				+ "$Dyno4WSProxy");
		try {
			CtClass javassistRealClass = classPool.get(realClass.getName());
			clazz.setSuperclass(classPool.get(realClass.getName()));
			clazz.addInterface(ClassPool.getDefault().get(
					"it.unisannio.ding.dyno4ws.proxy.DynO4WSProxy"));
			createFields(clazz);
			createConstructor(clazz);
			for (Method m : realClass.getDeclaredMethods()) {
				if (m.getName().startsWith("get")) {
					String body = createGetterBody(javassistRealClass, m);
					createMethod(m, clazz, javassistRealClass, body,
							new CtClass[0], new CtClass[0]);
				} else if (m.getName().startsWith("set")) {
					String body = createSetterBody(javassistRealClass, m);
					createMethod(m, clazz, javassistRealClass, body,
							new CtClass[] { classPool.get(decodeTypeName(m
									.getParameterTypes()[0])) }, new CtClass[0]);
				}

			}
			Method finalize = Object.class.getDeclaredMethod("finalize",
					new Class<?>[0]);
			String body = createFinalizeBody(javassistRealClass, finalize);
			createMethod(finalize, clazz, javassistRealClass, body,
					new CtClass[0], new CtClass[0]);

			Method getContext = DynO4WSProxy.class.getDeclaredMethod(
					"getContext", new Class<?>[0]);
			createMethod(getContext, clazz, javassistRealClass,
					"{return this.context;}", new CtClass[0], new CtClass[0]);

			clazz.toClass();
		} catch (CannotCompileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		proxyClasses.put(target.getClass().getName(), clazz);
		return clazz;
	}

	private static void createFields(CtClass clazz) {
		CtField f = null;
		try {
			f = CtField
					.make("private it.unisannio.ding.dyno4ws.proxy.ProxyContext context;",
							clazz);
			clazz.addField(f);
		} catch (CannotCompileException e) {
			e.printStackTrace();
		}

	}

	private static void createConstructor(CtClass clazz) {
		CtConstructor c = null;
		CtClass[] parameters = new CtClass[1];
		ClassPool classPool = ClassPool.getDefault();
		try {
			parameters[0] = classPool
					.get("it.unisannio.ding.dyno4ws.proxy.ProxyContext");
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CtClass[] exceptions = new CtClass[0];
		String body = "{" + "		this.context = $1;"
				+ "		this.context.setProxy(this);"
				+ "		this.context.getInterceptor().onConstruct(this.context);"
				+ "}";
		try {
			c = CtNewConstructor.make(parameters, exceptions, body, clazz);
			clazz.addConstructor(c);
		} catch (CannotCompileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void createMethod(Method m, CtClass clazz,
			CtClass javassistRealClass, String body, CtClass[] parameters,
			CtClass[] exceptions) {
		CtMethod method = null;
		try {
			// CtField field = javassistRealClass.getDeclaredField(fieldName);
			ClassPool classPool = ClassPool.getDefault();
			CtClass returnType = classPool
					.get(decodeTypeName(m.getReturnType()));
			method = CtNewMethod.make(returnType, m.getName(), parameters,
					exceptions, body, clazz);
			clazz.addMethod(method);
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (CannotCompileException e) {
			e.printStackTrace();
		}
	}

	private static String decodeTypeName(Class<?> returnType) {
		String name = "";
		if (returnType.isPrimitive()) {
			name = returnType.getSimpleName();
		} else if (returnType.isArray()) {
			if (returnType.getPackage() == null) {
				name = returnType.getSimpleName();
			} else {
				name = returnType.getPackage().getName() + "."
						+ returnType.getSimpleName();
			}
		} else {
			name = returnType.getName();
		}
		return name;
	}

	private static String createGetterBody(CtClass javassistRealClass, Method m) {
		String body = "{"
				+ "	Object result = null;"
				+ "	synchronized (this.context) {"
				+ "			java.lang.reflect.Method m = null;"
				+ "			try {"
				+ "				m = this.context.getTarget().getClass().getDeclaredMethod(\""
				+ m.getName()
				+ "\", new Class[0]);"
				+ "				this.context.setInterceptedMethod(m);"
				+ "				result = this.context.getInterceptor().onGet(this.context);"
				+ "			} catch (Exception e) {" + "				e.printStackTrace();"
				+ "			}" + "	}" + "	return " + "("
				+ decodeTypeName(m.getReturnType()) + ") result;" + "}";
		return body;
	}

	private static String createSetterBody(CtClass javassistRealClass, Method m) {
		String body = "{"
				+ "	synchronized (this.context) {"
				+ "			java.lang.reflect.Method m = null;"
				+ "			try {"
				+ "				m = this.context.getTarget().getClass().getDeclaredMethod(\""
				+ m.getName() + "\", new Class[]{"
				+ decodeTypeName(m.getParameterTypes()[0]) + ".class" + "});"
				+ "				this.context.setInterceptedMethod(m);"
				+ "				this.context.setArgs(new Object[] {$1});"
				+ "				this.context.getInterceptor().onSet(this.context);"
				+ "			} catch (Exception e) {" + "				e.printStackTrace();"
				+ "			}" + "	}" + "}";
		return body;
	}

	private static String createFinalizeBody(CtClass javassistRealClass,
			Method finalize) {
		String body = "{" + "	synchronized (this.context) {"
				+ "			java.lang.reflect.Method m = null;" + "			try {"
				+ "				m = java.lang.Object.class.getDeclaredMethod(\""
				+ finalize.getName() + "\", new Class[0]);"
				+ "				this.context.setInterceptedMethod(m);"
				+ "				this.context.getInterceptor().onDestroy(this.context);"
				+ "			} catch (Exception e) {" + "				e.printStackTrace();"
				+ "			}" + "	}" + "}";
		return body;
	}

}
