/**
 * 
 */
package it.unisannio.ding.wslink.core;

import it.unisannio.ding.dyno4ws.DynO4WSParam;
import it.unisannio.ding.dyno4ws.DynO4WSParamField;
import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.ProfileData;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.dyno4ws.repository.OffloadingRepository;
import it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy;
import it.unisannio.ding.wslink.api.annotation.Consistency;
import it.unisannio.ding.wslink.api.annotation.Encoding;
import it.unisannio.ding.wslink.api.annotation.Strategy;
import it.unisannio.ding.wslink.api.annotation.WSLink;
import it.unisannio.ding.wslink.core.annotation.AnnotationProcessor;
import it.unisannio.ding.wslink.core.annotation.ConsistencyAnnotationProcessor;
import it.unisannio.ding.wslink.core.annotation.EncodingAnnotationProcessor;
import it.unisannio.ding.wslink.core.annotation.StrategyAnnotationProcessor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Quirino Zagarese
 * 
 */
public class WSLinkOffloadingStrategy implements OffloadingStrategy {

	private static final Logger LOGGER = Logger
			.getLogger(WSLinkOffloadingStrategy.class.getName());

	private Map<Object, Class<? extends Annotation>> processorsMap;

	private Map<Field, Map<Class<? extends Annotation>, Map<String, Object>>> annotationsCache;

	public WSLinkOffloadingStrategy() {
		annotationsCache = new HashMap<Field, Map<Class<? extends Annotation>, Map<String, Object>>>();

		processorsMap = new HashMap<Object, Class<? extends Annotation>>();
		processorsMap.put(new ConsistencyAnnotationProcessor(),
				Consistency.class);
		processorsMap.put(new EncodingAnnotationProcessor(), Encoding.class);
		processorsMap.put(new StrategyAnnotationProcessor(), Strategy.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy#
	 * getParamOffloadableFields(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.util.List,
	 * java.util.List)
	 */
	public List<String> getParamOffloadableFields(String nodeId,
			String endpoint, String operation, String parameterName,
			String parameterType, List<String> fieldsList,
			List<Integer> fieldsSizes) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy#
	 * selectAndOffloadParamFields(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.Object,
	 * it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext)
	 */
	public DynO4WSParam selectAndOffloadParamFields(String nodeId,
			String endpoint, String operation, String paramName, Object target,
			OffloadingContext context) {
		DynO4WSParam result = new DynO4WSParam();
		List<OffloadedField> fields2Offload = new ArrayList<OffloadedField>();

		// Inspect fields only if the target is annotated with @WSLink
		if (target.getClass().isAnnotationPresent(WSLink.class)) {
			for (Field f : target.getClass().getDeclaredFields()) {
				Object fieldValue = getValue(f, target);
				// Lazily process annotations only if the field is not null
				if (fieldValue != null) {
					OffloadedField offloadedField = processField(f, fieldValue,
							target, context);
					if (offloadedField != null) {
						fields2Offload.add(offloadedField);
						setNull(f, target);
					}
				}
			}
		}

		// offload selected fields to repository, if any
		OffloadingRepository repository = context.getRepository();
		if (fields2Offload.size() > 0) {

			List<String> ids = repository.offloadParamFields(
					context.getNodeId(), context.getCallId(),
					context.getParameterName(), fields2Offload);

			// consistently adjust the result
			for (int i = 0; i < ids.size(); i++) {
				OffloadedField field = fields2Offload.get(i);
				DynO4WSParamField fieldHeader = new DynO4WSParamField();
				fieldHeader.setHost(context.getRepositoryEndpoint());
				fieldHeader.setName(field.getName());
				fieldHeader.setCallId(context.getCallId());
				fieldHeader.setId(ids.get(i));
				result.getFields().add(fieldHeader);
			}
		}

		return result;
	}

	private void setNull(Field f, Object target) {
		boolean accessible = f.isAccessible();
		f.setAccessible(true);
		try {
			f.set(target, null);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		f.setAccessible(accessible);

	}

	private OffloadedField processField(Field f, Object fieldValue,
			Object parent, OffloadingContext context) {
		OffloadedField result = new OffloadedField();
		result.setName(f.getName());
		result.setTimestamp(System.nanoTime());
		result.setType(f.getType().getName());
		result.setReference(fieldValue);

		// Search in cache first, otherwise invoke annotation processors
		Map<Class<? extends Annotation>, Map<String, Object>> metadata = annotationsCache
				.get(f);
		if (metadata == null) {
			metadata = new HashMap<Class<? extends Annotation>, Map<String, Object>>();

			for (Object o : processorsMap.keySet()) {
				AnnotationProcessor processor = (AnnotationProcessor) o;
				Class<? extends Annotation> annotationClass = processorsMap
						.get(processor);
				Annotation annotation = f.getAnnotation(annotationClass);

				if (annotation != null) {
					metadata.put(annotationClass,
							processor.scanAnnotation(annotation, f));
				}
			}
		}

		// Process field by means of annotation processors
		List<Object> keysList = new ArrayList<Object>(processorsMap.keySet());
		for (int i = 0; (i < keysList.size()) && (result != null); i++) {
			AnnotationProcessor processor = (AnnotationProcessor) keysList
					.get(i);

			// If a processor returns a null value, it means that the field is
			// not an offloading candidate,
			// thus all other annotations can be skipped
			result = processor.processField(metadata, f, fieldValue, parent,
					result, context);
		}
		// Finally update cache
		annotationsCache.put(f, metadata);

		return result;
	}

	private Object getValue(Field f, Object target) {
		Object value = null;
		boolean accessible = f.isAccessible();
		f.setAccessible(true);
		try {
			value = f.get(target);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		f.setAccessible(accessible);
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.dyno4ws.strategy.OffloadingStrategy#updateProfiling
	 * (java.lang.String, it.unisannio.ding.dyno4ws.ProfileData)
	 */
	public void updateProfiling(String nodeId, ProfileData data) {
		// TODO Auto-generated method stub

	}

	public void setAnnotationProcessor(Object processor,
			Class<? extends Annotation> annotation)
			throws ConfigurationException {
		if (processor instanceof AnnotationProcessor) {
			processorsMap.put(processor, annotation);
		} else {
			throw new ConfigurationException(
					"The provided instance must implement the "
							+ AnnotationProcessor.class.getName()
							+ " interface.");
		}
	}
}
