package it.unisannio.ding.wslink.dataevents.eventservice.core;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.NotificationManager;

import java.util.List;

import javax.jws.Oneway;
import javax.jws.WebService;

@WebService(serviceName = "DataEventConsumer", portName = "DataEventConsumerPort", name = "DataEventConsumerPortType", endpointInterface = "it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer")
public class BaseEventConsumerService implements DataEventConsumer {
	private NotificationManager notificationManager;

	public BaseEventConsumerService(NotificationManager notificationManager) {
		this.notificationManager = notificationManager;
	}

	@Oneway
	@Override
	public void notify(DataEvent event) {
		List<Subscription> subscriptions = notificationManager
				.getSubscriptionsFor(event.getTopicExpression());
		if (subscriptions != null) {
			for (Subscription subscription : subscriptions) {
				notificationManager.deliverEvent(event, subscription);
			}
		}
	}

}
