/**
 * 
 */
package it.unisannio.ding.wslink.core.annotation;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.wslink.api.EncodingType;
import it.unisannio.ding.wslink.api.annotation.Encoding;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Quirino Zagarese
 * 
 */
public class EncodingAnnotationProcessor implements AnnotationProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.wslink.core.AnnotationProcessor#scanAnnotation(java
	 * .lang.annotation.Annotation, java.lang.reflect.Field)
	 */
	public Map<String, Object> scanAnnotation(Annotation annotation,
			Field declaringField) {
		Map<String, Object> result = new HashMap<String, Object>();
		Encoding e = (Encoding) annotation;
		result.put("value", e.value());
		return result;
	}

	public OffloadedField processField(
			Map<Class<? extends Annotation>, Map<String, Object>> metadata,
			Field f, Object fieldValue, Object parent, OffloadedField result,
			OffloadingContext context) {
		Map<String, Object> localData = metadata.get(Encoding.class);
		if (localData != null) {
			result.setEncoding(((EncodingType) localData.get("value"))
					.toString());
		}
		return result;
	}

}
