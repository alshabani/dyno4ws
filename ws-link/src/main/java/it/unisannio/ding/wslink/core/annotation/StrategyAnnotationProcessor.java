/**
 * 
 */
package it.unisannio.ding.wslink.core.annotation;

import it.unisannio.ding.dyno4ws.OffloadedField;
import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;
import it.unisannio.ding.wslink.api.WSLinkStrategy;
import it.unisannio.ding.wslink.api.annotation.Strategy;
import it.unisannio.ding.wslink.api.annotation.StrategyParameter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Quirino Zagarese
 * 
 */
public class StrategyAnnotationProcessor implements AnnotationProcessor {

	private static final Logger LOGGER = Logger
			.getLogger(StrategyAnnotationProcessor.class.getName());

	private Map<Class<? extends WSLinkStrategy>, Object> strategies;

	public StrategyAnnotationProcessor() {
		strategies = new HashMap<Class<? extends WSLinkStrategy>, Object>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.unisannio.ding.wslink.core.AnnotationProcessor#scanAnnotation(java
	 * .lang.annotation.Annotation, java.lang.reflect.Field)
	 */
	public Map<String, Object> scanAnnotation(Annotation annotation,
			Field declaringField) {
		Map<String, Object> result = new HashMap<String, Object>();
		Strategy s = (Strategy) annotation;
		result.put("impl", s.impl());
		Map<String, String> params = new HashMap<String, String>();
		for (StrategyParameter sp : s.params()) {
			params.put(sp.name(), sp.value());
		}
		result.put("params", params);
		WSLinkStrategy strategy = (WSLinkStrategy) strategies.get(s.impl());
		if (strategy == null) {
			try {
				strategy = s.impl().newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.log(Level.SEVERE, "Could not instantiate strategy "
						+ s.impl().getName()
						+ ". A zero parameters constructor must be provided.");
			}
			if (strategy instanceof WSLinkStrategy) {
				strategies.put(s.impl(), strategy);
			} else {
				LOGGER.log(Level.SEVERE, "Strategy " + s.impl().getName()
						+ " does not implement interface "
						+ WSLinkStrategy.class.getSimpleName()
						+ ". Skipping...");
			}
		}
		return result;
	}

	public OffloadedField processField(
			Map<Class<? extends Annotation>, Map<String, Object>> metadata,
			Field f, Object fieldValue, Object parent, OffloadedField result,
			OffloadingContext context) {
		Map<String, Object> localData = metadata.get(Strategy.class);
		if (localData != null) {
			WSLinkStrategy strategy = (WSLinkStrategy) strategies.get(localData
					.get("impl"));
			@SuppressWarnings("unchecked")
			Map<String, String> params = (Map<String, String>) localData
					.get("params");
			boolean strategyResult = strategy.shouldOffload(fieldValue,
					context, params);
			if (strategyResult) {
				result = null;
			}
		} else {
			result = null;
		}
		return result;
	}

}
