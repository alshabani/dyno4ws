/**
 * 
 */
package it.unisannio.ding.wslink.dataevents.distributedtest;

import it.unisannio.ding.wslink.dataevents.AttachmentDescriptor;
import it.unisannio.ding.wslink.dataevents.AttachmentType;
import it.unisannio.ding.wslink.dataevents.DataEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.apache.cxf.endpoint.Client;

/**
 * @author aalshaba
 * 
 */
public class EventGenerator implements Runnable {

    public static final Logger logger =
            Logger.getLogger(EventGenerator.class.getName());

    private Random rand = new Random();

    private String producerEndpoint;
    private String topic;
    private int attachmentSizeInKB;
    private long delay;
    private int numberOfEvents;
    private Client publisherClient;

    private int threadPoolSize;
    private ExecutorService threadPool;

    public EventGenerator(String producerEndpoint, String topic,
            int numberOfEvents, int attachmentSizeInKB, long delay,
            Client publisherClient, int threadPoolSize) {
        this.producerEndpoint = producerEndpoint;
        this.topic = topic;
        this.numberOfEvents = numberOfEvents;
        this.attachmentSizeInKB = attachmentSizeInKB;
        this.delay = delay;
        this.publisherClient = publisherClient;

        this.threadPoolSize = threadPoolSize;

        logger.info("Event generator starting with threadPoolSize="
                + this.threadPoolSize + " and attachmentSizeInKB="
                + this.attachmentSizeInKB);

        if (this.threadPoolSize > 0) {
            this.threadPool = Executors.newFixedThreadPool(this.threadPoolSize);
        }
    }

    @Override
    public void run() {
        System.out.println("New publisher starts at endpoint: "
                + producerEndpoint);

        for (int i = 0; i < numberOfEvents; i++) {
            final DataEvent event = createDataEvent(topic, attachmentSizeInKB);

            if (this.threadPoolSize > 0) {
                this.threadPool.execute(new Runnable() {

                    @Override
                    public void run() {
                        publishEvent(event);
                    }
                });
            } else {
                publishEvent(event);

                if (this.delay > 0) {
                    try {
                        Thread.sleep(delay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void publishEvent(DataEvent event) {
        try {
            publisherClient.invoke("notify", new Object[] {event});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private DataEvent createDataEvent(String topic, int attachmentSize) {
        DataEvent event = new DataEvent();
        event.setTopicExpression(topic);

        AttachmentDescriptor descriptor = new AttachmentDescriptor();
        descriptor.setType(AttachmentType.INSTANCE);
        descriptor.setBeginOffset(0);

        List<AttachmentDescriptor> descriptors =
                new ArrayList<AttachmentDescriptor>();
        descriptors.add(descriptor);
        event.setDescriptors(descriptors);

        byte[] data = generateAndFillData(attachmentSize);
        descriptor.setEndOffset(data.length);

        event.setAttachments(data);

        event.setTimestamp(System.currentTimeMillis());

        return event;
    }

    private byte[] generateAndFillData(int attachmentSize) {
        byte[] data = new byte[attachmentSize * 1024];
        this.rand.nextBytes(data);
        return data;
    }

}
