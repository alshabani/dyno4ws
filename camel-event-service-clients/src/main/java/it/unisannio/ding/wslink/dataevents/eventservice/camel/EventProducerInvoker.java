package it.unisannio.ding.wslink.dataevents.eventservice.camel;

import it.unisannio.ding.wslink.dataevents.DataEvent;
import it.unisannio.ding.wslink.dataevents.Subscription;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventConsumer;
import it.unisannio.ding.wslink.dataevents.eventservice.DataEventProducer;
import it.unisannio.ding.wslink.dataevents.util.ServiceUtility;

import java.net.URL;

import org.apache.cxf.endpoint.Client;

public class EventProducerInvoker {

	private static final String SUBSCRIBER_ENDPOINT_1 = "http://localhost:43567/receiver1";

	private static final String SUBSCRIBER_ENDPOINT_2 = "http://localhost:43568/receiver2";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Client client1 = ServiceUtility.createWsClient(DataEventProducer.class,
				"http://localhost:9999/subscriptionService");
		Subscription s1 = new Subscription();
		s1.setContent("graphics");
		ServiceUtility.createService(new SubscriberService1(),
				SUBSCRIBER_ENDPOINT_1);

		try {
			s1.setSubscriberEndpoint(new URL(SUBSCRIBER_ENDPOINT_1));
			client1.invoke("subscribe", new Object[] { s1 });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Client eventSender = ServiceUtility.createWsClient(
				DataEventConsumer.class, "http://localhost:9999/eventService");
		DataEvent event = new DataEvent();
		event.setTopicExpression("graphics");
		event.setAttachments("A nice picture...".getBytes());

		try {
			eventSender.invoke("notify", new Object[] { event });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Client client2 = ServiceUtility.createWsClient(DataEventProducer.class,
				"http://localhost:9999/subscriptionService");
		Subscription s2 = new Subscription();
		s2.setContent("graphics");
		ServiceUtility.createService(new SubscriberService2(),
				SUBSCRIBER_ENDPOINT_2);

		Subscription s3 = new Subscription();
		s3.setContent("music");

		try {
			s2.setSubscriberEndpoint(new URL(SUBSCRIBER_ENDPOINT_2));
			s3.setSubscriberEndpoint(new URL(SUBSCRIBER_ENDPOINT_2));
			client2.invoke("subscribe", new Object[] { s2 });
			client2.invoke("subscribe", new Object[] { s3 });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DataEvent event2 = new DataEvent();
		event2.setTopicExpression("graphics");
		event2.setAttachments("An even nicer picture...".getBytes());

		try {
			eventSender.invoke("notify", new Object[] { event2 });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DataEvent event3 = new DataEvent();
		event3.setTopicExpression("music");
		event3.setAttachments("Some extraordinary music...".getBytes());

		try {
			eventSender.invoke("notify", new Object[] { event3 });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
