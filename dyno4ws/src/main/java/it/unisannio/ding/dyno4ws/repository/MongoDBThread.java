package it.unisannio.ding.dyno4ws.repository;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class MongoDBThread extends Thread {

	private DBCollection coll;
	
	private Queue<List<DBObject>> jobs = new ConcurrentLinkedQueue<List<DBObject>>();

	private boolean started = true;

	public MongoDBThread(DBCollection coll) {
		this.coll = coll;
		this.setPriority(Thread.NORM_PRIORITY);
	}

	public void run(){
		while(this.started ){
			if(!jobs.isEmpty()){
				List<DBObject> docs = jobs.remove();
				coll.getDB().requestStart();
				coll.insert(docs);
				coll.getDB().requestDone();
			}
			Thread.yield();
		}
				
	}
	
	public void save(List<DBObject> docs){
		jobs.add(docs);
	}
	
	
}
