package it.unisannio.ding.wslink.dataevents.eventservice.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class SimpleOutputWriter {

	private PrintStream ps;

	public SimpleOutputWriter(String filename) {
		try {
			ps = new PrintStream(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void writeOutput(String threadname, long interval,
			int events) {
		double throughtput = (double) ((double) events * 1000)
				/ (double) interval;
		ps.println(threadname + "," + interval + "," + events + ","
				+ throughtput);
	}

}
