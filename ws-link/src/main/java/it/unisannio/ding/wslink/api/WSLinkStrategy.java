package it.unisannio.ding.wslink.api;

import it.unisannio.ding.dyno4ws.interceptors.offloading.OffloadingContext;

import java.util.Map;

public interface WSLinkStrategy {

	public boolean shouldOffload(Object field, OffloadingContext context,
			Map<String, String> strategyParams);

}
